#include <bits/stdc++.h>
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

ll ans = 0;

ll cards[200];

void solve() {
    string s;
    int id;
    while(cin >> id) {
        cards[id] += 1;
        int x;
        set<int> st;
        while(cin.peek() != '|') {
            cin >> x;
            st.insert(x);
            cin.ignore();
        }
        char c;
        cin >> c;
        ll cur = 0;
        while(cin.peek() != '\n') {
            cin >> x;
            if (st.count(x)) cur++;
        }
        rep(card, id+1, id+1+cur) cards[card] += cards[id];
    }
    ans = 0;
    rep(i, 1, id+1) ans += cards[i];
    cout << ans << '\n';
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}