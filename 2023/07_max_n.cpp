#include <bits/stdc++.h>
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

vector<char> v = {
    'A', 'K', 'Q', 'J', 'T', '9', '8', '7', '6', '5', '4', '3', '2'
};

// This code follows my own invented design pattern: WET

bool five(string s) {
    rep(i, 0, 5) {
        if (s[i] == 'J') {
            bool ok = false;
            for (char c : v) if (c != 'J') {
                s[i] = c;
                ok = (ok || five(s));
            }
            return ok;
        }
    }


    map<char, int> mp1;
    for (char c : s) mp1[c]++;
    if (mp1.size() == 1) return true;
    return false;
}
bool four(string s) {
    rep(i, 0, 5) {
        if (s[i] == 'J') {
            bool ok = false;
            for (char c : v) if (c != 'J') {
                s[i] = c;
                ok = (ok || four(s));
            }
            return ok;
        }
    }

    map<char, int> mp1;
    for (char c : s) mp1[c]++;
    if (mp1.size() == 2) {
        int mx = -1;
        for (auto p : mp1) mx = max(mx, p.second);
        return mx == 4;
    }
    return false;
}
bool house(string s) {
    rep(i, 0, 5) {
        if (s[i] == 'J') {
            bool ok = false;
            for (char c : v) if (c != 'J') {
                s[i] = c;
                ok = (ok || house(s));
            }
            return ok;
        }
    }

    map<char, int> mp1;
    for (char c : s) mp1[c]++;
    if (mp1.size() == 2) {
        int mx = -1;
        for (auto p : mp1) mx = max(mx, p.second);
        return mx == 3;
    }
    return false;
}
bool three(string s) {
    rep(i, 0, 5) {
        if (s[i] == 'J') {
            bool ok = false;
            for (char c : v) if (c != 'J') {
                s[i] = c;
                ok = (ok || three(s));
            }
            return ok;
        }
    }

    map<char, int> mp1;
    for (char c : s) mp1[c]++;
    if (mp1.size() == 3) {
        int mx = -1;
        for (auto p : mp1) mx = max(mx, p.second);
        return mx == 3;
    }
    return false;
}
bool two(string s) {
    rep(i, 0, 5) {
        if (s[i] == 'J') {
            bool ok = false;
            for (char c : v) if (c != 'J') {
                s[i] = c;
                ok = (ok || two(s));
            }
            return ok;
        }
    }

    map<char, int> mp1;
    for (char c : s) mp1[c]++;
    if (mp1.size() == 3) {
        int mx = -1;
        for (auto p : mp1) mx = max(mx, p.second);
        return mx == 2;
    }
    return false;
}
bool one(string s) {
    rep(i, 0, 5) {
        if (s[i] == 'J') {
            bool ok = false;
            for (char c : v) if (c != 'J') {
                s[i] = c;
                ok = (ok || one(s));
            }
            return ok;
        }
    }

    map<char, int> mp1;
    for (char c : s) mp1[c]++;
    if (mp1.size() == 4) {
        int mx = -1;
        for (auto p : mp1) mx = max(mx, p.second);
        return mx == 2;
    }
    return false;
}

map<char, int> mp;

string cmp_s(string s1) {
    string t1;
    for (char c : s1) t1 += mp[c];
    return t1;
}


void solve() {
    rep(i, 0, v.size()) mp[v[i]] = i;

    vector<pair<string, ll>> w;
    string s;
    ll x;
    while(cin >> s >> x) {
        w.pb(make_pair(s, x));
        cout << s << ' ' << five(s) << ' ' << four(s) << ' ' << house(s) << ' ' << three(s) << ' ' << two(s) << ' ' << one(s) << '\n';
    }
    sort(all(w), [](auto p1, auto p2) {
        string s1 = p1.first, s2 = p2.first;
        if (five(s1) && five(s2)) return cmp_s(s1) < cmp_s(s2);
        if (five(s1) != five(s2)) return five(s1);

        if (four(s1) && four(s2)) return cmp_s(s1) < cmp_s(s2);
        if (four(s1) != four(s2)) return four(s1);

        if (house(s1) && house(s2)) return cmp_s(s1) < cmp_s(s2);
        if (house(s1) != house(s2)) return house(s1);

        if (three(s1) && three(s2)) return cmp_s(s1) < cmp_s(s2);
        if (three(s1) != three(s2)) return three(s1);

        if (two(s1) && two(s2)) return cmp_s(s1) < cmp_s(s2);
        if (two(s1) !=two(s2)) return two(s1);

        if (one(s1) && one(s2)) return cmp_s(s1) < cmp_s(s2);
        if (one(s1) != one(s2)) return one(s1);

        return cmp_s(s1) < cmp_s(s2);
    });
    reverse(all(w));
    ll ans = 0;
    ll rnk = 1;
    for (auto p : w) {
        cout << "got p = " << p.first << ' ' << p.second << '\n';
        ans += p.second*rnk;
        rnk++;
    }
    cout << ans << '\n';
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    ll test = 1;
    // cin >> test;
    while(test--) solve();
}