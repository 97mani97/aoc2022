#include <bits/stdc++.h>
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

vector<string> mat;

ll getans() {
    int n = mat.size(), m = mat[0].size();
    ll ans = 0;
    rep(j, 1, m) {
        int no = 0;
        rep(i, 0, n) rep(dj, 0, m) {
            if (j+dj < m && j - dj - 1 >= 0) {
                if (mat[i][j+dj] != mat[i][j-dj-1]) no++;
            }
        }
        if (no == 1) return j;
    }
    rep(i, 1, n) {
        int no = 0;
        rep(j, 0, m) rep(di, 0, n) {
            if (i+di < n && i - di - 1 >= 0) {
                if (mat[i+di][j] != mat[i-di-1][j]) no++;
            }
        }
        if (no == 1) return 100*i;
    }
    assert(false);
    return 0;
}
    
void solve() {
    string s;
    ll ans = 0;
    while(getline(cin, s)) {
        if (s.size() == 0) {
            ans += getans();
            mat.clear();
        } else {
            mat.pb(s);
        }
    }
    cout << ans << '\n';
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif
    solve();
}