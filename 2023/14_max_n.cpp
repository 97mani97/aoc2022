#include <bits/stdc++.h>
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

vector<string> mat;

ll tilt() {
    ll ans = 0;

    int n = mat.size(), m = mat[0].size();
    rep(j, 0, m) {
        int rock = 0;
        rep(i, 0, n) {
            if (mat[i][j] == '#') rock = i+1;
            else if (mat[i][j] == 'O'){                
                mat[i][j] = '.';
                mat[rock][j] = 'O';
                
                rock++;
            }
        }
    }
    rep(i, 0, n) {
        int rock = 0;
        rep(j, 0, m) {
            if (mat[i][j] == '#') rock = j+1;
            else if (mat[i][j] == 'O'){                
                mat[i][j] = '.';
                mat[i][rock] = 'O';
                
                rock++;
            }
        }
    }
    rep(j, 0, m) {
        int rock = n-1;
        for (int i = n-1; i >= 0; i--) {
            if (mat[i][j] == '#') rock = i-1;
            else if (mat[i][j] == 'O'){                
                mat[i][j] = '.';
                mat[rock][j] = 'O';
                
                rock--;
            }
        }
    }
    rep(i, 0, n) {
        int rock = m-1;
        for (int j = m-1; j >= 0; j--) {
            if (mat[i][j] == '#') rock = j-1;
            else if (mat[i][j] == 'O'){                
                mat[i][j] = '.';
                mat[i][rock] = 'O';
                
                rock--;
            }
        }
    }
    ans = 0;
    rep(i, 0, n) rep(j, 0, m) if (mat[i][j] == 'O') ans += n-i;
    return ans;
}

void solve() {
    string s;
    ll ans = 0;
    while(cin >> s) mat.pb(s);

    int n = mat.size(), m = mat[0].size();
    int mxn = 1000000000;

    map<vector<string>, int> ind;

    bool found = false;
    for (int i = 1; i <= mxn; i++) {
        ans = tilt();

        if (ind.count(mat) && !found) {
            ll x = ind[mat];
            ll rot = i - ind[mat];

            ll fit = (mxn - x)/rot;
            i += rot*(fit-1);
            found = true;
        }
        ind[mat] = i;
    }
    cout << ans << '\n';
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}