#include <bits/stdc++.h>
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

string s;
map<string, string> mp;

ll glob = 0;

void pass(string s, map<char, pair<ll, ll>> cur) {

    if (s.size() == 1 && s[0] == 'A') {
        glob += (cur['x'].second+1-cur['x'].first)*(cur['m'].second+1-cur['m'].first)*(cur['a'].second+1-cur['a'].first)*(cur['s'].second+1-cur['s'].first);
        return;

    } 
    if (s.size() == 1 && s[0] == 'R') {
        return;
    }

    string t = mp[s];
    stringstream ss;
    ss << t;

    string tt;
    char c, op;
    ll x;
    string nxt;
    // cout << " got t = " << t << '\n';
    while(ss >> tt) {
        if (tt.size() > 1) {
            pass(tt, cur);
            return;
        }
        c = tt[0];
        if (c == 'R') return;
        if (c == 'A') {
            glob += (cur['x'].second+1-cur['x'].first)*(cur['m'].second+1-cur['m'].first)*(cur['a'].second+1-cur['a'].first)*(cur['s'].second+1-cur['s'].first);

            return;
        }
        ss >> op >> x >> nxt;
        if (op == '>') {
            pii p = cur[c];

            ll new_min = max(x+1, p.first);
            pii q(new_min, p.second);
            if (q.first <= q.second) {
                map<char, pair<ll, ll>> cur2 = cur;
                cur2[c] = q;
                pass(nxt, cur2);
            }

            pii q2(p.first, new_min-1);
            if (q2.first > q2.second) return;
            cur[c] = q2;
        }
        if (op == '<') {
            pii p = cur[c];

            ll new_max = min(x-1, p.second);
            pii q(p.first, new_max);
            if (q.first <= q.second) {
                map<char, pair<ll, ll>> cur2 = cur;
                cur2[c] = q;
                pass(nxt, cur2);
            }

            pii q2(new_max+1, p.second);
            if (q2.first > q2.second) return;
            cur[c] = q2;
        }
    }
}

void solve() {
    while(getline(cin, s) && s.size() != 0) {
        stringstream ss ;
        ss << s;
        string name, rest;
        ss >> name;
        rest = s.substr(name.size());
        mp[name] = rest;
    }
    char c;
    int x;
    ll ans = 0;
    ll now = 0;
    while(cin >> c >> x) {
        now = 0;
        // cur.clear();
        now += x;
        cin >> c >> x; 
        cin >> c >> x; 
        cin >> c >> x; 
    }

    map<char, pair<ll, ll>> cur;
    cur['x'] = pii(1, 4000);
    cur['m'] = pii(1, 4000);
    cur['a'] = pii(1, 4000);
    cur['s'] = pii(1, 4000);
    pass("in", cur);

    ans = 0;

    cout << glob << '\n';

} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(2) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}