#include <bits/stdc++.h>
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

map<ll, vector<pair<string, ll>>> mp;

void solve() {
    ll ans = 0;
    string s;
    while(cin >> s) {
        ll cur = 0;
        for (char c : s) {
            cur += c;
            cur *= 17;
            cur %= 256;
        }
        string t;
        cin >> t;
        if (t == "-") {
            rep(i, 0, mp[cur].size()) {
                if (mp[cur][i].first == s) {
                    mp[cur].erase(mp[cur].begin()+i);
                    break;
                }
            }
        } else {
            t = t.substr(1);
            bool found = false;
            rep(i, 0, mp[cur].size()) {
                if (mp[cur][i].first == s) {
                    mp[cur][i].second = stoi(t);
                    found = true;
                    break;
                }
            }
            if (!found) {
                mp[cur].pb(make_pair(s, stoi(t)));
            }

        }
    }

    for (auto p : mp) {
        ll i = 1;
        for (auto q : p.second) {
            ans += (p.first+1)*(i)*q.second;
            i++;
        }
    }

    cout << ans << '\n';
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}