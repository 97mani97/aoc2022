#include <bits/stdc++.h>
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

vector<string> mat;
map<pii, ll> vis;
set<pair<double, double>> vis_dbl;
set<pair<double, double>> out;
set<pair<double, double>> out_double;

vector<string> new_mat;

const double mxs = 200;

void outside(double i, double j) {
    int ii = (int)(round(i)), jj = (int)(round(j));

    if (min(i, j) < -1 || max(i, j) >= mxs) return;

    if (vis_dbl.count(make_pair(i, j))) return;
    if (out_double.count(make_pair(i, j))) return;
    out_double.insert(make_pair(i, j));

    if (round(i) != i && round(j) != j) out.insert(make_pair(i, j));

    outside(i+0.5, j);
    outside(i-0.5, j);
    outside(i, j-0.5);
    outside(i, j+0.5);
}

ll computeArea() {
    for (auto p : vis) {
        int i = p.first.first, j = p.first.second;
        double ii = p.first.first + 0.5, jj = p.first.second + 0.5;


        vis_dbl.insert(make_pair(ii, jj));

        if (mat[i][j] == '|') vis_dbl.insert(make_pair(i, j+0.5)), vis_dbl.insert(make_pair(i+1.0, j+0.5));
        if (mat[i][j] == '-') vis_dbl.insert(make_pair(i+0.5, j)), vis_dbl.insert(make_pair(i+0.5, j+1.0));
        if (mat[i][j] == 'L') vis_dbl.insert(make_pair(i, j+0.5)), vis_dbl.insert(make_pair(i+0.5, j+1.0));
        if (mat[i][j] == 'J') vis_dbl.insert(make_pair(i, j+0.5)), vis_dbl.insert(make_pair(i+0.5, j));
        if (mat[i][j] == '7') vis_dbl.insert(make_pair(i+0.5, j)), vis_dbl.insert(make_pair(i+1.0, j+0.5));
        if (mat[i][j] == 'F') vis_dbl.insert(make_pair(i+1.0, j+0.5)), vis_dbl.insert(make_pair(i+0.5, j+1.0));
    }

    ll mxi = -1, mxj = -1;
    for (auto p : vis) mxi = max(mxi, p.first.first), mxj = max(mxj, p.first.second);

    ll res = 0;
    outside(mxs-0.5, mxs-0.5);
    res = (mxs+1)*(mxs+1) - out.size() - vis.size();

    return res;
}


void solve() {
    string s;
    while(getline(cin, s)) {
        mat.pb(s);
    }

    int n = mat.size();
    int m = mat[0].size();

    pii start;
    rep(i, 0, n) rep(j, 0, m) {
        if (mat[i][j] == 'S') {
            start = pii(i, j);
            mat[i][j] = 'J';
            break;
        }
    }

    queue<pii> toVis;
    toVis.push(start);
    ll d = 0;
    vis[start] = d;

    while(toVis.size()) {
        int k = toVis.size();
        while(k--) {
            pii q = toVis.front(); toVis.pop();
            int i = q.first, j = q.second;
            if (mat[i][j] == '|') {
                if (vis.count(pii(i+1, j)) == 0) {
                    vis[pii(i+1, j)] = d+1;
                    toVis.push(pii(i+1, j));
                }
                if (vis.count(pii(i-1, j)) == 0) {
                    vis[pii(i-1, j)] = d+1;
                    toVis.push(pii(i-1, j));
                }

            }
            if (mat[i][j] == '-') {
                if (vis.count(pii(i, j+1)) == 0) {
                    vis[pii(i, j+1)] = d+1;
                    toVis.push(pii(i, j+1));
                }
                if (vis.count(pii(i, j-1)) == 0) {
                    vis[pii(i, j-1)] = d+1;
                    toVis.push(pii(i, j-1));
                }
            }
            if (mat[i][j] == 'J') {
                if (vis.count(pii(i-1, j)) == 0) {
                    vis[pii(i-1, j)] = d+1;
                    toVis.push(pii(i-1, j));
                }
                if (vis.count(pii(i, j-1)) == 0) {
                    vis[pii(i, j-1)] = d+1;
                    toVis.push(pii(i, j-1));
                }
            }
            if (mat[i][j] == '7') {
                if (vis.count(pii(i+1, j)) == 0) {
                    vis[pii(i+1, j)] = d+1;
                    toVis.push(pii(i+1, j));
                }
                if (vis.count(pii(i, j-1)) == 0) {
                    vis[pii(i, j-1)] = d+1;
                    toVis.push(pii(i, j-1));
                }
            }
            if (mat[i][j] == 'F') {
                if (vis.count(pii(i+1, j)) == 0) {
                    vis[pii(i+1, j)] = d+1;
                    toVis.push(pii(i+1, j));
                }
                if (vis.count(pii(i, j+1)) == 0) {
                    vis[pii(i, j+1)] = d+1;
                    toVis.push(pii(i, j+1));
                }
            }
            if (mat[i][j] == 'L') {
                if (vis.count(pii(i-1, j)) == 0) {
                    vis[pii(i-1, j)] = d+1;
                    toVis.push(pii(i-1, j));
                }
                if (vis.count(pii(i, j+1)) == 0) {
                    vis[pii(i, j+1)] = d+1;
                    toVis.push(pii(i, j+1));
                }
            }
        }
        d++;
    }


    ll ans = computeArea();
    cout << ans << '\n';

} 


// 547

int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(2) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}