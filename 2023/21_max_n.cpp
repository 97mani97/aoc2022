#include <bits/stdc++.h>
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

int n, m;
vector<string> mat, mat_original;

map<int, set<pii>> odds;
map<int, set<pii>> evens;
ll oddSize[1000];

void moveEvens(int i) {
    set<pii> st = evens[i];
    set<pii> ans;
    for (pii p : st) {
        int x = p.first, y = p.second;
        if (x > 0 && mat[x-1][y] == '.') ans.insert(pii(x-1, y));
        if (y > 0 && mat[x][y-1] == '.') ans.insert(pii(x, y-1));
        if (x+1 < n && mat[x+1][y] == '.') ans.insert(pii(x+1, y));
        if (y+1 < m && mat[x][y+1] == '.') ans.insert(pii(x, y+1));
    }
    odds[i+1] = ans;
}

void moveOdds(int i) {
    set<pii> st = odds[i];
    set<pii> ans;
    for (pii p : st) {
        int x = p.first, y = p.second;
        if (x > 0 && mat[x-1][y] == '.') ans.insert(pii(x-1, y));
        if (y > 0 && mat[x][y-1] == '.') ans.insert(pii(x, y-1));
        if (x+1 < n && mat[x+1][y] == '.') ans.insert(pii(x+1, y));
        if (y+1 < m && mat[x][y+1] == '.') ans.insert(pii(x, y+1));
    }
    evens[i+1] = ans;
}

ll fromStart(pii start, ll steps) {
    odds.clear();
    evens.clear();
    evens[0] = {start};
    for (int i = 0; i < steps; i += 2) {
        moveEvens(i);
        moveOdds(i+1);
    }
    rep(i, 0, steps) oddSize[i] = odds[i].size();
    return odds[steps-1].size();
}

ll getHowMany(pii start, ll goal) {
    odds.clear();
    evens.clear();
    evens[0] = {start};
    for (ll i = 0; ; i += 2) {
        moveEvens(i);
        moveOdds(i+1);
        if (odds[i+1].size() == goal) return i+1;
    }
}

void moveEvens2(int i) {
    set<pii> st = evens[i];
    set<pii> ans;
    for (pii p : st) {
        int x = p.first, y = p.second;
        if (mat[Mod(x-1, n)][Mod(y, m)] == '.' && odds[i-1].count(pii(x-1, y)) == 0) ans.insert(pii(x-1, y));
        if (mat[Mod(x, n)][Mod(y-1, m)] == '.' && odds[i-1].count(pii(x, y-1)) == 0) ans.insert(pii(x, y-1));
        if (mat[Mod(x+1, n)][Mod(y, m)] == '.' && odds[i-1].count(pii(x+1, y)) == 0) ans.insert(pii(x+1, y));
        if (mat[Mod(x, n)][Mod(y+1, m)] == '.' && odds[i-1].count(pii(x, y+1)) == 0) ans.insert(pii(x, y+1));
    }
    odds[i+1] = ans;
}

void moveOdds2(int i) {
    set<pii> st = odds[i];
    set<pii> ans;
    for (pii p : st) {
        int x = p.first, y = p.second;
        if (mat[Mod(x-1, n)][Mod(y, m)] == '.' && evens[i-1].count(pii(x-1, y)) == 0) ans.insert(pii(x-1, y));
        if (mat[Mod(x, n)][Mod(y-1, m)] == '.' && evens[i-1].count(pii(x, y-1)) == 0) ans.insert(pii(x, y-1));
        if (mat[Mod(x+1, n)][Mod(y, m)] == '.' && evens[i-1].count(pii(x+1, y)) == 0) ans.insert(pii(x+1, y));
        if (mat[Mod(x, n)][Mod(y+1, m)] == '.' && evens[i-1].count(pii(x, y+1)) == 0) ans.insert(pii(x, y+1));
    }
    evens[i+1] = ans;
}

ll fromStart2(pii start, ll steps) {
    odds.clear();
    evens.clear();
    evens[0] = {start};
    for (int i = 0; i <= steps; i += 2) {
        moveEvens2(i);
        moveOdds2(i+1);
    }
    ll ans = 0;
    for (int i = 1; i <= steps; i += 2) ans += odds[i].size();
    return ans;


    return odds[steps].size();
}

int xi, yi;

ll bruteforce(ll steps) {
    ll ret = fromStart2(pii(xi, yi), steps);
    set<pii> all_pts;
    for (int i = 1; i <= steps; i += 2) for (auto p : odds[i]) all_pts.insert(p);

    ll ans1 = 0;
    ll other_1 = 0;
    ll corner_1 = 0; 
    ll side_1 = 0;
    ll strictly_right = 0;
    ll strictly_left = 0;
    ll strictly_above = 0;
    ll strictly_below = 0;
    ll strictly_above_right = 0;
    ll strictly_below_right = 0;
    assert(n == m);

    ll mxj = 0;

    for (auto p : all_pts) {
        ll i = p.first, j = p.second;
        if (i >= 0 && i < n && j >= 0 && j < n) ans1++;
        else {
            if (i >= 0 && i < n && j >= n && j < 2*n) other_1++;
            if (i >= n && i < 3*n && j >= n && j < 3*n) corner_1++;
            if (i >= 0 && i < n && j >= 2*n*4 && j < 2*n*4 + 2*n) side_1++;
            if (i >= 0 && i < n && j >= n ) strictly_right++;
            if (i >= 0 && i < n && j < 0 ) strictly_left++;
            if (j >= 0 && j < n && i >= n ) strictly_below++;
            if (j >= 0 && j < n && i < 0 ) strictly_above++;
            if (i < 0 && j >= n ) strictly_above_right++;
            if (i >= n+ 2*n*0 && j >= n ) strictly_below_right++;
            // if (i >= n + 1*2*n && i < 3*n + 1*2*n && j >= n + 0*2*n && j < 3*n + 0*2*n) strictly_below_right++;
        }
        mxj = max(mxj, j);
    }

    cout << "ans1 = " << ans1 << '\n';
    cout << "other_1 = " << other_1 << '\n';
    cout << "corner_1 = " << corner_1 << '\n';
    cout << "side_1 = " << side_1 << '\n';
    cout << "mjx = " << mxj << '\n';
    cout << "strictly_right = " << strictly_right << '\n';
    cout << "strictly_left = " << strictly_left << '\n';
    cout << "strictly_below = " << strictly_below << '\n';
    cout << "strictly_above = " << strictly_above << '\n';
    cout << "strictly_below_right = " << strictly_below_right << '\n';
    cout << "strictly_above_right = " << strictly_above_right << '\n';

    return ret;
}

ll solve(ll steps) {
    string s;
    while(cin >> s) mat.pb(s);
    n = mat.size(), m = mat[0].size();
    pii start;
    rep(i, 0, n) rep(j, 0, m) if (mat[i][j] == 'S') {
        start = pii(i, j);
        xi = i;
        yi = j;
        mat[i][j] = '.';
    }
    mat_original = mat;

    cout << "start = " << start.first << ' ' << start.second << '\n';

    ll ans = 0; 

    ll little_square = fromStart(start, 502);
    assert(little_square == fromStart(start, 502));
    assert(little_square == fromStart(start, 504));

    // compute sides right
    mat = mat_original;
    n = mat.size(), m = mat[0].size();
    rep(i, 0, n) mat[i] += mat[i];
    n = mat.size(), m = mat[0].size();

    cout << n << ' ' << m << '\n';

    ll reachable = 15344;
    ll steps_to_reach_in_box = 325;
    assert(reachable == fromStart(pii(start.first, 0), 500));
    assert(reachable == fromStart(pii(start.first, 0), 502));
    assert(reachable == fromStart(pii(start.first, m-1), 500));
    assert(steps_to_reach_in_box == getHowMany(pii(start.first, 0), reachable));

    ll other_square2 = reachable - little_square;

    ll get_to_11 = 66;

    fromStart(pii(start.first, 0), steps_to_reach_in_box+10); // UNCOMMENT

    // cout << "printing odds\n";
    // for (int i = 1; i <= 31; i+=2) cout << " i = " << i << " odssi = " << odds[i].size() << '\n';
    // cout << "printing evens\n";
    // for (int i = 0; i <= 30; i+=2) cout << " i = " << i << " evens = " << evens[i].size() << '\n';

    ll strictly_right = 0;

    ll y = 0;
    while(1) { 
        ll cur = get_to_11 + y*m;
        ll left_to_go = steps - cur;

        if (left_to_go < 0) break;

        if(left_to_go - steps_to_reach_in_box >= 0) {
            ans += reachable;
            strictly_right += reachable;
        } else {
            if (left_to_go%2 == 0) left_to_go--;
            ans += odds[left_to_go].size();
            strictly_right += odds[left_to_go].size();
        }
        y++;
    }
    cout << " got strictly_right = " << strictly_right << '\n';

    ll strictly_left = 0;

    fromStart(pii(start.first, m-1), steps_to_reach_in_box+10); // UNCOMMENT
    y = 0;
    while(1) { 
        ll cur = get_to_11 + y*m;
        ll left_to_go = steps - cur;

        if (left_to_go < 0) break;

        if(left_to_go - steps_to_reach_in_box >= 0) {
            ans += reachable;
            strictly_left += reachable;
        } else {
            if (left_to_go%2 == 0) left_to_go--;
            ans += odds[left_to_go].size();
            strictly_left += odds[left_to_go].size();
        }
        y++;
    }

    cout << " got strictly_left = " << strictly_left << '\n';

    // compute sides up
    mat = mat_original;
    n = mat.size(), m = mat[0].size();
    rep(i, 0, n) mat.pb(mat[i]);
    n = mat.size(), m = mat[0].size();

    cout << n << ' ' << m << '\n';
  
    reachable = 15344;
    steps_to_reach_in_box = 325;
    assert(reachable == fromStart(pii(0, start.second), 500));
    assert(reachable == fromStart(pii(0, start.second), 502));
    assert(reachable == fromStart(pii(0, start.second), 498));
    assert(reachable == fromStart(pii(n-1, start.second), 500));
    assert(steps_to_reach_in_box == getHowMany(pii(0, start.second), reachable));

    get_to_11 = 66;

    ll other_square = reachable - little_square;

    assert(other_square == other_square2);

    fromStart(pii(0, start.second), steps_to_reach_in_box+10); // UNCOMMENT
    ll strictly_below = 0;
    y = 0;
    while(1) { 
        ll cur = get_to_11 + y*n;
        ll left_to_go = steps - cur;

        if (left_to_go < 0) break;

        if(left_to_go - steps_to_reach_in_box >= 0) {
            ans += reachable;
            strictly_below += reachable;
        } else {
            if (left_to_go%2 == 0) left_to_go--;
            ans += odds[left_to_go].size();
            strictly_below += odds[left_to_go].size();
        }
        y++;
    }
    cout << "strictly_below = " << strictly_below << '\n';

    fromStart(pii(n-1, start.second), steps_to_reach_in_box+10); // UNCOMMENT
    ll strictly_above = 0;
    y = 0;
    while(1) { 
        ll cur = get_to_11 + y*n;
        ll left_to_go = steps - cur;

        if (left_to_go < 0) break;

        if(left_to_go - steps_to_reach_in_box >= 0) {
            ans += reachable;
            strictly_above += reachable;
        } else {
            if (left_to_go%2 == 0) left_to_go--;
            ans += odds[left_to_go].size();
            strictly_above += odds[left_to_go].size();
        }
        y++;
    }
    cout << "strictly_above = " << strictly_above << '\n';
    
    // compute corners

    mat = mat_original;
    n = mat.size(), m = mat[0].size();

    rep(i, 0, n) {
        mat[i] += mat[i];
    }
    rep(i, 0, n) mat.pb(mat[i]);

    n = mat.size(), m = mat[0].size();

    cout << n << ' ' << m << '\n';

    reachable = 30688;
    steps_to_reach_in_box = 521; // getHowMany(pii(0, 0), reachable)

    assert(reachable == fromStart(pii(0, 0), 600));
    assert(reachable == fromStart(pii(0, 0), 602));
    assert(steps_to_reach_in_box == getHowMany(pii(0, 0), reachable));
    assert(steps_to_reach_in_box == getHowMany(pii(n-1, 0), reachable));
    assert(steps_to_reach_in_box == getHowMany(pii(0, n-1), reachable));
    assert(steps_to_reach_in_box == getHowMany(pii(n-1, n-1), reachable));


    get_to_11 = 66+66;

    ll strictly_below_right = 0;

    fromStart(pii(0, 0), steps_to_reach_in_box+10); // UNCOMMENT
    y = 0;
    while(1) { 
        ll nw = 0;


        ll cur = get_to_11 + y*n;
        ll left_to_go = steps - cur;

        if (left_to_go < 0) break;

        // cout << " at y = " << y << " got left_to_go = " << left_to_go << '\n';

        while(left_to_go - steps_to_reach_in_box >= 0) {
            ans += reachable;
            strictly_below_right += reachable;
            nw += reachable;
            left_to_go -= n;
            // cout << " at y = " << y << " adding reachable with cur = " << cur << " left_to_go = " << left_to_go << '\n'; 
        }
        while(left_to_go >= 0) {
            ll c = left_to_go;
            if (c%2 == 0) c--;
            ans += odds[c].size();
            strictly_below_right += odds[c].size();
            nw += odds[c].size();
            // cout << " at y = " << y  << " adding odds " << odds[c].size() << " left_to_go = " << c << " and nw = " << nw << '\n'; 
            left_to_go -= n;
        }

        y++;
    }

    cout << "strictly_below_right = " << strictly_below_right << '\n';

    fromStart(pii(0, n-1), steps_to_reach_in_box+10); // UNCOMMENT
    y = 0;
    ll strictly_below_left = 0;
    while(1) { 
        ll nw = 0;
        ll cur = get_to_11 + y*n;
        ll left_to_go = steps - cur;

        if (left_to_go < 0) break;

        while(left_to_go - steps_to_reach_in_box >= 0) {
            ans += reachable;
            left_to_go -= n;
            strictly_below_left += reachable;
        }
        while(left_to_go >= 0) {
            ll c = left_to_go;
            if (c%2 == 0) c--;
            ans += odds[c].size();
            strictly_below_right += odds[c].size();
            nw += odds[c].size();
            // cout << " at y = " << y  << " adding odds " << odds[c].size() << " left_to_go = " << c << " and nw = " << nw << '\n'; 
            left_to_go -= n;
        }
        y++;
    }
    cout << "strictly_below_left = " << strictly_below_left << '\n';
    fromStart(pii(n-1, 0), steps_to_reach_in_box+10); // UNCOMMENT
    y = 0;
    ll strictly_above_right = 0;
    while(1) { 
        ll nw = 0;
        ll cur = get_to_11 + y*n;
        ll left_to_go = steps - cur;

        if (left_to_go < 0) break;

        while(left_to_go - steps_to_reach_in_box >= 0) {
            ans += reachable;
            left_to_go -= n;
        }
        while(left_to_go >= 0) {
            ll c = left_to_go;
            if (c%2 == 0) c--;
            ans += odds[c].size();
            strictly_below_right += odds[c].size();
            nw += odds[c].size();
            // cout << " at y = " << y  << " adding odds " << odds[c].size() << " left_to_go = " << c << " and nw = " << nw << '\n'; 
            left_to_go -= n;
        }
        y++;
    }
    fromStart(pii(n-1, n-1), steps_to_reach_in_box+10); // UNCOMMENT
    y = 0;
    while(1) { 
        ll nw = 0;
        ll cur = get_to_11 + y*n;
        ll left_to_go = steps - cur;

        if (left_to_go < 0) break;

        while(left_to_go - steps_to_reach_in_box >= 0) {
            ans += reachable;
            left_to_go -= n;
        }
        while(left_to_go >= 0) {
            ll c = left_to_go;
            if (c%2 == 0) c--;
            ans += odds[c].size();
            strictly_below_right += odds[c].size();
            nw += odds[c].size();
            // cout << " at y = " << y  << " adding odds " << odds[c].size() << " left_to_go = " << c << " and nw = " << nw << '\n'; 
            left_to_go -= n;
        }
        y++;
    }

    cout << "little square = " << little_square << '\n';
    cout << "other square = " << other_square << '\n';

    ans += little_square;

    mat = mat_original;
    n = mat.size(), m = mat[0].size();
    return ans;
} 
// 627957378315976
// 627957378332250

int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    ll steps = 26501365;

    ll ans1 = solve(steps); // 22367147
    // ll ans2 = bruteforce(steps);
    cout << ans1 << '\n';
    // cout << ans2 << '\n';
}