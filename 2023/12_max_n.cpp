#include <bits/stdc++.h>
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

vi pa(const string& s) {
    ll n = sz(s);
    vi res;
    rep(i, 0, n) {
        if (isdigit(s[i])) {
            ll j = i, x = 0;
            while (j < n && isdigit(s[j])) {
                x = 10 * x + s[j] - '0';
                j++;
            }
            if (i != 0 && s[i - 1] == '-')
                x *= -1;
            res.pb(x);
            i = j - 1;
        }
    }
    return res;
}

string s;
vi v;
int n, m;

ll dp[5*50][5*10];
ll getdp(int i, int j) {
    if (i == n) {
        if (j == m) return 1;
        return 0;
    }
    if (j == m) {
        bool ok = true;
        rep(x, i, n) if (s[x] == '#') ok = false;
        if (ok) return 1;
        return 0;
    }
    if (dp[i][j] >= 0) return dp[i][j];

    ll res = 0;
    if (s[i] == '#' || s[i] == '?') {
        bool ok = true;
        rep(x, 0, v[j]) if (i+x >= n || s[i+x] == '.') ok = false;
        if (i+v[j] < n && s[i+v[j]] == '#') ok = false;
        if (ok) res += getdp(i+v[j]+1, j+1);
    }
    if (s[i] == '?' || s[i] == '.') {
        res += getdp(i+1, j);
    }
    dp[i][j] = res;
    return res;
}

ll getans(string s_, vi v_) {
    v.clear();
    s = s_;
    v = v_;
    n = s.size();
    m = v.size();
    rep(i, 0, 5*50) rep(j, 0, 5*10) dp[i][j] = -1;
    ll res = getdp(0, 0);
    return res;
}

void solve() {
    ll ans = 0;
    string s, t;
    while(cin >> s) {
        getline(cin, t);
        vi v = pa(t);
        int m = v.size();
        string S = s;
        rep(i, 0, 4) S += "?" + s;
        rep(i, 0, 4) {
            rep(j, 0, m) v.pb(v[j]);
        }

        ans += getans(S, v);
    }
    cout << ans << '\n';
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif
    solve();
}