#include <bits/stdc++.h>
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

int id;
char c;
string s;

map<string, int> loaded = {{"red", 12}, {"green", 13}, {"blue", 14}};

void solve() {
    ll ans = 0;
    while(getline(cin, s)) {
        bool ok = true;
        stringstream ss;
        ss << s;
        ss >> id;
        ll x;
        string color;
        cout << "id = " << id << ' ';
        map<string, ll> fewest;
        while(ss >> x >> color) {
            cout << " x = " << x << " color = " << color << '\n';
            if (color.back() == ';') { // stupid ; parsing is totally unnecessary - why do you make problems like this??
                color.pop_back();
                fewest[color] = max(fewest[color], x);
            } else fewest[color] = max(fewest[color], x);
        }
        ll cur = 1;
        for (auto p : fewest) {
            cout << "game " << id << " p = " << p.first << ' ' << p.second << '\n';
            cur *= p.second;
        }
        ans += cur;
    }

    cout << ans << '\n';
    
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    ll test = 1;
    // cin >> test;
    while(test--) solve();
}
