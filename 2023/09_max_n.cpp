#include <bits/stdc++.h>
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

vi pa(const string& s) {
    ll n = sz(s);
    vi res;
    rep(i, 0, n) {
        if (isdigit(s[i])) {
            ll j = i, x = 0;
            while (j < n && isdigit(s[j])) {
                x = 10 * x + s[j] - '0';
                j++;
            }
            if (i != 0 && s[i - 1] == '-')
                x *= -1;
            res.pb(x);
            i = j - 1;
        }
    }
    return res;
}

ll foo(vi v) {
    ll ans = 0;
    vector<vi> vs = {v};
    vi vp = v;
    while(1) {
        vi nv;
        rep(i, 0, vp.size()-1) nv.pb(vp[i+1]-vp[i]);

        

        vs.pb(nv);

        bool ok = true;
        for (auto x : nv) if (x != 0) ok = false;


        vp = nv;

        if (ok) break;
    }

    vs.back().pb(0);

    int cur = 0;

    for (int i = vs.size()-2; i >= 0; i--) {
        cur = vs[i][0] - cur;
        // vs[i].pb(vs[i].back() + vs[i+1].back());
    }

    return cur;
}

void solve() {
    string s;
    ll ans = 0;
    while(getline(cin, s)) {
        vi v = pa(s);
        ans += foo(v);
    }
    cout << ans << '\n';
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}