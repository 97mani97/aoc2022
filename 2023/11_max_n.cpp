#include <bits/stdc++.h>
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

vector<string> mat;

void solve() {
    string s;
    while(cin >> s) {
        mat.pb(s);
    }
    int n = mat.size();
    int m = mat[0].size();
    int dist[n][m];

    ll D = 1000000;
    rep(i, 0, n) rep(j, 0, m) dist[i][j] = 1;
    rep(i, 0, n) {
        bool ok = true;
        rep(j, 0, m) if (mat[i][j] == '#') ok = false;
        int d;
        if (ok) rep(j, 0, m) dist[i][j] = D; 
    }
    rep(j, 0, m) {
        bool ok = true;
        rep(i, 0, n) if (mat[i][j] == '#') ok = false;
        int d;
        if (ok) rep(i, 0, n) dist[i][j] = D; 
    }
    ll len = 0;
    vector<pii> ps;
    rep(i, 0, n) rep(j, 0, m) if (mat[i][j] == '#') ps.pb(pii(i, j));
    rep(x, 0, ps.size()) rep(y, x+1, ps.size()) {
        pii p = ps[x], q = ps[y];

        ll ans = 0;
        for (int i = min(p.first, q.first); i < max(p.first, q.first); i++) {
            ans += dist[i][p.second];
        }
        for (int j = min(p.second, q.second); j < max(p.second, q.second); j++) {
            ans += dist[p.first][j];
        }
        len += ans;
    }
    cout << len << '\n';
} 

int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(2) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}