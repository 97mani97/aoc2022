#include <bits/stdc++.h>
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

set<string> alls;
map<string, set<string>> g;

set<string> vis;
set<pair<string, string>> edge;

map<pair<string, string>, int> cnt;

void dfs(string u, int dist) {
    if (dist > alls.size()/6) return;
    if (vis.count(u)) return;
    vis.insert(u);
    for (string x : g[u]) {
        if (vis.count(x) == 0) {
            edge.insert(make_pair(min(x, u), max(x, u)));
            dfs(x, dist+1);
        }
    }
}

void components(string u) {
    if (vis.count(u)) return;
    vis.insert(u);
    for (string x : g[u]) {
        components(x);
    }
}

void solve() {
    string s;
    while(getline(cin, s)) {
        stringstream ss;
        ss << s;
        ss >> s;
        string v;
        alls.insert(s);
        while(ss >> v) {
            g[s].insert(v);
            g[v].insert(s);
            alls.insert(v);
        }
    }

    g["ptj"].erase("qmr");
    g["qmr"].erase("ptj");

    g["lsv"].erase("lxt");
    g["lxt"].erase("lsv");

    g["dhn"].erase("xvh");
    g["xvh"].erase("dhn");


    int tot = 0;

    for (string u : alls) {
        vis.clear();
        edge.clear();
        dfs(u, 0);
        for (auto p : edge) cnt[p]++;
        tot++;
        // if (tot >= 10000) break;
    }

    vector<pair<int, pair<string, string>>> v;
    for (auto p : cnt) v.pb(make_pair(p.second, p.first));
    sort(all(v));
    reverse(all(v));

    rep(i, 0, 20) cout << v[i].first << ' ' << v[i].second.first << ' ' << v[i].second.second << '\n';

    vis.clear();
    components("ptj");
    int ans1 = vis.size();
    vis.clear();
    components("qmr");
    int ans2 = vis.size();
    cout << alls.size() << " and component = " << ans1 << ' ' << ans2 << ' ' << ans1*ans2 << '\n';
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}