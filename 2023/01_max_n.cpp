#include <bits/stdc++.h>
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

vi pa(const string& s) {
    ll n = sz(s);
    vi res;
    rep(i, 0, n) {
        if (isdigit(s[i])) {
            ll j = i, x = 0;
            while (j < n && isdigit(s[j])) {
                x = 10 * x + s[j] - '0';
                j++;
            }
            if (i != 0 && s[i - 1] == '-')
                x *= -1;
            res.pb(x);
            i = j - 1;
        }
    }
    return res;
}

string s;
ll ans = 0;

string numbers[9] = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};

void solve() {
    while(getline(cin, s)) {
        vector<ll> cur;
        rep(i, 0, s.size()){
            char c = s[i];
            if (c >= '0' && c <= '9') cur.pb(c-'0');

            ll ind = 1;
            for (string no : numbers) {
                if (s.substr(i, no.size()) == no) {
                    cur.pb(ind);
                }
                ind++;
            }
        }
        ll x = 10*cur.front() + cur.back();
        ans += x;
    }
    cout << ans << '\n';
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    ll test = 1;
    // cin >> test;
    while(test--) solve();
}
