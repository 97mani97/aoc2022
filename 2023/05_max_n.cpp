#include <bits/stdc++.h>
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

vi pa(const string& s) {
    ll n = sz(s);
    vi res;
    rep(i, 0, n) {
        if (isdigit(s[i])) {
            ll j = i, x = 0;
            while (j < n && isdigit(s[j])) {
                x = 10 * x + s[j] - '0';
                j++;
            }
            if (i != 0 && s[i - 1] == '-')
                x *= -1;
            res.pb(x);
            i = j - 1;
        }
    }
    return res;
}

vi seeds;

map<pair<string, string>, vector<pair<ll, pii>>> mp;

ll ans(ll x, string now) {
    if (now == "location") return x;

    // cout << "now = " << now << " x = " << x << '\n';

    ll ret = LLONG_MAX;
    for (auto p : mp) {
        if (p.first.first == now) {
            string nxt = p.first.second;
            bool found = false;
            for (auto q : p.second) {
                ll r = q.first, x1 = q.second.first, x2 = q.second.second;
                if (x >= x2 && x < x2+r) {
                    found = true;
                    ret = min(ret, ans(x1+x-x2, nxt));
                }
            }
            if (!found) ret = min(ret, ans(x, nxt));
        }
    }
    // cout << "now = " << now << " x = " << x << " returning ret = " << ret << '\n';

    return ret;
}

vii interval(ll x, vii from, vii to) {
    vii ret_interval;

    return ret_interval;
}

// 

vii ans2(vii current_interval, string now) {

    return {};
}

ll rev(ll x, string now) {
    if (now == "seed") return x;
    // cout << "begin  = " << now << " x = " << x << '\n';


    ll ret = LLONG_MAX;
    for (auto p : mp) {
        if (p.first.second == now) {
            string nxt = p.first.first;
            bool found = false;
            for (auto q : p.second) {
                ll r = q.first, x1 = q.second.first, x2 = q.second.second;
                if (x >= x1 && x < x1+r) {
                    found = true;
                    ret = min(ret, rev(x2+x-x1, nxt));
                }
            }
            if (!found) ret = min(ret, rev(x, nxt));
        }
    }
    // cout << "now = " << now << " x = " << x << " returning ret = " << ret << '\n';

    return ret;
}
 
void solve() {
    ll mx = -1;
    string s;
    getline(cin, s);
    vi seed_ranges = pa(s);

    for (int i = 0; i < seed_ranges.size(); i += 2) {
        ll x = seed_ranges[i], r = seed_ranges[i+1];
        // for (ll y = x; y < x+r; y++) seeds.pb(y);
        mx = max(mx, x + r);
    }

    for (auto x : seeds) mx = max(mx, x);

    getline(cin, s); // 
    while(getline(cin, s)) {
        stringstream ss;
        ss << s;
        string s1, s2;
        ss >> s1 >> s2;

        vector<pair<ll, pii>> cur;

        while(getline(cin, s) && s.size() > 0) {
            vi ra = pa(s);
            ll x1 = ra[0], x2 = ra[1], r = ra[2];
            cur.pb(make_pair(r, pii(x1, x2)));
            // while(min(x1, x2) < mx) {
            //     cur[x2] = x1;
            //     x2 += r;
            //     x1 += r;
            //     // cout << "here \n";
            // }
        }
        mp[make_pair(s1, s2)] = cur;
    }
    
    ll ret = LLONG_MAX;
    // ll ans = LLONG_MAX;

    for (ll x : seeds) ret = min(ans(x, "seed"), ret);
    for (ll x = 0; ; x++) {
        ll y = rev(x, "location");
        bool ok = false;
        for (int i = 0; i < seed_ranges.size(); i += 2) {
            ll a = seed_ranges[i], r = seed_ranges[i+1];
            if (y >= a && y < a+r) {
                cout << x << '\n';
                return;
            }
        } 
    }

    vii input;
    for (int i = 0; i < seed_ranges.size(); i += 2) {
        ll x = seed_ranges[i], r = seed_ranges[i+1];
        input.pb(make_pair(x, x+r-1));
    }

    vii output = ans2(input, "seeds");
    for (auto p : output) ret = min(ret, p.first);

    cout << ret << '\n';
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}