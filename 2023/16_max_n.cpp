#include <bits/stdc++.h>
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

vi pa(const string& s) {
    ll n = sz(s);
    vi res;
    rep(i, 0, n) {
        if (isdigit(s[i])) {
            ll j = i, x = 0;
            while (j < n && isdigit(s[j])) {
                x = 10 * x + s[j] - '0';
                j++;
            }
            if (i != 0 && s[i - 1] == '-')
                x *= -1;
            res.pb(x);
            i = j - 1;
        }
    }
    return res;
}

vector<string> mat;
bool vis[1000][1000];
set<pair<pii, pii>> true_vis;
int n, m;

pii turnLeft(pii v) {
    return pii(-v.second, v.first);
}
pii turnRight(pii v) {
    return turnLeft(turnLeft(turnLeft(v)));
}

void dfs(pii s, pii v) {
    int x = s.first, y = s.second;
    if (min(x, y) < 0 || x >= n || y >= m) return;

    if (true_vis.count(make_pair(s, v))) return;
    vis[s.first][s.second] = true;
    true_vis.insert(make_pair(s, v));

    pii s1, s2;
    pii v1, v2;

    if (mat[x][y] == '\\'){
        if (v == pii(0, 1)) { // right
            v = turnRight(v);
        } else if (v == pii(1, 0)){ // down
            v = turnLeft(v);
        } else if (v == pii(0, -1)) {
            v = turnRight(v);
        } else {
            v = turnLeft(v);
        }

        s1 = s2 = s;
        v1 = v2 = v;

        s1.first += v1.first;
        s1.second += v1.second;

        s2.first += v2.first;
        s2.second += v2.second;
    } else if (mat[x][y] == '/') {
        if (v == pii(0, 1)) { // right
            v = turnLeft(v);
        } else if (v == pii(1, 0)){ // down
            v = turnRight(v);
        } else if (v == pii(0, -1)) {
            v = turnLeft(v);
        } else {
            v = turnRight(v);
        }

        s1 = s2 = s;
        v1 = v2 = v;

        s1.first += v1.first;
        s1.second += v1.second;

        s2.first += v2.first;
        s2.second += v2.second;
    } else if (mat[x][y] == '|') {
        if (v == pii(0, 1)) { // right
            v1 = turnLeft(v);
            v2 = turnRight(v);
        } else if (v == pii(1, 0)){ // down
            v1 = v2 = v;
        } else if (v == pii(0, -1)) {
            v1 = turnLeft(v);
            v2 = turnRight(v);
        } else {
            v1 = v2 = v;
        }

        s1 = s2 = s;
        s1.first += v1.first;
        s1.second += v1.second;

        s2.first += v2.first;
        s2.second += v2.second;
    } else if (mat[x][y] == '-') {
        if (v == pii(0, 1)) { // right
            v1 = v2 = v;
        } else if (v == pii(1, 0)){ // down
            v1 = turnLeft(v);
            v2 = turnRight(v);
        } else if (v == pii(0, -1)) {
            v1 = v2 = v;
        } else {
            v1 = turnLeft(v);
            v2 = turnRight(v);
        }

        s1 = s2 = s;
        s1.first += v1.first;
        s1.second += v1.second;

        s2.first += v2.first;
        s2.second += v2.second;
    } else {
        s1 = s2 = s;
        v1 = v2 = v;

        s1.first += v1.first;
        s1.second += v1.second;

        s2.first += v2.first;
        s2.second += v2.second;
    }

    // rep(i, 0, n) {
    //     rep(j, 0, m) {
    //         if (vis[i][j]) cout << "#";
    //         else cout << ".";
    //     }
    //     cout << '\n';
    // }

    dfs(s1, v1);
    dfs(s2, v2);
}

void solve() {
    string s;
    while(cin >> s) {
        mat.pb(s);
    }
    n = mat.size();
    m = mat[0].size();


    ll ans = 0;

    rep(i, 0, n) {
        rep(i, 0, n) rep(j, 0, m) {
            vis[i][j] = false;
        }
        true_vis.clear();


        pii start(i, 0);
        pii v(0, 1);

        dfs(start, v);
        ll cur = 0;
        rep(i, 0, n) {
            rep(j, 0, m) {
                if (vis[i][j]) cur++;
            }
        }
        ans = max(ans, cur);
    }
    rep(i, 0, n) {
        rep(i, 0, n) rep(j, 0, m) {
            vis[i][j] = false;
        }
        true_vis.clear();


        pii start(i, m-1);
        pii v(0, -1);

        dfs(start, v);
        ll cur = 0;
        rep(i, 0, n) {
            rep(j, 0, m) {
                if (vis[i][j]) cur++;
            }
        }
        ans = max(ans, cur);
    }
    rep(j, 0, m) {
        rep(i, 0, n) rep(j, 0, m) {
            vis[i][j] = false;
        }
        true_vis.clear();


        pii start(0, j);
        pii v(1, 0);

        dfs(start, v);
        ll cur = 0;
        rep(i, 0, n) {
            rep(j, 0, m) {
                if (vis[i][j]) cur++;
            }
        }
        ans = max(ans, cur);
    }
    rep(j, 0, m) {
        rep(i, 0, n) rep(j, 0, m) {
            vis[i][j] = false;
        }
        true_vis.clear();


        pii start(n-1, j);
        pii v(-1, 0);

        dfs(start, v);
        ll cur = 0;
        rep(i, 0, n) {
            rep(j, 0, m) {
                if (vis[i][j]) cur++;
            }
        }
        ans = max(ans, cur);
    }
    cout << ans << '\n';
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}