#include <bits/stdc++.h>
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

int n, m;
vector<vi> mat;  

typedef struct Node {
    pii start;
    pii v;
    int dist;
} Node;

class ComparisonClass {
public:
    bool operator() (const Node& p1, const Node& p2) {
        return p1.dist > p2.dist;
    }
};

pii turnLeft(pii v) {
    return pii(-v.second, v.first);
}
pii turnRight(pii v) {
    return turnLeft(turnLeft(turnLeft(v)));
}

void solve() {
    string st;
    while(cin >> st) {
        vi w;
        for (char c : st) w.pb(c-'0');
        mat.pb(w);
    }
    n = mat.size(), m = mat[0].size();

    pii start(0, 0);
    priority_queue<Node, vector<Node>, ComparisonClass> pq;
    
    Node s;
    s.dist = 0;
    s.start = start;
    s.v = pii(0, 1);
    pq.push(s);
    s.v = pii(1, 0);
    pq.push(s);

    map<pair<pii, pii>, int> dists;

    while(pq.size()) {
        s = pq.top(); pq.pop();
        int i = s.start.first, j = s.start.second;
        
        if (dists.count(make_pair(s.start, s.v)) > 0 && s.dist > dists[make_pair(s.start, s.v)]) continue;
        if (i == n-1 && j == m-1) {
            cout << s.dist << '\n';
            return;
        }

        dists[make_pair(s.start, s.v)] = s.dist;


        pii v1 = turnLeft(s.v);
        pii v2 = turnRight(s.v);

        int cur = s.dist;
        rep(x, 0, 10) {
            i += v1.first, j += v1.second;
            if (i < 0 || i >= n || j < 0 || j >= m) break;
            else {
                cur += mat[i][j];
                if (x < 3) continue;
                if (dists.count(make_pair(pii(i, j), v1)) == 0 || cur < dists[make_pair(pii(i, j), v1)]) {
                    dists[make_pair(pii(i, j), v1)] = cur;
                    Node new_s;
                    new_s.start = pii(i, j);
                    new_s.v = v1;
                    new_s.dist = cur;
                    pq.push(new_s);
                }   
            }
        }
        cur = s.dist;
        i = s.start.first, j = s.start.second;
        rep(x, 0, 10) {
            i += v2.first, j += v2.second;
            if (i < 0 || i >= n || j < 0 || j >= m) break; 
            else {
                cur += mat[i][j];
                if (x < 3) continue;
                if (dists.count(make_pair(pii(i, j), v2)) == 0 || cur < dists[make_pair(pii(i, j), v2)]) {
                    dists[make_pair(pii(i, j), v2)] = cur;
                    Node new_s;
                    new_s.start = pii(i, j);
                    new_s.v = v2;
                    new_s.dist = cur;
                    pq.push(new_s);
                }   
            }
        }
    }
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}