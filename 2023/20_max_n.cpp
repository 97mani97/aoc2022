#include <bits/stdc++.h>
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

map<string, vector<string>> mp;
map<string, vector<string>> mp_inv;
set<string> isper;

void solve() {
    string s,t;
    while(cin >> s) {
        if (s != "broadcaster") {
            if (s[0] == '%') {
                s = s.substr(1);
                isper.insert(s);
            } else {
                s = s.substr(1);
            }
        }

        getline(cin, t);
        stringstream ss;
        ss << t;
        while(ss >> t) {
            mp[s].pb(t);
            mp_inv[t].pb(s);
        }
    }

    ll lows = 0, highs = 0;

    map<string, bool> ons;

    map<string, map<string, bool>> conjunction;

    for (auto p : isper) ons[p] = false;

    for (auto p : mp_inv) for (auto q : p.second) conjunction[p.first][q] = false;

    int pushes = 1000;

    while(pushes--) {
        queue<pair<string, bool>> q;
        for (string t : mp["broadcaster"]) {
            if (ons.count(t)) {
                if (!ons[t]) q.push(make_pair(t, true));
                else q.push(make_pair(t, false));
                ons[t] = !ons[t];
            }
            lows++;
        }

        lows++;

        while(q.size()) {
            auto p = q.front(); q.pop();
            s = p.first;
            bool high = p.second;

            if (mp[s].size() == 0) continue;

            for (string t : mp[s]) {
                if (high) highs++;
                else lows++;
                if (isper.count(t)) {
                    if (high) continue;
                    if (!ons[t]) q.push(make_pair(t, true));
                    else q.push(make_pair(t, false));
                    ons[t] = !ons[t];
                } else {
                    conjunction[t][s] = high;

                    bool all_high = true;
                    for (auto p : conjunction[t]) if (p.second == false) all_high = false;
                    if (all_high) q.push(make_pair(t, false));
                    else q.push(make_pair(t, true));
                }
            }
        }
    }
    cout << lows << ' ' << highs << '\n';
    cout << highs*lows << '\n';
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}