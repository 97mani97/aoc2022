#include <bits/stdc++.h>
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

vector<string> mat;

int n, m;

map<pii, int> gearCount;
map<pii, ll> gearRatio;

int isStart(int i, int j) {
    if (isdigit(mat[i][j]) && (j == 0 || !isdigit(mat[i][j-1]))) {
        ll sum = 0;
        int ind = 0;
        while(j+ind < m && isdigit(mat[i][j+ind])) {
            sum = 10*sum + (mat[i][j+ind]-'0');
            ind++;
        }
        bool ok = false;
        rep(dj, -1, ind+1) rep(di, -1, 2) {
            if (i+di >= 0 && i+di < n && j+dj >= 0 && j+dj < m) {
                if (dj >= 0 && dj <= ind-1 && di == 0) continue;
                if (mat[i+di][j+dj] != '.') {
                    if (mat[i+di][j+dj] == '*') {
                        gearCount[pii(i+di, j+dj)]++;
                        gearRatio[pii(i+di, j+dj)]*= sum;
                    }
                    ok = true; 
                }
            }
        }
        if (ok) return sum;
        return 0;
    } else {
        return 0;
    }
}

void solve() {
    string s;
    while(getline(cin, s)) mat.pb(s);

    n = mat.size();
    m = mat[0].size();
    rep(i, 0, n) rep(j, 0, m) if (mat[i][j] == '*') gearRatio[pii(i, j)] = 1;

    ll ans = 0;
    rep(i, 0, n) rep(j, 0, m)  ans += isStart(i, j);
    cout << "part1 : "<< ans << '\n';

    ans = 0;
    rep(i, 0, n) rep(j, 0, n) if (gearCount[pii(i, j)] == 2) ans += gearRatio[pii(i, j)];
    cout << "part2 : " << ans << '\n';

} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    ll test = 1;
    // cin >> test;
    while(test--) solve();
}