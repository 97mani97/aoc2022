#include <bits/stdc++.h>
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

// The code below is more complicated than necessary (story of my life)
// but really I was ready to handle the a1 + x1*b1 = ... = an + xn*bn case
// for unknown x1, ..., xn, while minimizing (a1 + x1*b1).
// When inspecting the output, I saw that a1 = b1, ..., an = bn, so it is just the lcm of all of them.
// Like the curry in my fridge, cur2 is left unused and unwanted.

map<string, pair<string, string>> mp;

void solve() {
    string ins;
    string s1, s2, s3;
    cin >> ins;
    while(cin >> s1 >> s2 >> s3) mp[s1] = make_pair(s2, s3);

    vector<pii> v;
    ll ans = -1;
    for (auto p : mp) {
        s1 = p.first;
        if (s1.back() != 'A') continue;
        ll cur = 0;
        int ind = 0;
        while(s1.back() != 'Z') {
            if (ins[ind] == 'L') s1 = mp[s1].first;
            else s1 = mp[s1].second;
            cur++;
            ind = Mod(ind+1, ins.size());
        }

        // The rest is WET code (I did not know this was a real thing, but it is, see the wikipedia)
        // This code is left as is for nostalgic and historic reasons.

        ll cur2 = 0;
        if (ins[ind] == 'L') s1 = mp[s1].first;
        else s1 = mp[s1].second;
        cur2++;
        ind = Mod(ind+1, ins.size());
        while(s1.back() != 'Z') {
            if (ins[ind] == 'L') s1 = mp[s1].first;
            else s1 = mp[s1].second;
            cur2++;
            ind = Mod(ind+1, ins.size());
        }
        v.pb(pii(cur, cur2));
        cout << cur << ' ' << cur2 << '\n';
        
        if (ans == -1) ans = cur;
        else ans = ans*cur/gcd(ans, cur);
    }
    cout << ans << '\n';


} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}