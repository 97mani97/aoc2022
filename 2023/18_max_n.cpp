#include <bits/stdc++.h>
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;


template <class T> int sgn(T x) { return (x > 0) - (x < 0); }
template<class T>
struct Point {
    typedef Point P;
    T x, y;
    explicit Point(T x=0, T y=0) : x(x), y(y) {}
    bool operator<(P p) const { return tie(x,y) < tie(p.x,p.y); }
    bool operator==(P p) const { return tie(x,y)==tie(p.x,p.y); }
    P operator+(P p) const { return P(x+p.x, y+p.y); }
    P operator-(P p) const { return P(x-p.x, y-p.y); }
    P operator*(T d) const { return P(x*d, y*d); }
    P operator/(T d) const { return P(x/d, y/d); }
    T dot(P p) const { return x*p.x + y*p.y; }
    T cross(P p) const { return x*p.y - y*p.x; }
    T cross(P a, P b) const { return (a-*this).cross(b-*this); }
    T dist2() const { return x*x + y*y; }
    double dist() const { return sqrt((double)dist2()); }
    // angle to x-axis in interval [-pi, pi]
    double angle() const { return atan2(y, x); }
    P unit() const { return *this/dist(); } // makes dist()=1
    P perp() const { return P(-y, x); } // rotates +90 degrees
    P normal() const { return perp().unit(); }
    // returns point rotated 'a' radians ccw afloor the origin
    P rotate(double a) const {
        return P(x*cos(a)-y*sin(a),x*sin(a)+y*cos(a)); }
    friend ostream& operator<<(ostream& os, P p) {
        return os << "(" << p.x << "," << p.y << ")"; }
};

template<class P> bool onSegment(P s, P e, P p) {
    return p.cross(s, e) == 0 && (s - p).dot(e - p) <= 0;
}


template<class P>
bool inPolygon(vector<P> &p, P a, bool strict = true) {
    int cnt = 0, n = sz(p);
    rep(i,0,n) {
        P q = p[(i + 1) % n];
        if (onSegment(p[i], q, a)) return !strict;
        //or: if (segDist(p[i], q, a) <= eps) return !strict;
        cnt ^= ((a.y<p[i].y) - (a.y<q.y)) * a.cross(p[i], q) > 0;
    }
    return cnt;
}

char c;
ll a;
string s;

ll fromHex(string s) {
    ll res = 0;
    for (char c : s) {
        ll cur = 0;
        if (c >= '0' && c <= '9') cur += (c-'0');
        else cur += (c-'a') + 10LL;
        res = res*16LL + cur;
    }
    return res;
}

set<ll> xs, ys;



// 0 1
// 0 5411

void solve() {
    Point<ll> cur(0, 0);
    vector<Point<ll>> polygon = {cur};
    while(cin >> c >> a >> s) {
        c = s.back();
        if (c == '0') c = 'R';
        if (c == '1') c = 'D';
        if (c == '2') c = 'L';
        if (c == '3') c = 'U';

        s = s.substr(0, s.size()-1);
        // cout << fromHex(s) << '\n';
        a = fromHex(s);
        if (c == 'R') {
            cur.x += a;
        } else if (c == 'D') {
            cur.y += a;
        } else if (c == 'L') {
            cur.x -= a;
        } else if (c == 'U') {
            cur.y -= a;
        }
        polygon.pb(cur);
        xs.insert(cur.x);
        ys.insert(cur.y);
        cout << "got cur = " << cur << '\n';
    }

    map<ll, ll> mpx, mpy;
    map<ll, ll> mpx_rev, mpy_rev;
    ll ind = 0;
    for (ll x : xs) {
        // if (ind == ys.size()-1) break;
        mpx[x] = ind;
        mpx_rev[ind] = x;
        // cout << "x = " << x << " to ind = " << ind << '\n';
        ind++;
    }
    ind = 0;
    for (ll y : ys) {
        // if (ind == ys.size()-1) break;
        mpy[y] = ind;
        mpy_rev[ind] = y;
        // cout << "y = " << y << " to ind = " << ind << '\n';
        ind++;
    }

    map<ll, ll> diffx, diffy;
    rep(i, 0, mpx.size()-1) {
        diffx[i] = mpx_rev[i+1] -  mpx_rev[i]-1;
        // cout << "diffx i = " << i << " to = " <<  diffx[i] << '\n';
    }
    rep(i, 0, mpy.size()-1) {
        diffy[i] = mpy_rev[i+1] -  mpy_rev[i]-1;
        // cout << "diffy j = " << i << " to = " <<  diffy[i] << '\n';
    }

    vector<Point<double>> polygon_dbl;

    for (auto& p : polygon) {
        p.x = mpx[p.x];
        p.y = mpy[p.y];

        Point<double> p_dbl(p.x, p.y);
        polygon_dbl.pb(p_dbl);
        // cout << "at p_dbl = " << p_dbl << '\n';
    }

    set<Point<double>> allCorners;

    ll ans = 0;
    ll corners = 0;
    ll sides = 0;
    ll insides = 0;

    double mxn = 1000;
    for (double i = -mxn; i <= mxn; i+=1) for (double j = -mxn; j <= mxn; j+=1) {
        ll cur = 1;
        bool ok = false;
        Point<double> p(i, j), q1(i-1, j), q2(i, j-1), q3(i-1, j-1), q(i-0.5, j-0.5);

        if (inPolygon(polygon_dbl, p, false)) {
            // cout << "got corner at p = " << p << '\n';
            allCorners.insert(p);
        }

        if (inPolygon(polygon_dbl, q, false)) {
            cur = (diffx[i-1])*(diffy[j-1]);
            // cout << "got one at q = " << q << " with diffx = " << diffx[i-1] << " diffy = " << diffy[j-1] << " and cur " << cur << '\n';
            ok = true;
            
        }
        if (ok) {
            // cout << '\n';
            insides += cur;
        }
    }
    corners = allCorners.size();

    for (Point<double> c1 : allCorners) for (Point<double> c2 : allCorners) {
        if ((c1-c2).dist() == 1 && (c1 < c2) && inPolygon(polygon_dbl, (c1+c2)*0.5, false)) {
            if (c2.x - c1.x == 1) {
                // cout << " got side at c1 = " << c1 << " c2 = " << c2 << " diffx = " << diffx[c1.x] << '\n';
                sides += diffx[c1.x];
            }
            else {
                // cout << " got side at c1 = " << c1 << " c2 = " << c2 << " diffy = " << diffy[c1.y] << '\n';
                sides += diffy[c1.y];
            }

        }
    }

    cout << "corners = " << corners << '\n';
    cout << "sides =  " << sides << '\n';
    cout << "insides = " << insides << '\n';
    // cout << "should be insides = " << insides + 
    ans = corners + sides + insides;
    cout << ans << '\n';
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(2) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}