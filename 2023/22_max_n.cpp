#include <bits/stdc++.h>
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

vi pa(const string& s) {
    ll n = sz(s);
    vi res;
    rep(i, 0, n) {
        if (isdigit(s[i])) {
            ll j = i, x = 0;
            while (j < n && isdigit(s[j])) {
                x = 10 * x + s[j] - '0';
                j++;
            }
            if (i != 0 && s[i - 1] == '-')
                x *= -1;
            res.pb(x);
            i = j - 1;
        }
    }
    return res;
}

vector<pii> xs, ys, zs;
map<pii, vector<pii>> mp;

set<ll> g[1300];
set<ll> g_rev[1300];

ll height[1300];

ll getHeight(ll i) {
    if (height[i] >= 0) return height[i];
    ll mxh = 0;
    for (ll u : g_rev[i]) mxh = max(mxh, getHeight(u));
    ll res = mxh + zs[i].second - zs[i].first + 1;
    height[i] = res;
    return res;
}

set<ll> touches[1300];
set<ll> touches_rev[1300];

set<ll> getans2(ll i) {
    set<ll> removed = {i};

    queue<ll> q;
    q.push(i);

    while(q.size()) {
        ll u = q.front(); q.pop();
        for (ll v : touches[u]) {
            set<ll> st_rev = touches_rev[v];
            bool ok = true;
            for (ll x : st_rev) if (removed.count(x) == 0) ok = false;

            if (ok) {
                removed.insert(v);
                q.push(v);
            } 
        }
    }
    return removed;
}

void solve() {
    ll x1, y1, z1, x2, y2, z2;
    ll ind = 0;
    while(cin >> x1 >> y1 >> z1 >> x2 >> y2 >> z2) {
        xs.pb(pii(x1, x2));
        ys.pb(pii(y1, y2));
        zs.pb(pii(z1, z2));

        assert(x1 <= x2);
        assert(y1 <= y2);
        assert(z1 <= z2);


        rep(x, x1, x2+1) rep(y, y1, y2+1) mp[pii(x, y)].pb(pii(z1, ind));
        ind++;
    }

    int ans = 0;
    int n = ind;
    for (auto& p : mp) sort(all(p.second));

    for (auto p : mp) {
        vii v = p.second;
        rep(i, 0, v.size()-1) {
            assert(v[i].first <= v[i+1].first);
            g[v[i].second].insert(v[i+1].second);
            g_rev[v[i+1].second].insert(v[i].second);
        }
    }

    rep(i, 0, n) height[i] = -1;

    rep(i, 0, n) {
        bool ok = true;
        for (int v : g[i]) {
            map<ll, ll> heights;
            for (int j : g_rev[v]) {
                heights[getHeight(j)]++;
            }
            if (heights[getHeight(v)-(zs[v].second-zs[v].first+1)] == 1 && getHeight(v)-(zs[v].second-zs[v].first+1) == getHeight(i)) {
                ok = false;
            }

            if (getHeight(v)-(zs[v].second-zs[v].first+1) == getHeight(i)) {
                touches[i].insert(v);
                touches_rev[v].insert(i);
            }
        }
        if (ok) ans++;
    }

    cout << ans << '\n';

    ll ans2 = 0;
    rep(i, 0, n) {
        ans2 += getans2(i).size()-1;
    }
    cout << ans2 << '\n';


} 

// 1142
// 1023
// 1024

int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}