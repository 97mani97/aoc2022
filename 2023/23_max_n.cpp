#include <bits/stdc++.h>
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;


const int mxn = 150;

int n, m;
vector<string> mat;
map<pii, vector<pair<int, pii>>> g;
bool vis[mxn][mxn];

map<char, pii> mp = {
    {'>', {0, 1}},
    {'v', {1, 0}},
    {'<', {0, -1}},
    {'^', {-1, 0}}
};

void connect(pii start, pii p, int dist) {
    if (p.first < 0 || p.first >= n || p.second < 0 || p.second >= m) return;
    if (mat[p.first][p.second] == '#') return;

    if (p != start && g.count(p) > 0) {
        g[start].pb(make_pair(dist, p));
        return;
    }
    if (vis[p.first][p.second]) return;
    vis[p.first][p.second] = true;

    for (auto q : mp) {
        pii d = q.second;
        auto new_p = p;
        new_p.first += d.first;
        new_p.second += d.second;
        connect(start, new_p, dist+1);
    }
}

int cnt(int i, int j) {
    if (i < 0 || j < 0 || i >= n || j >= m) return 0;
    if (mat[i][j] != '#') return 1;
    return 0;
}

pii begin_u, end_u;
map<pii, int> ans;
set<pii> visited;

int longest_path(pii p) {
    // if (ans.count(p)) return ans[p];
    if (p == end_u) return 0;
    if (visited.count(p)) return INT_MIN;
    visited.insert(p);
    int res = 0;
    for (auto q : g[p]) {
        res = max(res, q.first + longest_path(q.second));
    }
    visited.erase(p);
    // ans[p] = res;
    return res;
}

void solve() {
    string s;
    while(cin >> s) {
        mat.pb(s);
    }
    n = mat.size(), m = mat[0].size();


    rep(j, 0, m) {
        if (mat[0][j] == '.') {
            begin_u = pii(0, j);
            g[pii(0, j)] = {};
        }
        if (mat[n-1][j] == '.') {
            end_u = pii(n-1, j);
            g[pii(n-1, j)] = {};
        }
    }

    rep(i, 0, n) rep(j, 0, m) {
        if (mat[i][j] != '.') continue;
        int tot = cnt(i-1, j) + cnt(i+1, j) + cnt(i, j-1) + cnt(i, j+1);
        if (tot > 2) g[pii(i, j)] = {};
    }

    rep(i, 0, n) rep(j, 0, m) if (g.count(pii(i, j))) {
        rep(i, 0, mxn) rep(j, 0, mxn) vis[i][j] = false;
        connect(pii(i, j), pii(i, j), 0);
    }
    cout << longest_path(begin_u) << '\n';
} 

int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}