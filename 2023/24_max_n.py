import sys
from decimal import *
import math

L = []

mn = 200000000000000
mx = 400000000000000
no = 300

with open('input.txt') as f:
    for line in f:
        int_list = [int(i) for i in line.split()]
        L.append(int_list)


   # vx = l[0] + 

ans = 0

def dot(x1, y1, x2, y2):
    return x1*x2 + y1*y2

def onLine(x1, y1, x2, y2, x3, y3):
    vx1 = x2-x1
    vy1 = y2-y1
    vx2 = x3-x1
    vy2 = y3-y1

    if vx1*vy2 - vx2*vy1 == 0:
        return True
    return False

"""
p1 + t1*v1
"""

for i in range(no):
    for j in range(i+1, no):
        x1 = L[i][0]
        y1 = L[i][1]
        x2 = x1+L[i][3]
        y2 = y1+L[i][4]

        x3 = L[j][0]
        y3 = L[j][1]
        x4 = x3+L[j][3]
        y4 = y3+L[j][4]
        if ((x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4) == 0):
            #print(f"p1 = ({L[i][0]}, {L[i][1]}, {L[i][2]}), v1 = ({L[i][3]}, {L[i][4]}, {L[i][5]})")
            #print(f"p2 = ({L[j][0]}, {L[j][1]}, {L[j][2]}), v2 = ({L[j][3]}, {L[j][4]}, {L[j][5]})")
            continue

        px = ((x1*y2 - y1*x2)*(x3 - x4) - (x1 - x2)*(x3*y4 - y3*x4))/((x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4))
        py = ((x1*y2 - y1*x2)*(y3 - y4) - (y1 - y2)*(x3*y4 - y3*x4))/((x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4))

        if (dot(px-x1, py-y1, x2-x1, y2-y1) < 0 or dot(px-x3, py-y3, x4-x3, y4-y3) < 0):
            continue
        if (px >= mn and px <= mx and py >= mn and py <= mx):
            t1 = (px-x1)//L[i][3]
            t2 = (px-x3)//L[j][3]

            print(f"p1 = ({L[i][0]}, {L[i][1]}, {L[i][2]}), v1 = ({L[i][3]}, {L[i][4]}, {L[i][5]})")
            print(f"p2 = ({L[j][0]}, {L[j][1]}, {L[j][2]}), v2 = ({L[j][3]}, {L[j][4]}, {L[j][5]})")
            print(f"px = {px} py = {py}")
            print(f"t = {t1} x= {x1+t1*L[i][3]} y= {y1 + t1*L[i][4]} z= {L[i][2] + t1*L[i][5]}")
            print(f"t = {t2} x= {x3+t2*L[j][3]} y= {y3 + t2*L[j][4]} z= {L[j][2] + t*L[i][5]}")
            ans += 1

print(ans)
