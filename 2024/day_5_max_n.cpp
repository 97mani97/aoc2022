#include <bits/stdc++.h>

#ifdef LOCAL
#include "./cpp-dump/cpp-dump.hpp"
#define pr(x) cpp_dump(x)
#endif

using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

vi pa(const string& s) {
    ll n = sz(s);
    vi res;
    rep(i, 0, n) 
        if (isdigit(s[i])) {
            ll j = i, x = 0;
            while (j < n && isdigit(s[j])) {
                x = 10 * x + s[j] - '0';
                j++;
            }
            if (i != 0 && s[i - 1] == '-')
                x *= -1;
            res.pb(x);
            i = j - 1;
        }
    return res;
}

map<ll, set<ll>> come_before;

ll ans = 0;
string s;

void solve() {
    while(getline(cin, s)){
        if (s.size() == 0) break;
        vi v = pa(s);
        come_before[v[1]].insert(v[0]);
    } 
    while(getline(cin, s)) {
        vi v = pa(s);
        bool ok = true;
        rep(i, 0, v.size()) rep(j, 0, i) 
            if (come_before[v[j]].count(v[i])) ok = false;
            
        if (!ok) {
            sort(all(v), [&come_before](ll x, ll y) {
                return come_before[y].count(x);
            });
            ans += v[sz(v)/2];
        }
    }
    pr(ans);
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}
