#include <bits/stdc++.h>

#ifdef LOCAL
#include "./cpp-dump/cpp-dump.hpp"
#define pr(x) cpp_dump(x)
#endif

using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

ll ans = 0;
string s;

pair<bool, int> check(int i) {
    if (s.substr(i, 4) == "do()") {
        return pair<bool, int>(true, 1);
    } else if (s.substr(i, 7) == "don't()") {
        return pair<bool, int>(true, 0);
    }
    return pair<bool, int>(false, 0);
}


void solve() {
    int dis = 1;
    while(cin >> s) {
        string t;
        for (int i = 0; i < s.size(); ) {
            if (check(i).first) {
                if (check(i).second == 0) {
                    dis = 0;
                    i += 7;
                    continue;
                } else {
                    dis = 1;
                    i += 4;
                }
            }

            if(dis == 1) {
                t += s[i];
            }
            i++;
        }
        s = t;

        for (int i = 0; i < s.size(); ){
            if (s.substr(i, 3) == "mul") {
                i+=3;
                ll x = 0; 
                ll y = 0;
                if (s[i] == '(') {
                    i++;
                    while(isdigit(s[i])) {
                        x = 10 * x + s[i] - '0';
                        i++;
                    }
                    if(s[i] == ',') {
                        i++;
                        while(isdigit(s[i])) {
                            y = 10 * y + s[i] - '0';
                            i++;
                        }
                        if (s[i] == ')') {
                            ans += x*y;
                            i++;
                        }
                    }
                }
            } else i++;
        }
    }
    pr(ans);

} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}
