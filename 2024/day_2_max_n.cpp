#include <bits/stdc++.h>

#ifdef LOCAL
#include "./cpp-dump/cpp-dump.hpp"
#define pr(x) cpp_dump(x)
#endif

using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

vi pa(const string& s) {
    ll n = sz(s);
    vi res;
    rep(i, 0, n) {
        if (isdigit(s[i])) {
            ll j = i, x = 0;
            while (j < n && isdigit(s[j])) {
                x = 10 * x + s[j] - '0';
                j++;
            }
            if (i != 0 && s[i - 1] == '-')
                x *= -1;
            res.pb(x);
            i = j - 1;
        }
    }
    return res;
}
vector<string> pas(const string& s) {
    stringstream ss; ss << s;
    vector<string> ret;
    string t;
    while(ss >> t) ret.pb(t);
    return ret;
}

ll ans = 0;
string s;

bool safe(vi v) {
    bool ok1 = false, ok2 = true;
    vi w = v;
    sort(all(w));
    if (w == v) ok1 = true;
    reverse(all(w));
    if (w == v) ok1 = true;

    rep(i, 0, v.size()-1) {
        ll x = abs(v[i] - v[i+1]);
        if (!(x >= 1 && x <= 3)) ok2 = false;
    }
    if (ok1 && ok2) return true;
    return false;
}

void solve() {
    vi v;
    while(getline(cin, s)) {

        v = pa(s);
        if (safe(v)) {
            ans++;
        } else rep(i, 0, v.size()) {
            vi w;
            rep(j, 0, v.size()) if (j != i) {
                w.pb(v[j]);
            }
            if (safe(w)) {
                ans++;
                break;
            }
        }

    }

    pr(ans);
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}
