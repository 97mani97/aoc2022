#include <bits/stdc++.h>

#ifdef LOCAL
#include "./cpp-dump/cpp-dump.hpp"
#define pr(x) cpp_dump(x)
#endif

using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

vector<vector<string>> vss;
vector<vector<int>> keys, locks;
void solve() {
    string s;
    while(cin.peek() != '\n') {
        vector<string> vs;
        rep(i, 0, 7) {
            cin >> s;
            vs.pb(s);
        }
        vss.pb(vs);
        cin.ignore();
        cin.ignore();

        bool lock = true;
        rep(j, 0, 5) if (vs[0][j] != '#') lock = false;
        vector<int> curs;
        rep(j, 0, 5) {
            int score1 = 0;
            rep(i, 1, 7) {
                if (vs[i][j] == '#') score1++;
                else break;
            }
            int score2 = 0;
            for (int i = 5; i>=0;i--) {
                if (vs[i][j] == '#') score2++;
                else break;
            }
            if (lock) curs.pb(score1);
            else curs.pb(score2);
        } 
        if (lock) locks.pb(curs);
        else keys.pb(curs);
    }

    int ans = 0;
    rep(i, 0, keys.size()) rep(j, 0, locks.size()) {
        bool ok = true;
        rep(k, 0, 5) if (keys[i][k]+locks[j][k] > 5) ok = false;
        if (ok) {
            ans++;
        }
    }
    pr(ans);

} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}
