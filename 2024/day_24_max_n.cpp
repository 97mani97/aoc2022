#include <bits/stdc++.h>

#ifdef LOCAL
#include "./cpp-dump/cpp-dump.hpp"
#define pr(x) cpp_dump(x)
#endif

using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;
ll binaryToDecimal(string n) {
    string num = n;
    ll dec_value = 0;
 
    // Initializing base value to 1, i.e 2^0
    ll base = 1;
 
    ll len = num.length();
    for (int i = len - 1; i >= 0; i--) {
        if (num[i] == '1')
            dec_value += base;
        base = base * 2;
    }
 
    return dec_value;
}
string decToBinary(ll n){// Size of an integer is assumed to be 32 bits
    string res;
    for (int i = 45; i >= 0; i--) {
        ll k = n >> i;
        if (k & 1)
            res.pb('1');
        else
            res.pb('0');
    }
    return res;
}
string adding_a_zero_to_string(int i) {
    string s = to_string(i);
    if (s.size() == 1) s = "0" + s;
    return s;
}

map<string, int> mp;

map<pair<string, string>, string> ANDS, ORS, XORS;
map<pair<string, string>, set<pair<char, string>>> OPS;
map<string, pair<string, string>> ANDS_rev, ORS_rev, XORS_rev;

void OPS_to() {
    ANDS.clear();
    ORS.clear();
    XORS.clear();
    ANDS_rev.clear();
    ORS_rev.clear();
    XORS_rev.clear();
    for (auto p : OPS) {
        string s1 = p.first.first, s2 = p.first.second;
        for (auto q : p.second) {
            if (q.first == '&') {
                ANDS[{s1, s2}] = q.second, ANDS[{s2, s1}] = q.second;
                ANDS_rev[q.second] = p.first;
            }
            if (q.first == '|') {
                ORS[{s1, s2}] = q.second, ORS[{s2, s1}] = q.second;
                ORS_rev[q.second] = p.first;
            }
            if (q.first == '^') {
                XORS[{s1, s2}] = q.second, XORS[{s2, s1}] = q.second;
                XORS_rev[q.second] = p.first;
            }
        }
    }
}

int rec(string s) {
    if (mp.count(s)) return mp[s];
    int res = 0;

    if (ANDS_rev.count(s)) {
        auto p = ANDS_rev[s]; 
        res = rec(p.first) & rec(p.second);
    } else if (ORS_rev.count(s)) {
        auto p = ORS_rev[s]; 
        res = rec(p.first) | rec(p.second);
    } else if (XORS_rev.count(s)) {
        auto p = XORS_rev[s]; 
        res = rec(p.first) ^ rec(p.second);
    } else {
        assert(false);
    }
    return (mp[s] = res);
}

void load_values(string sx, string sy) {
    if (sz(sx) > 0) mp.clear();
    for (int i = 45; i >= 0; i--) {
        string s = to_string(i);
        if (s.size() == 1) s = "0" + s;
        if (sz(sx) > 0) {
            mp["x"+s] = sx[45-i]-'0';
            mp["y"+s] = sy[45-i]-'0';
        }
    }

}

ll get_z(string sx, string sy) {
    vector<pair<string, int>> vs;
    for (int i = 45; i >= 0; i--) {
        string s = "z"+adding_a_zero_to_string(i);
        vs.pb(make_pair(s, rec(s)));
    }

    sort(all(vs));
    string bts = "";
    for (auto p : vs) {
        bts.pb(p.second+'0');
    }
    reverse(all(bts));
    ll ans1 = binaryToDecimal(bts);
    return ans1;
}

pair<string, string> rands() {
    string sx, sy;
    for (int i = 45; i >= 0; i--) {
        int x1 = rand()%2;
        int y1 = rand()%2;
        sx += x1+'0';
        sy += y1+'0';
    }
    return {sx, sy};
}

bool do_some() {
    rep(i, 0, 5) {
        vector<int> diff;
        pair<string, string> randoms = rands();
        string sx = randoms.first, sy = randoms.second;
        
        load_values(sx, sy);

        ll x = binaryToDecimal(sx), y = binaryToDecimal(sy);
        ll z = get_z(sx, sy);
        string sz1 = decToBinary(z);

        ll z2 = x+y;
        string sz2 = decToBinary(z2);

        for (int i = 45; i >= 0; i--) if (sz1[45-i] != sz2[45-i]) {
            diff.pb(i);
        }

        pr(sz1);
        pr(sz2);
        pr(diff);
        pr(" ");
    }

    return true;
}


map<pair<pair<string, string>, char>, set<int>> associations;

void get_associations(string s, int i) {
    char c;
    pair<string, string> p;
    if (ANDS_rev.count(s)) {
        p = ANDS_rev[s]; 
        c = '&';
    } else if (ORS_rev.count(s)) {
        p = ORS_rev[s]; 
        c = '|';
    } else if (XORS_rev.count(s)) {
        p = XORS_rev[s]; 
        c = '^';
    } else {
        return;
        assert(false);
    }
    associations[make_pair(make_pair(p.first, p.second), c)].insert(i);
    get_associations(p.first, i);
    get_associations(p.second, i);
}

void print_those_associatied(int j) {
    associations.clear();
    for (int i = 45; i >= 0; i--) {
        string s = "z"+adding_a_zero_to_string(i);
        get_associations(s, i);
    }

    pr("printing unique associations");
    pr(j);
    for (auto p : associations) {
        if (p.second.count(j)) {
            bool ok = true;
            for (int k = 0; k < j; k++) if (p.second.count(k)) ok = false;
            if (ok) {
                if (p.first.second == '&') pr(make_pair(p.first, ANDS[p.first.first]));
                if (p.first.second == '|') pr(make_pair(p.first, ORS[p.first.first]));
                if (p.first.second == '^') pr(make_pair(p.first, XORS[p.first.first]));
            }
        }
    }
}

void switch_some_pairs(string s1, string s2, char c1, string t1, string u1, string u2, char c2, string t2) {
    pr("switching");
    bool ok = (OPS[{s1, s2}].count(make_pair(c1, t1)) == 1);
    assert(ok);
    ok = (OPS[{u1, u2}].count(make_pair(c2, t2)) == 1);
    assert(ok);

    OPS[{s1, s2}].erase(make_pair(c1, t1));
    OPS[{s2, s1}].erase(make_pair(c1, t1));

    OPS[{u1, u2}].erase(make_pair(c2, t2));
    OPS[{u2, u1}].erase(make_pair(c2, t2));

    OPS[{s1, s2}].insert(make_pair(c1, t2));
    OPS[{s2, s1}].insert(make_pair(c1, t2));
    OPS[{u1, u2}].insert(make_pair(c2, t1));
    OPS[{u2, u1}].insert(make_pair(c2, t1));

    OPS_to();
}

// 

void solve() { 
    string s1, s2, t;
    string op;
    int bt;
    while(cin >> s1 >> bt) {
        mp[s1] = bt;
        cin.ignore();
        if (cin.peek() == '\n') break;
    }
    while(cin >> s1 >> op >> s2 >> t) {
        if (op == "AND") {
            OPS[{s1, s2}].insert(make_pair('&', t));
            OPS[{s2, s1}].insert(make_pair('&', t));
        } else if (op == "OR") {
            OPS[{s1, s2}].insert(make_pair('|', t));
            OPS[{s2, s1}].insert(make_pair('|', t));
        } else if (op == "XOR") {
            OPS[{s1, s2}].insert(make_pair('^', t));
            OPS[{s2, s1}].insert(make_pair('^', t));
        } else assert(false);
    }

    OPS_to();
    rep(j, 0, 46) print_those_associatied(j);
    do_some();
    switch_some_pairs("x09", "y09", '&', "cnk", "x09", "y09", '^', "qwf");
    switch_some_pairs("rkm", "ndq", '^', "vhm", "x14", "y14", '&', "z14");
    switch_some_pairs("kqw", "kqj", '^', "mps", "snv", "jgq", '|', "z27");
    switch_some_pairs("trn", "gpm", '^', "msq", "trn", "gpm", '&', "z39");
    rep(j, 0, 46) print_those_associatied(j);
    do_some();

    set<string> ans = {"cnk", "vhm", "mps", "msq", "qwf", "z14", "z27", "z39"};
    for (string s : ans) {
        cout << s << ',';
    }
    
} 

// 6457374031397


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}
