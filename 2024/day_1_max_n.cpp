#include <bits/stdc++.h>

#ifdef LOCAL
#include "./cpp-dump/cpp-dump.hpp"
#define pr(x) cpp_dump(x)
#endif

using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

vi pa(const string& s) {
    ll n = sz(s);
    vi res;
    rep(i, 0, n) {
        if (isdigit(s[i])) {
            ll j = i, x = 0;
            while (j < n && isdigit(s[j])) {
                x = 10 * x + s[j] - '0';
                j++;
            }
            if (i != 0 && s[i - 1] == '-')
                x *= -1;
            res.pb(x);
            i = j - 1;
        }
    }
    return res;
}
vector<string> pas(const string& s) {
    stringstream ss; ss << s;
    vector<string> ret;
    string t;
    while(ss >> t) ret.pb(t);
    return ret;
}

int ans = 0;

void solve() {
    string s;
    vi l, r;
    map<int, int> mp;
    while(getline(cin, s)) {
        vi v = pa(s);
        l.pb(v[0]);
        r.pb(v[1]);
        mp[v[1]]++;

    }

    rep(i, 0, l.size()) ans += l[i]*mp[l[i]];

    pr(ans);

    // sort(all(l));
    // sort(all(r));
    // rep(i, 0, l.size()) ans += abs(l[i] - r[i]);
    // pr(ans);
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}
