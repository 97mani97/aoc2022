#include <bits/stdc++.h>

#ifdef LOCAL
#include "./cpp-dump/cpp-dump.hpp"
#define pr(x) cpp_dump(x)
#endif

using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

set<string> alls;
vector<string> alls_vector;

map<string, set<string>> mp;
set<vector<string>> st;

set<string> cur;
set<string> join_to_cur(int i) {
    if (i == alls_vector.size()) {
        return cur;
    }
    set<string> cur_res = join_to_cur(i+1);

    string s = alls_vector[i];   

    for (string t : cur) if (mp[s].count(t) == 0) {
        return cur_res;
    } 

    cur.insert(s);
    set<string> cur_res2 = join_to_cur(i+1);
    if (sz(cur_res2) > sz(cur_res)) {
        cur_res = cur_res2;
    }
    cur.erase(s);

    return cur_res;
}

void solve() {
    string s1, s2, s3;
    while(cin >> s1 >> s2) {
        alls.insert(s1);
        alls.insert(s2);
        mp[s1].insert(s2);
        mp[s2].insert(s1);
    }

    for (string s : alls) alls_vector.pb(s);

    ll ans = 0;
    for (auto s1 : alls_vector) {
        for (auto s2 : mp[s1]) {
            for (auto s3 : mp[s2]) {
                if (mp[s1].count(s3)) {
                    vector<string> res = {s1, s2, s3};
                    sort(all(res));
                    st.insert(res);
                }
            }
        }
    }
    for (auto res : st) {
        bool ok = false;
        for (auto s : res) if (s[0] == 't') ok = true;
        if (ok) ans++; 
    }
    pr(ans);

    set<string> ans2 = join_to_cur(0);
    for (auto s : ans2) cout << s << ',';
    cout << '\n';

} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}
