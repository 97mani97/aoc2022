#include <bits/stdc++.h>

#ifdef LOCAL
#include "./cpp-dump/cpp-dump.hpp"
#define pr(x) cpp_dump(x)
#endif

using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

ll mix(ll x, ll secret) {
    return x^secret;
}
ll prune(ll x) {
    return Mod(x, (ll)16777216);
}

ll step_1(ll secret) {
    return prune(mix(secret*64, secret));
}
ll step_2(ll secret) {
    return prune(mix(secret/32, secret));
}
ll step_3(ll secret) {
    return prune(mix(secret*2048, secret));
}

void solve() {
    ll test;
    ll no = 2000;
    ll ans = 0;

    map<string, ll> tot;

    while(cin >> test) {

        vector<ll> lasts;
        lasts.pb(test%10);
        rep(i, 0, no) {
            test = step_1(test);
            test = step_2(test);
            test = step_3(test);
            lasts.pb(test%10);
        }

        vector<ll> changes;
        changes.pb(0);
        rep(i, 0, sz(lasts)-1) changes.pb(lasts[i+1]-lasts[i]);

        map<string, ll> mp;
        rep(i, 1, sz(changes)-3) {
            ll x1 = changes[i], x2 = changes[i+1], x3 = changes[i+2], x4 = changes[i+3];
            string s = to_string(x1) + "," + to_string(x2) + "," +to_string(x3) + "," + to_string(x4);
            if (mp.count(s) == 0) mp[s] = lasts[i+3];
        }

        for (auto p : mp) tot[p.first] += p.second;
        ans += test;
    }

    pr(ans);

    ll ans2 = 0;
    string best;
    for (auto p : tot) {
        if (p.second > ans2) best = p.first;
        ans2 = max(ans2, p.second);
    }
    pr(best);
    pr(ans2);
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}
