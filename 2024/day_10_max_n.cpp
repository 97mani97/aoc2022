#include <bits/stdc++.h>

#ifdef LOCAL
#include "./cpp-dump/cpp-dump.hpp"
#define pr(x) cpp_dump(x)
#endif

using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

ll ans = 0;
vector<vi> vs;
set<pii> vis;

int n = 0;
int m = 0;

ll get_score(int x, int y, int d) {
    if (vs[x][y] == 9) return 1;
    ll ret = 0;
    rep(dx, -1, 2) rep(dy, -1, 2) if (dx == 0 || dy == 0) {
        int nx = x+dx, ny = y+dy;
        if (min(nx, ny) >= 0 && nx < n && ny < m && vs[nx][ny] == d+1 && vis.count(pii(nx, ny)) == 0) {
            vis.insert(pii(nx, ny));
            ret += get_score(nx, ny, d+1);
            vis.erase(pii(nx, ny));
        } 
    }

    return ret;

}

void solve() {
    string s;
    while(cin >> s) {
        vi v;
        for (char c : s) v.pb(c-'0');
        vs.pb(v);
    }
    n = sz(vs);
    m = sz(vs[0]);

    rep(x, 0, n) rep(y, 0, m) {
        vis.clear();

        ll res = 0;
        if (vs[x][y] == 0) {
            vis.insert(pii(x, y));
            res = get_score(x, y, 0);
        }
        
        ans += res;
        // pr(res);
    }

    pr(ans);
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}
