#include <bits/stdc++.h>

#ifdef LOCAL
#include "./cpp-dump/cpp-dump.hpp"
#define pr(x) cpp_dump(x)
#endif

using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

vector<string> mat;

int n, m;

pii push_hor(pii s, pii ds) {
    int i = s.first, j = s.second, di = ds.first, dj = ds.second;
    int ni = i+di, nj = j+dj;
    if (mat[ni][nj] == '#') return s;
    if (mat[ni][nj] == '[' || mat[ni][nj] == ']') {
        push_hor(pii(ni,nj), ds);
    }

    if (mat[ni][nj] == '.') {
        swap(mat[i][j], mat[ni][nj]);
        return pii(ni, nj);
    }
    return s;
}
pii push_ver(pii s, pii ds) {
    int i = s.first, j = s.second, di = ds.first, dj = ds.second;
    int ni = i+di, nj = j+dj;

    if (mat[ni][nj] == '#') return s;
    if (mat[ni][nj] == '[') {
        push_ver(pii(ni,nj), ds);
        push_ver(pii(ni,nj+1), ds);
        if (!(mat[ni][nj] == '.' && mat[ni][nj+1] == '.')) {
            return s;
        }

    } else if (mat[ni][nj] == ']') {
        push_ver(pii(ni,nj), ds);
        push_ver(pii(ni,nj-1), ds);
        if (!(mat[ni][nj] == '.' && mat[ni][nj-1] == '.')) {
            return s;
        }
    }

    if (mat[ni][nj] == '.') {
        swap(mat[i][j], mat[ni][nj]);
        return pii(ni, nj);
    }
    return s;
}

pii move(pii s, char c) {
    int di = 0;
    int dj = 0;
    if (c == '^') di = -1;
    if (c == '>') dj =  1;
    if (c == 'v') di =  1;
    if (c == '<') dj = -1;

    int i = s.first, j = s.second;
    assert(mat[i][j] == '@');

    if (c == '>' || c == '<') return push_hor(s, pii(di, dj));
    else return push_ver(s, pii(di, dj));
}

void solve() {
    string s;
    while(cin >> s) {
        if (s.size() == 0) break;
        string t;

        for (char c : s) {
            if (c == '#') t += "##";
            if (c == 'O') t += "[]";
            if (c == '.') t += "..";
            if (c == '@') t += "@.";
        }


        mat.pb(t);
        cin.ignore();
        if (cin.peek() == '\n') break;
    }
    n = sz(mat);
    m = sz(mat[0]);
    

    s.clear();
    string t;
    while(cin >> t) s += t;

    pii st;
    rep(i, 0, n) rep(j, 0, m) if (mat[i][j] == '@') {
        st = pii(i, j);
    }

    for (char c : s){
        vector<string> mat2 = mat;
        pii st2 = st;

        st = move(st, c);
        if (st2 == st) {
            mat = mat2;
        }
    }


    ll ans = 0;
    rep(i, 1, n) rep(j, 1, m) if (mat[i][j] == '[') {
        ans += 100*i + j;
    }
    pr(ans);



} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}
