#include <bits/stdc++.h>

#ifdef LOCAL
#include "./cpp-dump/cpp-dump.hpp"
#define pr(x) cpp_dump(x)
#endif

using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef unsigned long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

vi pa(const string& s) {
    ll n = sz(s);
    vi res;
    rep(i, 0, n) 
        if (isdigit(s[i])) {
            ll j = i, x = 0;
            while (j < n && isdigit(s[j])) {
                x = 10 * x + s[j] - '0';
                j++;
            }
            if (i != 0 && s[i - 1] == '-')
                x *= -1;
            res.pb(x);
            i = j - 1;
        }
    return res;
}

string s;
ll ans = 0;

ll f(vi v) {
    vi w;
    ll x = v[0];
    rep(i, 1, v.size()) w.pb(v[i]);

    rep(i, 0, pow(3, sz(w))) {
        vi signs;
        ll cur = i;
        rep(j, 0, sz(w)-1) {
            signs.pb(cur%3);
            cur /= 3;
        }
        vi res;
        rep(j, 0, sz(w)-1) res.pb(w[j]), res.pb(signs[j]);
        res.pb(w[sz(w)-1]); 

        ll z = res[0];

        rep(j, 1, res.size()) {
            if (j % 2 == 1) {
                if (res[j] == 0) {
                    z += res[j+1];
                } else if (res[j] == 1){
                    z *= res[j+1];
                } else if (res[j] == 2){
                    string z1 = to_string(z), z2 = to_string(res[j+1]);
                    
                    z = stoull(z1 + z2);
                }
            }

            if (z > x) break;
            if (j == sz(res)-1 && z == x) return x; 
        }
    }

    return 0;
}

void solve() {
    while(getline(cin, s)) {
        vi v = pa(s);
        ll to_add = f(v);
        ans += to_add;
    }
    pr(ans);
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}
