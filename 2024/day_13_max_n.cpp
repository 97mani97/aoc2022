#include <bits/stdc++.h>
#ifdef LOCAL
#include "./cpp-dump/cpp-dump.hpp"
#define pr(x) cpp_dump(x)
#endif
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

ll ans = 0;

pii a, b;
ll cost_a = 3, cost_b = 1;
pii goal;

ll euclid(ll a, ll b, ll &x, ll &y) {
    if (!b) return x = 1, y = 0, a;
    ll d = euclid(b, a % b, y, x);
    return y -= a/b * x, d;
}

vi solve_dio(ll a, ll b, ll c) {
    ll d = gcd(a, b);
    ll x = 0, y = 0;
    if (c % d != 0) {
        return {0, 0, 0, 0};
    }
    assert(c%d== 0);
    euclid(a, b, x, y);
    x *= (c/d);
    y *= (c/d);

    assert(a*x + b*y == c);

    ll u = a/d;
    ll v = b/d;
    assert(a*(x + 3*v) + b*(y - 3*u) == c);


    return {x, y, v, u};
}

ll added = 10000000000000;

ll get_bin(ll x0, ll y0, ll v0, ll u0, ll a, ll b, ll c, ll k) {
    ll x = x0 + k*v0, y = y0 - k*u0;
    return a*x + b*y - c; 
}

ll get_ans(ll x0, ll y0, ll v0, ll u0, ll a, ll b, ll c) {
    ll kl = -1, kr = 1;
    while(1) { 
        ll z1 = get_bin(x0, y0, v0, u0, a, b, c, kl);
        ll z2 = get_bin(x0, y0, v0, u0, a, b, c, kr);
        if (z1 < 0 && 0 < z2) break;
        if (z2 < 0 && 0 < z1) break;
        kl *= 2;
        kr *= 2;
    }
    ll sgn = 1;
    if (get_bin(x0, y0, v0, u0, a, b, c, kl) > 0) sgn = -1;

    ll kans = LLONG_MAX;

    while(kl <= kr) {
        ll km = (kl+kr)/2;
        if (sgn*get_bin(x0, y0, v0, u0, a, b, c, km) < 0) kl = km+1;
        else if (sgn*get_bin(x0, y0, v0, u0, a, b, c, km) > 0) kr = km-1;
        else {
            kans = km;
            break;
        }
    }

    if (get_bin(x0, y0, v0, u0, a, b, c, kans) != 0) return 0;

    ll x = x0 + kans*v0, y = y0 - kans*u0;

    return cost_a*x + cost_b*y;
}
 
void solve() {
    ll ax, ay, bx, by, px, py;

    int cnt = 0;

    while(cin >> ax >> ay >> bx >> by >> px >> py) {
        a = pii(ax, ay);
        b = pii(bx, by);
        goal = pii(px+added, py+added);

        ll A = a.first-a.second;
        ll B = b.first-b.second;
        ll C = goal.first-goal.second;

        vi dio = solve_dio(A, B, C);
        ll x0 = dio[0], y0 = dio[1], v0 = dio[2], u0 = dio[3];
        if (x0 == 0 && y0 == 0 && v0 == 0 && u0 == 0) continue;

        ans += get_ans(x0, y0, v0, u0, a.first, b.first, goal.first);
    }

    pr(ans);
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}
