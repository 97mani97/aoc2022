#include <bits/stdc++.h>

#ifdef LOCAL
#include "./cpp-dump/cpp-dump.hpp"
#define pr(x) cpp_dump(x)
#endif

using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

ll ans1 = 0, ans2 = 0;
string s;

const int mxn = 200;

int n, m;

char mat[mxn][mxn];
char mat2[mxn][mxn];
int on[mxn][mxn];

pii move(pii cur) {
    int x = cur.first, y = cur.second;

    int di = 0, dj = 0;

    if (mat[x][y] == '^') di = -1;
    if (mat[x][y] == '>') dj = 1;
    if (mat[x][y] == '<') dj = -1;
    if (mat[x][y] == 'v') di = 1;

    mat[x][y] = '.';   

    while(1) {
        on[x][y]++;
        int nx = x+di, ny = y+dj;
        if (min(nx, ny) < 0 || nx >= n || ny >= n) return pii(-1, -1);
        if (mat[nx][ny] == '#') {
            if (di == -1) mat[x][y] = '>';
            if (dj == 1) mat[x][y] = 'v';
            if (dj == -1) mat[x][y] = '^';
            if (di == 1) mat[x][y] = '<';

            return pii(x, y);
        }
        x = nx;
        y = ny;
    }

}

bool got_loop(pii obst, pii st) {
    rep(i, 0, n) rep(j, 0, m) mat[i][j] = mat2[i][j], on[i][j] = 0;
    if (obst != st) mat[obst.first][obst.second] = '#';

    while(st.first > -1) {
        if (on[st.first][st.second] > 2) return true;
        st = move(st);
    }

    return false;
}

void solve() {
    int i = 0;
    while(getline(cin, s)) {
        rep(j, 0, s.size()) mat2[i][j] = s[j];
        m = s.size();
        i++;
    }
    n = i;

    pii st;
    rep(i, 0, n) rep(j, 0, m) if (mat2[i][j] != '.' && mat2[i][j] != '#') {
        st = pii(i, j);
    }

    // part 1
    got_loop(st, st);
    rep(i, 0, n) rep(j, 0, n) if (on[i][j]) ans1++;
    pr(ans1);   

    // part 2
    rep(i, 0, n) rep(j, 0, m) if (got_loop(pii(i, j), st)) ans2++;
    pr(ans2);
    
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}
