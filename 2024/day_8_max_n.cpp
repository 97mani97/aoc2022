#include <bits/stdc++.h>

#ifdef LOCAL
#include "./cpp-dump/cpp-dump.hpp"
#define pr(x) cpp_dump(x)
#endif

using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

vector<string> vs;
map<pii, bool> ant;

int n = 0, m = 0;

void solve() {
    string s;
    while(getline(cin, s)) {
        vs.pb(s);
        n++;
        m = vs[0].size();
    }

    rep(i1, 0, n) rep(j1, 0, m) rep(i2, 0, n) rep(j2, 0, m) if (vs[i1][j1] == vs[i2][j2] && vs[i1][j1] != '.' && (i1 != i2 || j1 != j2)) {
        int di = i2 - i1, dj = j2 - j1;

        for (ll k = 0; k <= 100; k++) {
            int nx = i2+k*di;
            int ny = j2+k*dj;

            if (min(nx, ny) >= 0 && nx < n && ny < m) {
                ant[pii(nx, ny)] = true;
            }
        }
        for (ll k = 0; k <= 100; k++) {
            int nx = i1-k*di;
            int ny = j1-k*dj;

            if (min(nx, ny) >= 0 && nx < n && ny < m) {
                ant[pii(nx, ny)] = true;
            }
        }


    }

    int ans = sz(ant);
    pr(ans);

    
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}
