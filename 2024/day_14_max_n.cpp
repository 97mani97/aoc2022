#include <bits/stdc++.h>
#ifdef LOCAL
#include "./cpp-dump/cpp-dump.hpp"
#define pr(x) cpp_dump(x)
#endif
using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

map<int, pii> pos;
map<int, pii> mp;

ll m = 101; // wide
ll n = 103; // tall

void move() {
    map<int, pii> pos2;
    for (auto p : pos) {
        int id = p.first;
        pii q = p.second;
        pii vs = mp[id];
        q.first = Mod(q.first+vs.first, m);
        q.second = Mod(q.second+vs.second, n);

        pos2[id] = q;
    }
    pos = pos2;
}

void print() {
    set<pii> exists;
    for (auto p : pos) exists.insert(p.second);
    rep(j, 0, n) {
        rep(i, 0, m) {
            if (exists.count(pii(i, j))) cout << '#';
            else cout << '.';
        } 
        cout << '\n';
    }
}

vi get_quadrants() {
    ll ans11 = 0;
    ll ans12 = 0;
    ll ans21 = 0;
    ll ans22 = 0;

    for (auto p : pos) {
        ll x = p.second.first;
        ll y = p.second.second;

        if (x >= 0 && x < (m-1)/2) {
            if (y >= 0 && y < (n-1)/2) ans11++;
            else if (y > (n-1)/2) ans12++;
        }

        if (x >= 0 && x > (m-1)/2) {
            if (y >= 0 && y < (n-1)/2) ans21++;
            else if (y > (n-1)/2) ans22++;
        }
    }

    return {ans11, ans12, ans21, ans22};
}

bool find_long_row() {
    set<pii> exists;
    for (auto p : pos) exists.insert(p.second);

    int row = 10;

    rep(j, 0, n){
        rep(i, 0, m) {
            bool ok = true;
            rep(dj, 0, row) {
                if (!exists.count(pii(i, j+dj))) {
                    ok = false;
                    break;
                }
            }
            if (ok) return true;
        }
    }
    return false;
}
 
void solve() {
    ll x, y, vx, vy;
    int i = 0;
    while(cin >> x >> y >> vx >> vy) {
        pos[i] = pii(x, y);
        mp[i] = pii(vx, vy);
        i++;
    }

    bool PART1 = false;

    ll iter = LLONG_MAX;
    if (PART1) iter = 100;

    rep(i, 0, iter) {
        move();
        vi ans = get_quadrants();
        if (!PART1 && find_long_row()) {
            print();
            cout << i+1 << '\n';
            break;
        }
    }
    
    if (PART1) {
        vi ans = get_quadrants();
        cout << ans[0]*ans[1]*ans[2]*ans[3] << '\n';
    }
} 

int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}
