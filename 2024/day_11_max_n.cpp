#include <bits/stdc++.h>

#ifdef LOCAL
#include "./cpp-dump/cpp-dump.hpp"
#define pr(x) cpp_dump(x)
#endif

using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

vi pa(const string& s) {
    ll n = sz(s);
    vi res;
    rep(i, 0, n) 
        if (isdigit(s[i])) {
            ll j = i, x = 0;
            while (j < n && isdigit(s[j])) {
                x = 10 * x + s[j] - '0';
                j++;
            }
            if (i != 0 && s[i - 1] == '-')
                x *= -1;
            res.pb(x);
            i = j - 1;
        }
    return res;
}

const int mxn = 75;

ll ans = 0;
map<ll, ll> dp[mxn+1];


vector<ll> transform(ll x) {
    vi w;
    string s = to_string(x);
    if (x == 0) w.pb(1);
     else if (sz(s)%2 == 0) {
        string t1 = s.substr(0, sz(s)/2), t2 = s.substr(sz(s)/2);
        w.pb(stoll(t1)), w.pb(stoll(t2));
    } else w.pb(x*((ll)2024));
    return w;
}

ll getdp(ll x, ll rem) {
    if (rem == 0) return 1;

    if (dp[rem].count(x)) return dp[rem][x];
    ll res = 0;
    for (ll y : transform(x)) res += getdp(y, rem-1);
    return dp[rem][x] = res;
}


void solve() {
    string s;
    getline(cin, s);
    vi v = pa(s);
    for (ll x : v) ans += getdp(x, mxn);

    pr(ans);
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}
