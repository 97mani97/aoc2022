#include <bits/stdc++.h>

#ifdef LOCAL
#include "./cpp-dump/cpp-dump.hpp"
#define pr(x) cpp_dump(x)
#endif

using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;
const bool PART1 = false;

set<string> pas(const string& s) {
    stringstream ss; ss << s;
    set<string> ret;
    string t;
    while(ss >> t) ret.insert(t);
    return ret;
}

set<string> designs;
string s;

map<int, ll> dp;
ll pos(int i) {
    if (i == sz(s)) return 1;
    if (dp.count(i)) return dp[i];
    ll res = 0;

    for (string t : designs) if (t == s.substr(i, sz(t))) {
        res += pos(i+sz(t));
    }

    dp[i] = res;
    return res;
}



void solve() {
    getline(cin, s);
    designs = pas(s);
    
    ll ans = 0;

    while(cin >> s) {
        dp.clear();
        ans += (PART1 && pos(0) ? 1 : pos(0));
    }
    pr(ans);
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}
