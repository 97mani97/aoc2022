#include <bits/stdc++.h>

#ifdef LOCAL
#include "./cpp-dump/cpp-dump.hpp"
#define pr(x) cpp_dump(x)
#endif

using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

const int n = 71;
int m = n;
int fallen = 1024;
vector<pii> bytes;
char mat[n][n];

int i4[4] = {-1, 0, 0, 1};
int j4[4] = { 0,-1, 1, 0};

typedef pii TT; // often ll or string or pii

vector<pair<ll, TT>> get_edges(TT u) { // here it is possible to implement an infinite graph
    // vector<pair<ll, TT>> edges = gr[u];

    vector<pair<ll, TT>> edges; // this is example for 2d graph without explicit gr
    rep(x, 0, 4) { // or rep(di, -1, 2) rep(dj, -1, 2) if (di != 0 || dj != 0)
        int i = u.first + i4[x], j = u.second + j4[x];
        if (min(i, j) >= 0 && i < n && j < m && mat[i][j] != '#') { // change for appropriate bounds and #
            edges.pb(pair<ll, pii>(1, pii(i, j))); // change for appropriate distance
        }
    }

    return edges;
}

ll dijkstra(TT src, TT targ) { // needs a defined get_edges, and if you want to return dist/prev just ...
    map<TT, ll> dist;
    map<TT, TT> prev;
    set<TT> vis;
    priority_queue<pair<ll, TT>, vector<pair<ll, TT>>, greater<pair<ll, TT>>> pq;
    dist[src] = 0;
    pq.push(pair<ll, TT>(0, src));

    while(pq.size()) {
        pair<ll, TT> p = pq.top(); pq.pop();
        ll du = p.first;
        TT u = p.second;
        if (dist[u] < du) continue;
        if (u == targ) return dist[u]; // ... remove this line ...

        for (auto [d, v] : get_edges(u)) {
            if (dist.count(v) == 0 || dist[v] > dist[u] + d) {
                dist[v] = dist[u] + d;
                prev[v] = u;
                pq.push(pair<ll, TT>(dist[v], v));
            }
        }
    }

    return LLONG_MAX; // ... and change this to corresponding output
}


void solve() {
    int x, y;
    rep(i, 0, n) rep(j, 0, n) mat[i][j] = '.';
    while(cin >> x >> y) bytes.pb(pii(x, y));
    rep(i, 0, sz(bytes)) {
        mat[bytes[i].first][bytes[i].second] = '#';
        pii srs(0, 0);
        pii targ(n-1, n-1);

        ll dist = dijkstra(srs, targ);
        if (dist == LLONG_MAX) {
            pr(bytes[i]);
            break;
        }
    }
} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}
