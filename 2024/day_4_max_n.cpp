#include <bits/stdc++.h>

#ifdef LOCAL
#include "./cpp-dump/cpp-dump.hpp"
#define pr(x) cpp_dump(x)
#endif

using namespace std;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(ll (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define pb push_back
#define gcd __gcd 
#define sz(x) (ll)(x.size())

typedef long long ll;
typedef unsigned long long ull;
typedef pair<ll, ll> pii;
typedef vector<ll> vi;
typedef vector<pii> vii;

const bool debug = false;

vi pa(const string& s) {
    ll n = sz(s);
    vi res;
    rep(i, 0, n) 
        if (isdigit(s[i])) {
            ll j = i, x = 0;
            while (j < n && isdigit(s[j])) {
                x = 10 * x + s[j] - '0';
                j++;
            }
            if (i != 0 && s[i - 1] == '-')
                x *= -1;
            res.pb(x);
            i = j - 1;
        }
    return res;
}
vector<string> pas(const string& s) {
    stringstream ss; ss << s;
    vector<string> ret;
    string t;
    while(ss >> t) ret.pb(t);
    return ret;
}

ll ans = 0;
string s;
vector<string> vs, vs2, vs3;

string t = "XMAS", t_rev = "SAMX";

void solve() {
    while(getline(cin, s)) {
        vs.pb(s);
    }
    int n = vs.size();
    int m = vs[0].size();

    rep(i, 1, n-1) {
        rep(j, 1, m-1) {
            if (vs[i][j] == 'A') {
                set<int> st1, st2;
                st1.insert(vs[i-1][j-1]);
                st1.insert(vs[i+1][j+1]);
                st2.insert(vs[i-1][j+1]);
                st2.insert(vs[i+1][j-1]);

                if (st1.count('M') &&
                    st1.count('S') &&
                    st2.count('M') &&
                    st2.count('S')) ans++;
            }
        }
    }

    pr(ans);
    return;

    rep(i, 0, n) {
        rep(j, 0, m) {
            if (vs[i].substr(j, t.size()) == t) ans++;
            if (vs[i].substr(j, t_rev.size()) == t_rev) ans++;

        }
    }

    vs2 = vs;
    rep(i, 0, n) rep(j, 0, m) vs2[i][j] = vs[j][i];


    rep(i, 0, n) {
        rep(j, 0, m) {
            if (vs2[i].substr(j, t.size()) == t) ans++;
            if (vs2[i].substr(j, t_rev.size()) == t_rev) ans++;
        }
    }


    rep(i, 0, n) {
        string diag;
        rep(k, i, n) {
            diag += vs[k][k-i];
        }
        rep(j, 0, diag.size()) if (diag.substr(j, t.size()) == t) ans++;
        rep(j, 0, diag.size()) if (diag.substr(j, t_rev.size()) == t_rev) ans++;
    }

    rep(j, 1, n) {
        string diag;
        rep(k, j, n) {
            diag += vs[k-j][k];
        }
        rep(i, 0, diag.size()) if (diag.substr(i, t.size()) == t) ans++;
        rep(i, 0, diag.size()) if (diag.substr(i, t_rev.size()) == t_rev) ans++;
    }

    vs3 = vs;
    rep(i, 0, n) rep(j, 0, m) vs3[n-1-i][j] = vs[i][j];

    rep(i, 0, n) {
        string diag;
        rep(k, i, n) {
            diag += vs3[k][k-i];
        }
        rep(j, 0, diag.size()) if (diag.substr(j, t.size()) == t) ans++;
        rep(j, 0, diag.size()) if (diag.substr(j, t_rev.size()) == t_rev) ans++;
    }

    rep(j, 1, n) {
        string diag;
        rep(k, j, n) {
            diag += vs3[k-j][k];
        }
        rep(i, 0, diag.size()) if (diag.substr(i, t.size()) == t) ans++;
        rep(i, 0, diag.size()) if (diag.substr(i, t_rev.size()) == t_rev) ans++;
    }

    pr(ans);

} 


int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    cout << setprecision(15) << fixed;

#ifdef LOCAL
    freopen("input.txt", "r", stdin);
#endif

    solve();
}
