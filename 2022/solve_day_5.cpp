#include <bits/stdc++.h>
#include <string>
using namespace std;
 
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
typedef vector<int> vi;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define ret(x) for (auto it = (x).begin(); it != (x).end(); ++it)
#define pb push_back

bool bound_check(pair<int, int> p, pair<int, int> d, int maxi, int maxj) { // true = bad
    return (p.first+d.first < 0 || p.first+d.first >= maxi || p.second+d.second < 0 || p.second+d.second >= maxj);
}

vector<pii> dir2 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1)}; 
vector<pii> dir3 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1),
                    pii(1, 1), pii(1, -1), pii(-1, 1), pii(-1, -1)};
vector<pii> dir4 = {pii(1, 1), pii(-1, 1), pii(1, -1), pii(-1, -1)}; 

     


#if 0
#define Cin cin
#endif 

//
fstream Cin;  // UNCOMMENT THIS

const int N = 2e5 + 2;
int test = 1;


void solve() {
    ull ans = 0;
    int x, y, z;
    char c;


    vector<vector<char>> v(9);
    v[0] = {'F', 'H', 'B', 'V', 'R', 'Q', 'D', 'P'};
    v[1] = {'L', 'D', 'Z', 'Q', 'W', 'V'};
    v[2] = {'H', 'L', 'Z', 'Q', 'G', 'R', 'P', 'C'};
    v[3] = {'R', 'D', 'H', 'F', 'J', 'V', 'B'};
    v[4] = {'Z', 'W', 'L', 'C'};
    v[5] = {'J', 'R', 'P', 'N', 'T', 'G', 'V', 'M'};
    v[6] = {'J', 'R', 'L', 'V', 'M', 'B', 'S'};
    v[7] = {'D', 'P', 'J'};
    v[8] = {'D', 'C', 'N', 'W', 'V'};

    while(1) {
        if (Cin.peek() == '\n' || Cin.peek() == EOF) {
            // Don't write here except for newline detection
        } else {
            Cin >> x >> y >> z;
            y-=1;
            z-=1;
            vector<char> temp;
            rep(i, 0, x) {
                if(v[y].size() == 0) continue;

                c = v[y].back();
                v[y].pop_back();
                temp.pb(c);
            }
            rep(i, 0, temp.size()) {
                v[z].pb(temp[temp.size()-1-i]);
            }
        }

        if (Cin.peek() == EOF) break;   // Don't remove
        Cin.ignore();                   // Don't remove
    }
    rep(i, 0, 9) {
        cout << v[i].back();
    }

}



int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout << setprecision(10) << fixed;

    //
    Cin.open("input.txt",ios::in); // UNCOMMENT THIS

    rep(i, 1, test+1) {
        //cout << "Case #" << i << ": ";
        solve();
    }

    //
    Cin.close(); // UNCOMMENT THIS
    return 0;
}

