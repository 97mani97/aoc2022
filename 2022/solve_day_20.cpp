#include <bits/stdc++.h>
#include <string>
using namespace std;
 
typedef long long ll;
typedef unsigned long long ull;
typedef pair<ull, ull> pii;
typedef vector<int> vi;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define ret(x) for (auto it = (x).begin(); it != (x).end(); ++it)
#define pb push_back
#define sz(x) (int)(x).size()

bool bound_check(pair<int, int> p, pair<int, int> d, int maxi, int maxj) { // true = bad
    return (p.first+d.first < 0 || p.first+d.first >= maxi || p.second+d.second < 0 || p.second+d.second >= maxj);
}

vector<pii> dir2 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1)}; 
vector<pii> dir3 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1),
                    pii(1, 1), pii(1, -1), pii(-1, 1), pii(-1, -1)};
vector<pii> dir4 = {pii(1, 1), pii(-1, 1), pii(1, -1), pii(-1, -1)}; 

     
#if 0
#define Cin cin
#endif 

//
fstream Cin;  // UNCOMMENT THIS

const int N = 2e5 + 2;
int test = 1;


void solve() {
    ll ans = 0;

    vector<ll> v;
    ll x;

    map<ll, ll> mp;
    map<ll, ll> mpinv;
    ll index = 0;

    while(1) {
        if (Cin.peek() == '\n' || Cin.peek() == EOF) {
            // Don't write here except for newline detection
        } else {   
            Cin >> x;
            v.pb(x);

            mp[index] = index;
            index++;
        }
        if (Cin.peek() == EOF) break;   // Don't remove
        Cin.ignore();                   // Don't remove     
    }

    ll n = v.size();
    mpinv = mp;
    
    int ratio = 811589153 % (n-1);

    rep(j, 0, n) {
        v[j] = v[j]*(811589153 % (n-1));
    }

    vector<ll> vc = v;

    rep(mix, 0, 10) {
        for (ll i = 0; i < n; i++) {
            x = v[mp[i]];
            ll at = Mod(mp[i] + x, n-1);
            if (at == 0 && mp[i] != 0) at = n-1;

            if (mp[i] != at) {
                if (mp[i] < at ) {
                    for (ll k = mp[i]+1; ; k = Mod(k+1, n)) {
                        mp[mpinv[k]]--;
                        if (k == at) break;
                    }
                    mp[i] = at;
                } else {
                    for (ll k = at; ; k = Mod(k+1, n)) {
                        mp[mpinv[k]]++;
                        if (k == mp[i]-1) break;
                    }
                    mp[i] = at;
                }
                
            }

            ret(mp) {
                mpinv[it->second] = it->first;
            }

            rep(i, 0, n) {
                v[i] = vc[mpinv[i]];
            }
        }
    }


    rep(i, 0, n) {
        if (v[i] == 0) {
            x = i;
        }
    }

    rep(i, 0, n) {
        v[i] = (v[i] / ratio) * 811589153 ;
    }

    ans = v[Mod(x + 1000, n)] + v[Mod(x + 2000, n)] + v[Mod(x + 3000, n)];


    cout << ans << '\n';

    return;
}



int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout << setprecision(10) << fixed;

    //
    Cin.open("input.txt",ios::in); // UNCOMMENT THIS



    solve();


    //
    Cin.close(); // UNCOMMENT THIS
    return 0;
}


