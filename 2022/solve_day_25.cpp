#include <bits/stdc++.h>
#include <string>
using namespace std;
 
typedef long long ll;
typedef long long ull;
typedef pair<int, int> pii;
typedef vector<int> vi;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define ret(x) for (auto it = (x).begin(); it != (x).end(); ++it)
#define pb push_back
#define sz(x) (int)(x).size()

bool bound_check(pair<int, int> p, pair<int, int> d, int maxi, int maxj) { // true = bad
    return (p.first+d.first < 0 || p.first+d.first >= maxi || p.second+d.second < 0 || p.second+d.second >= maxj);
}

vector<pii> dir2 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1)}; 
vector<pii> dir3 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1),
                    pii(1, 1), pii(1, -1), pii(-1, 1), pii(-1, -1)};
vector<pii> dir4 = {pii(1, 1), pii(-1, 1), pii(1, -1), pii(-1, -1)}; 

vector<vector<pii>> dirs = {{pii(0, -1), pii(-1, -1), pii(1, -1)},
                            {pii(0, 1), pii(-1, 1), pii(1, 1)},
                            {pii(-1, 0), pii(-1, -1), pii(-1, 1)},
                            {pii(1, 0), pii(1, -1), pii(1, 1)}};

     
#if 0
#define Cin cin
#endif 

//
fstream Cin;  // UNCOMMENT THIS

const int N = 2e5 + 2;
int test = 1;

ull decode(string s) {
    //cout << "Got s = " << s << '\n';
    char c;
    ull res = 0;
    ull count = 0;
    for (int i = s.size()-1; i >= 0 ; i--) {
        c = s[i];
        ull po = pow(5, count);
        if (c == '2') res += po * 2;
        else if (c == '1') res += po;
        else if (c == '0') res += 0*po;
        else if (c == '-') res += -1*po;
        else if (c == '=') res += -2*po;

        count++;
    }
    //cout << "Returnsning " << res << "\n\n";
    return res;
}

string encode(ull x) {
    string s;

    return s;
}

int convert(char c) {
    if (c == '2') return 2;
    else if (c == '1') return 1;
    else if (c == '0') return 0;
    else if (c == '-') return -1;
    else return -2;
}

char decon(int x) {
    if (x == 2) return '2';
    else if (x == 1) return '1';
    else if (x == 0) return '0';
    else if (x == -1) return '-';
    else return '=';
}

string add(string s1, string s2) {
    int rem = 0;
    string res = "";
    while(s1.size() < s2.size()) s1 = '0' + s1;
    while(s2.size() < s1.size()) s2 = '0' + s2;

    cout << "Adding s1 = " << s1 << " and s2 = " << s2 << '\n';

    for (int i = s1.size()-1; i >= 0; i--) {
        int x1 = convert(s1[i]);
        int x2 = convert(s2[i]);
        int x = x1 + x2 + rem;
        if (x >= -2 && x <= 2) {
            res = decon(x) + res;
            rem = 0;
        } else if (x > 2) {
            res = decon(x - 5) + res;
            rem = 1;
        } else if (x < -2) {
            res = decon(x + 5) + res;
            rem = -1;
        }
        cout << "Res = " << res << '\n';
    }
    if (rem == 1) res = '1' + res;
    cout << "Res = " << res << '\n';
    cout << "\n\n";

    return res;
}


void solve() {
    ull ans = 0;
    string s;

    string res = "";


    while(1) {
        if (Cin.peek() == '\n' || Cin.peek() == EOF) {
            // Don't write here except for newline detection

        } else {
            Cin >> s;
            //ans += decode(s);
            res = add(res, s);
            //cout << "Current sum should be = " << decode(res) << "\n\n";

        }
        if (Cin.peek() == EOF) break;   // Don't remove
        Cin.ignore();                   // Don't remove     
    }

    cout << ans << '\n';
    cout << res << '\n';
    return;
}



int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout << setprecision(10) << fixed;

    //
    Cin.open("input.txt",ios::in); // UNCOMMENT THIS

    solve();

    //
    Cin.close(); // UNCOMMENT THIS
    return 0;
}


