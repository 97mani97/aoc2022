#include <bits/stdc++.h>
#include <string>
using namespace std;
 
typedef long long ll;
typedef unsigned long long ull;
typedef pair<ull, ull> pii;
typedef vector<int> vi;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define ret(x) for (auto it = (x).begin(); it != (x).end(); ++it)
#define pb push_back
#define sz(x) (int)(x).size()

bool bound_check(pair<int, int> p, pair<int, int> d, int maxi, int maxj) { // true = bad
    return (p.first+d.first < 0 || p.first+d.first >= maxi || p.second+d.second < 0 || p.second+d.second >= maxj);
}

vector<pii> dir2 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1)}; 
vector<pii> dir3 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1),
                    pii(1, 1), pii(1, -1), pii(-1, 1), pii(-1, -1)};
vector<pii> dir4 = {pii(1, 1), pii(-1, 1), pii(1, -1), pii(-1, -1)}; 

     
#if 0
#define Cin cin
#endif 

//
fstream Cin;  // UNCOMMENT THIS

const int N = 2e5 + 2;
int test = 1;

map<string, ll> done;
map<string, pair<pair<string, string>, char>> wait;

ll eval(string mon) {
    if (done.count(mon) > 0) return done[mon];

    ll x;
    pair<pair<string, string>, char> p = wait[mon];
    pair<string, string> q = p.first;
    char op = p.second;

    string mon1 = q.first;
    string mon2 = q.second;

    ll x1 = eval(mon1);
    ll x2 = eval(mon2);

    if (op == '+') {
        x = x1 + x2;
    } else if (op == '-') {
        x = x1 - x2;
    } else if (op == '*') {
        x = x1 * x2;
    } else if (op == '/') {
        x = x1 / x2;
    } else {
        cout << "VeryVery bad\n";
        return -1;
    }

    done[mon] = x;
    return x;
}

void solve() {
    ll ans1 = 0;
    ll ans2 = -1;

    string mon, mon1, mon2;
    char op;
    ll x;

    map<string, ll> olddone;
    map<string, pair<pair<string, string>, char>> oldwait;
    

    while(1) {
        if (Cin.peek() == '\n' || Cin.peek() == EOF) {
            // Don't write here except for newline detection
        } else {   
            Cin >> mon >> mon1;
            if (Cin.peek() == '\n' || Cin.peek() == EOF) { // done
                x = stoi(mon1);
                done[mon] = x;
                olddone[mon] = x;
            } else {
                Cin >> op >> mon2;
                wait[mon] = {{mon1, mon2}, op};
                oldwait[mon] = {{mon1, mon2}, op};
            }

        }
        if (Cin.peek() == EOF) break;   // Don't remove
        Cin.ignore();                   // Don't remove     
    }

    pair<pair<string, string>, char> p = wait["root"];
    pair<string, string> q = p.first;

    mon1 = q.first;
    mon2 = q.second;

    ans2 = eval(mon2);
    

    int i = 0;
    olddone["humn"] = 3952673930000;
    while(ans1 != ans2) {
        olddone["humn"] = olddone["humn"] + 1;
        done = olddone;
        wait = oldwait;

        

        ans1 = eval(mon1);

        if (ans1 > ans2) {
            cout << "ans 1 = " << ans1 << '\n';
            cout << "ans 2 = " << ans2 << '\n';
            //return;
        } else {
            //cout << "OK\n";
            //return;
        }
        

        
        //cout << "ans 1 = " << ans1 << '\n';
        //cout << "ans 2 = " << ans2 << '\n';

        i++;
        //if (i > 2)
        //    break;
    }
    cout << "Got answer = " << done["humn"] << "\n";

    

    return;
}



int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout << setprecision(10) << fixed;

    //
    Cin.open("input.txt",ios::in); // UNCOMMENT THIS

    solve();

    //
    Cin.close(); // UNCOMMENT THIS
    return 0;
}


