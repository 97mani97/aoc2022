#include <bits/stdc++.h>
#include <string>
using namespace std;
 
typedef long long ll;
typedef unsigned long long ull;
typedef pair<ull, ull> pii;
typedef vector<int> vi;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define ret(x) for (auto it = (x).begin(); it != (x).end(); ++it)
#define pb push_back

bool bound_check(pair<int, int> p, pair<int, int> d, int maxi, int maxj) { // true = bad
    return (p.first+d.first < 0 || p.first+d.first >= maxi || p.second+d.second < 0 || p.second+d.second >= maxj);
}

vector<pii> dir2 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1)}; 
vector<pii> dir3 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1),
                    pii(1, 1), pii(1, -1), pii(-1, 1), pii(-1, -1)};
vector<pii> dir4 = {pii(1, 1), pii(-1, 1), pii(1, -1), pii(-1, -1)}; 

     
#if 0
#define Cin cin
#endif 

//
fstream Cin;  // UNCOMMENT THIS

const int N = 2e5 + 2;
int test = 1;

string s;



vector<string> t1 = {"####"};
vector<string> t2 = {".#.", "###", ".#."};
vector<string> t3 = {"###", "..#", "..#"};
vector<string> t4 = {"#", "#", "#", "#"};
vector<string> t5 = {"##", "##"};

vector<vector<string>> ts = {t1, t2, t3, t4, t5};

void printtower(set<pii> set_) {
    for (int j = 20; j >= 0; j--) {
        rep(i, 0, 7) {
            if (set_.count({j, i}) > 0) {
                cout << '#';
            } else cout << ".";
        } cout << '\n';
    }
    cout << '\n';
}

void solve() {
    ll ans = 0;

    while(1) {
        if (Cin.peek() == '\n' || Cin.peek() == EOF) {
            // Don't write here except for newline detection
        } else {
            Cin >> s;

        }
        if (Cin.peek() == EOF) break;   // Don't remove
        Cin.ignore();                   // Don't remove     
    }

    ull curint = 0;
    set<pii> set_; // height width
    ull height = 0;
    ull i = 0;

    ull co = 0;

    set<pii> test_set;

    map<pii, ull> mp;
    ull initial;
    ull co_initial;
    bool found = false;
    pii repeat;
    ull old_height;
    ull difference;
    ull old_co;
    ull co_difference;
    ull new_height;


    ull goal = 1000000000000;

    while(co < goal) {
        if (!found && test_set.count({i, curint}) > 0) {
            cout << "HERE " << i << " " << curint <<" and co = " << co << "\n";
            cout << mp[{i, curint}] << '\n';
            initial = mp[{i, curint}];
            found = true;
            repeat = {i, curint};
            old_height = height;
            old_co = co;
        } else if (!found) {
            test_set.insert({i, curint});
        } else if (found && pii({i, curint}) == repeat) {
            difference = height - old_height;
            co_difference = co - old_co;
            new_height = height;
            cout << "current height = " << height << '\n';
            cout << "old height " << old_height << '\n';
            cout << "co = " << co << " old co = " << old_co << '\n';
            cout << "difference = " << difference << " and co difference " << co_difference << '\n';

            ans = old_height + difference*((goal - old_co)/co_difference);
            goal = (goal - old_co) % co_difference; 
            cout << "New goal = " << goal << " and new height = " << height << "\n\n";
            co = 0;
            //return;   
        }
        mp[{i, curint}] = height;

        //cout << "Beginning with height = " << height << '\n';
        vector<string> t = ts[i];
        ull start = height + 3;
        ull left = 2;
        ull right = left + t[0].size();

        

        while(1) {
            char c = s[curint];
            //cout << c << '\n';
            curint = Mod(curint+1, s.size());
            

            bool canmove = true;

            if (c == '<') {
                //cout << "Pushing left\n";
                for (int j = 0; j < t.size(); j++) {
                    for (int i = 0; i < t[0].size(); i++) {
                        if (t[j][i] == '#' && set_.count({start + j, left-1+i}) > 0) canmove = false;
                    }
                }
                if (left == 0) canmove = false;
                if (canmove) {
                    left--;
                    right--;
                }
            } else {
                //cout << "Pushing right " << c << "\n";

                for (int j = 0; j < t.size(); j++) {
                    for (int i = 0; i < t[0].size(); i++) {
                        if (t[j][t[j].size()-1 - i] == '#' && set_.count({start + j, right - i}) > 0) canmove = false;
                    }
                }
                if (right == 7) canmove = false;
                if (canmove) {
                    left++;
                    right++;
                }
            }

           

            canmove = true;
            for (int i = 0; i < t.size(); i++) {
                for (int j = 0; j < t[0].size(); j++) {
                    if (t[i][j] == '#' && set_.count({start - 1 + i, left + j}) > 0) canmove = false;
                }
            }

            if (start == 0) canmove = false;


            if (!canmove) {
                rep(i, 0, t.size()) {
                    rep(j, 0, t[0].size()) {
                        if (t[i][j] == '#') {
                            set_.insert({start + i, left + j});
                            height = max(height, start + i + 1);
                        }
                    }
                }
                /*
                rep(i, 0, t.size()) {
                    bool remove = true;
                    rep(j, 0, 7) {
                        if (set_.count({start + i, j}) == 0) remove = false;
                    }
                    //remove = false;
                    if (remove) {
                        set<pii> new_set;
                        //cout << "Purging \n";
                        for (ull ii = start + i; ii < height+1; ii++) {
                            for (int j = 0; j < 7; j++) {
                                if (set_.count({ii, j}) > 0) new_set.insert({ii, j});
                            }
                        }
                        set_ = new_set;
                        //cout << set_.size() << '\n';
                    }
                } */
                break;
            } else {
                start--;
            }
        }


        i = Mod(i+1, 5);
         
        //printtower(set_);

        co++;

        //if (co >= 10) break;
    }
    cout << new_height << ' ' << height << '\n';


    cout << ans + height - new_height << '\n';
    
 
    return;
}



int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout << setprecision(10) << fixed;

    //
    Cin.open("input.txt",ios::in); // UNCOMMENT THIS



    solve();


    //
    Cin.close(); // UNCOMMENT THIS
    return 0;
}


