#include <bits/stdc++.h>
#include <string>
using namespace std;
 
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
typedef vector<int> vi;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define ret(x) for (auto it = (x).begin(); it != (x).end(); ++it)
#define pb push_back
#define sz(x) (int)(x).size()

bool bound_check(pair<int, int> p, pair<int, int> d, int maxi, int maxj) { // true = bad
    return (p.first+d.first < 0 || p.first+d.first >= maxi || p.second+d.second < 0 || p.second+d.second >= maxj);
}

vector<pii> dir2 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1)}; 
vector<pii> dir3 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1),
                    pii(1, 1), pii(1, -1), pii(-1, 1), pii(-1, -1)};
vector<pii> dir4 = {pii(1, 1), pii(-1, 1), pii(1, -1), pii(-1, -1)}; 

     
#if 0
#define Cin cin
#endif 

//
fstream Cin;  // UNCOMMENT THIS

const int N = 2e5 + 2;
int test = 1;

pii face = {0, 1};
pii start;

pii rot(pii faced) {
    if (faced == pii({0, 1})) return {1, 0};
    else if (faced == pii({1, 0})) return {0, -1};
    else if (faced == pii({0, -1})) return {-1, 0};
    else return pii({0, 1});
}

bool getnext(pii next, bool check, vector<vector<char>> v) {
    int i = next.first;
    int j = next.second;

    int ni, nj;

    int previ = start.first;
    int prevj = start.second;

    if (i == 50 && j == 100) {
        //cout << "here\n";
        assert(previ == i || prevj == j);
        if (previ == i) {
            ni = 49;
            nj = j;
            face = rot(rot(rot(face)));
        }

        if (prevj == j) {
            ni = i;
            nj = 99;
            face = rot(face);
        }

        start = pii({ni, nj});
        return true;
    }

    if (i == 99 && j == 49) {
        //cout << "here\n";
        //cout << "Moving from " << start.first << " and " << start.second << '\n';

        assert(previ == i || prevj == j);
        if (previ == i) {
            ni = 100;
            nj = 49;
            face = rot(rot(rot(face)));
        }

        if (prevj == j) {
            ni = 99;
            nj = 50;
            face = rot(face);
        }

        start = pii({ni, nj});
        //cout << "Going to " << start.first << " and " << start.second << '\n';
        //cout << "face from " << face.first << " and " << face.second << '\n';
        return true;
    }

    if (i == 150 && j == 50) {

        assert(previ == i || prevj == j);
        if (previ == i) {
            ni = 149;
            nj = 50;
            face = rot(rot(rot(face)));
        }

        if (prevj == j) {
            ni = 150;
            nj = 49;
            face = rot(face);
        }

        start = pii({ni, nj});
        return true;
    }

    if (i < 0 && j >= 50 && j <= 99) {
        if (check) assert(face == pii({-1, 0}));
        ni = 150 + (j - 50);
        nj = 0;

        assert(v[ni][nj] == '.' || v[ni][nj] == '#');

        if (v[ni][nj] == '.') {
            face = rot(face);
            start = pii({ni, nj});

        } 
        return true; // 1
    }
    if (i < 0 && j >= 100) {
        if (check) assert(face == pii({-1, 0}));

        ni = 199;
        nj = j - 100;

        assert(v[ni][nj] == '.' || v[ni][nj] == '#');

        if (v[ni][nj] == '.') {
            face = face;
            start = pii({ni, nj});

        }
        return true; // 2
    }
    if (i >= 0 && i <= 49 && j < 50) {
        if (check) assert(face == pii({0, -1}));

        ni = 149 - i;
        nj = 0;

        assert(v[ni][nj] == '.' || v[ni][nj] == '#');

        if (v[ni][nj] == '.') {

            face = rot(rot(face));
            start = pii({ni, nj});

            
        }
        return true; // 3
    }
    if (i >= 0 && i <= 49 && j == 150) {
        if (check) assert(face == pii({0, 1}));

        ni = 149 - i;
        nj = 99;
        assert(v[ni][nj] == '.' || v[ni][nj] == '#');
        if (v[ni][nj] == '.') {

            face = rot(rot(face));
            start = pii({ni, nj});

            
        }
        return true; // 4
    }
    if (i == 50 && j >= 100) {
        if (check) assert(face == pii({1, 0}));

        ni = 50 + (j - 100);
        nj = 99;
        assert(v[ni][nj] == '.' || v[ni][nj] == '#');
        if (v[ni][nj] == '.') {

            face = rot(face);
            start = pii({ni, nj});

        }
        return true; // 5
    }
    if (i >= 50 && i < 100 && j == 49) {
        if (check) assert(face == pii({0, -1}));

        ni = 100;
        nj = i - 50;
        assert(v[ni][nj] == '.' || v[ni][nj] == '#');
        if (v[ni][nj] == '.') {

            face = rot(rot(rot(face)));
            start = pii({ni, nj});

            
        }
        return true; // 6
    }
    if (i == 150 && j >= 50 && j < 100) {
        if (check) assert(face == pii({1, 0}));

        ni = 150 + (j - 50);
        nj = 49;
        assert(v[ni][nj] == '.' || v[ni][nj] == '#');
        if (v[ni][nj] == '.') {

            face = rot(face);
            start = pii({ni, nj});


        }
        return true; // 7
    }

    if (i >= 150 && i <= 199 && j < 0) {
        if (check) assert(face == pii({0, -1}));

        ni = 0;
        nj = 50 + i - 150;
        assert(v[ni][nj] == '.' || v[ni][nj] == '#');
        if (v[ni][nj] == '.') {

            face = rot(rot(rot(face)));
            start = pii({ni, nj});

            cout << "Going to " << start.first << " and " << start.second << '\n';
            cout << "face from " << face.first << " and " << face.second << "\n\n";
        }
        return true; // 1
    }
    if (i == 200 && j >= 0 && j <= 49) {
        if (check) assert(face == pii({1, 0}));

        ni = 0;
        nj = 100 + j;
        assert(v[ni][nj] == '.' || v[ni][nj] == '#');
        if (v[ni][nj] == '.') {

            face = face;
            start = pii({ni, nj});

        }
        return true; // 2
    }
    if (i >= 100 && i <= 149 && j < 0) {
        if (check) assert(face == pii({0, -1}));

        ni = 149 - i;
        nj = 50;
        assert(v[ni][nj] == '.' || v[ni][nj] == '#');
        if (v[ni][nj] == '.') {

            face = rot(rot(face));
            start = pii({ni, nj});
        }
        return true; // 3
    }
    if (i >= 100 && i <= 149 && j == 100) {
        if (check) assert(face == pii({0, 1}));

        ni = 149 - i;
        nj = 149;
        assert(v[ni][nj] == '.' || v[ni][nj] == '#');
        if (v[ni][nj] == '.') {
            face = rot(rot(face));
            start = pii({ni, nj});
        }
        return true; // 4
    }
    if (i >= 50 && i <= 99 && j == 100) {
        if (check) assert(face == pii({0, 1}));

        ni = 49;
        nj = 100 + (i - 50);
        assert(v[ni][nj] == '.' || v[ni][nj] == '#');
        if (v[ni][nj] == '.') {
            cout << "Moving from " << start.first << " and " << start.second << '\n';

            face = rot(rot(rot(face)));

            start = pii({ni, nj});

            cout << "Going to " << start.first << " and " << start.second << '\n';
            cout << "face from " << face.first << " and " << face.second << "\n\n";
        }
        return true; // 5
    }
    if (i == 99 && j >= 0 && j <= 49) {
        if (check) assert(face == pii({-1, 0}));

        ni = 50 + j;
        nj = 50;
        assert(v[ni][nj] == '.' || v[ni][nj] == '#');
        if (v[ni][nj] == '.') {
            face = rot(face);
            start = pii({ni, nj});
        }
        return true; // 6
    }
    if (i >= 150 && i <= 199 && j == 50) {
        if (check) assert(face == pii({0, 1}));

        ni = 149;
        nj = 50 + (i - 150);
        assert(v[ni][nj] == '.' || v[ni][nj] == '#');
        if (v[ni][nj] == '.') {
            face = rot(rot(rot((face))));
            start = pii({ni, nj});
        }
        return true; // 7
    }

    return false;
}


void solve() {
    ll ans = 0;

    char c;
    vector<vector<char>> v;
    string s;
    

    while(1) {
        if (Cin.peek() == '\n' || Cin.peek() == EOF) {
            // Don't write here except for newline detection
            Cin >> s;
        } else {
            vector<char> w;
            while(Cin.peek() != '\n') {
                Cin.get(c);
                w.pb(c);
                //cout << "Got c = " << c << '\n';
            }
            v.pb(w);

        }
        if (Cin.peek() == EOF) break;   // Don't remove
        Cin.ignore();                   // Don't remove     
    }

    int m = v.size();
    //cout << m << '\n';

    int mxn = 0;
    rep(i, 0, m) {
        mxn = max(mxn, (int)v[i].size());
    }

    rep(i, 0, m) {
        vector<char> w = v[i];
        while(w.size() < mxn) w.pb(' ');
        v[i] = w;
        //cout << v[i].size() << '\n';
    }

    //rep(i, 0, m) {
    //    rep(j, 0, v[i].size()) {
    //        cout << v[i][j];
    //    } cout << '\n';
    //}

    
    rep(j, 0, v[0].size()) {
        if (v[0][j] == '.') {
            start = {0, j};
            break;
        }
    }
    
    int i = 0;
    face = {0, 1};


    while(i < s.size()) {
        int j = i;
        while(s[j] >= '0' && s[j] <= '9') j++;
        string t = s.substr(i, j-i);
        c = s[j];
        int step = stoi(t);

        //cout << "Moving from " << start.first << " and " << start.second << '\n';


        rep(ig, 0, step) {
            pii next = {start.first + face.first, start.second + face.second};
            int ni = next.first;
            int nj = next.second;

            if (getnext(next, true, v)) {
                //cout << "over dice to " << start.first << " and " << start.second << '\n';
                continue;
            } 

            if (face.first != 0) { // Moving down
                ni = Mod(ni, m);
            } else {
                int modd = v[ni].size();
                nj = Mod(nj, modd);
            }

            //cout << "Got initial ni = " << ni << " and nj = " << nj << '\n';

            next = {ni, nj};

            if (v[ni][nj] == '#') break;
            else if (v[ni][nj] == '.') start = next;
            else {
                cout << "Very bad at start = " << start.first << " and " << start.second << '\n';
                cout << "With face " << face.first << " and " << face.second << '\n'; 
                return;
            }

            //cout << "    to " << start.first << " and " << start.second << '\n';
        }


        i = j+1;
        if (c == 'R') face = rot(face);
        else if (c == 'L') face = rot(rot(rot(face)));

        //cout << "At " << start.first << " and " << start.second << "\n\n";
        //cout << "Got c = " << c << " and step = " << step << '\n';
        //cout << face.first << " and " << face.second << '\n';
    }

    //cout << '\n' << s << '\n';

    int f;
    if (face == pii({0, 1})) f = 0;
    if (face == pii({1, 0})) f = 1;
    if (face == pii({0, -1})) f = 2;
    if (face == pii({-1, 0})) f = 3;

    ans = 1000*(start.first + 1) + 4*(start.second + 1) + f;

    cout << ans << '\n';
        

    return;
}



int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout << setprecision(10) << fixed;

    //
    Cin.open("input.txt",ios::in); // UNCOMMENT THIS

    solve();

    //
    Cin.close(); // UNCOMMENT THIS
    return 0;
}


