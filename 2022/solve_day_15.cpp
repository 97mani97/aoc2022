#include <bits/stdc++.h>
#include <string>
using namespace std;
 
typedef long long ll;
typedef unsigned long long ull;
typedef pair<long long, long long> pii;
typedef vector<int> vi;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define ret(x) for (auto it = (x).begin(); it != (x).end(); ++it)
#define pb push_back

bool bound_check(pair<int, int> p, pair<int, int> d, int maxi, int maxj) { // true = bad
    return (p.first+d.first < 0 || p.first+d.first >= maxi || p.second+d.second < 0 || p.second+d.second >= maxj);
}

vector<pii> dir2 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1)}; 
vector<pii> dir3 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1),
                    pii(1, 1), pii(1, -1), pii(-1, 1), pii(-1, -1)};
vector<pii> dir4 = {pii(1, 1), pii(-1, 1), pii(1, -1), pii(-1, -1)}; 

     
#if 0
#define Cin cin
#endif 

//
fstream Cin;  // UNCOMMENT THIS

const int N = 2e5 + 2;
int test = 1;
map<pii, long long> mp;

void help(long long y) {
    vector<pii> v;

    for (auto p : mp) {
        pii pair = p.first;
        long long dist = p.second;
        long long sx = pair.first;
        long long sy = pair.second;


        if (dist - abs(sy - y) >= 0){
            //cout << "OK at pair = " << sx << " and " << sy << " got dist = " << dist << "and large " << (dist - abs(sy - y)) <<" and "<<sx - (dist - abs(sy - y)) << " "<< sx + (dist - abs(sy - y)) << '\n';

            v.pb({sx - (dist - abs(sy - y)), sx + (dist - abs(sy - y))});
        }
        else {
            //cout << "HERE" << "at pair = " << sx << " and " << sy << " got dist = " << dist << " and "<<sx - (dist - abs(sy - y)) << " "<< sx + (dist - abs(sy - y)) <<'\n';
            //cout << dist - abs(sy - y) << '\n';
        }
    }

    sort(all(v), []( const pii& lhs, const pii& rhs )
    {
       return lhs.first < rhs.first;
    });

    long long curx = v[0].first;
    long long cury = v[0].second;

    //cout << '\n';
    for (auto p : v) {
        long long sx = p.first;
        long long sy = p.second;
        //cout << sx << ' ' << sy << '\n';

        if (sx <= cury+1) {
            cury = max(sy, cury);
        } else {
            if (cury+1 <= 4000000) {
                cout << "here!!!" << cury + 1 << " and " << y << '\n';
                cout << "curx = " << curx << " cury =" << cury << "sx = " << sx << " and sy = " << sy << " and y = " << y << '\n';
                cout << 4000000 * (cury + 1) + y << '\n';

            }
            
            return;
        }
    }
    return;
}


void solve() {
    long long ans = 0;
    long long sx, sy, bx, by;
    

    //long long y = 10;
    long long y = 2000000;

    set<pii> beacons;

    while(1) {
        if (Cin.peek() == '\n' || Cin.peek() == EOF) {
            // Don't write here except for newline detection
        } else {
            Cin >> sx >> sy >> bx >> by;
            long long dist = abs(bx-sx) + abs(by-sy);
            mp[{sx, sy}] = dist;

            if (by == y) {
                beacons.insert({bx, by});
            }
            //cout << "here at " << sx << " and " << sy << '\n';

        }
        if (Cin.peek() == EOF) break;   // Don't remove
        Cin.ignore();                   // Don't remove     
    }

    for(long long sy = 0; sy <= 4000000; sy++) {
        help(sy);
    }

    cout << ans << "\n";
}



int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout << setprecision(10) << fixed;

    //
    Cin.open("input.txt",ios::in); // UNCOMMENT THIS



    solve();


    //
    Cin.close(); // UNCOMMENT THIS
    return 0;
}


