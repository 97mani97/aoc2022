#include <bits/stdc++.h>
#include <string>
using namespace std;
 
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
typedef vector<int> vi;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define ret(x) for (auto it = (x).begin(); it != (x).end(); ++it)
#define pb push_back

bool bound_check(pair<int, int> p, pair<int, int> d, int maxi, int maxj) {
    return (p.first+d.first < 0 || p.first+d.first >= maxi || p.second+d.second < 0 || p.second+d.second >= maxj);
}

vector<pii> dir2 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1)}; 
vector<pii> dir3 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1),
                    pii(1, 1), pii(1, -1), pii(-1, 1), pii(-1, -1)};
vector<pii> dir4 = {pii(1, 1), pii(-1, 1), pii(1, -1), pii(-1, -1)}; 

     


#if 0
#define Cin cin
#endif 

//
fstream Cin;  // UNCOMMENT THIS

const int N = 2e5 + 2;
int test = 1;


void solve() {
    int ans = 0;
    int m, n, x, y;
    string s, t;
    vi v;
    map<int, int> mp;
    char c, d;

    while(1) {
        if (Cin.peek() == '\n' || Cin.peek() == EOF) {

        } else {
            int win = 0;
            Cin >> c >> d;
            char g;
            if (c == 'A') {
                if (d == 'X') g = 'C';
                else if (d == 'Y') g = 'A';
                else g = 'B'; 
            }
            if (c == 'B') {
                if (d == 'X') g = 'A';
                else if (d == 'Y') g = 'B';
                else g = 'C'; 
            }
            if (c == 'C') {
                if (d == 'X') g = 'B';
                else if (d == 'Y') g = 'C';
                else g = 'A'; 
            }

            if (d == 'X') ans += 0;
            else if (d == 'Y') ans += win + 3;
            else ans += win + 6;

            if (g == 'A') ans += 1;
            else if (g == 'B') ans += win + 2;
            else ans += win + 3;
        }

        if (Cin.peek() == EOF) break;   // Don't remove
        Cin.ignore();                   // Don't remove
    }



    cout << ans << '\n';


}



int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout << setprecision(10) << fixed;

    //
    Cin.open("input.txt",ios::in); // UNCOMMENT THIS

    rep(i, 1, test+1) {
        //cout << "Case #" << i << ": ";
        solve();
    }

    //
    Cin.close(); // UNCOMMENT THIS
    return 0;
}

