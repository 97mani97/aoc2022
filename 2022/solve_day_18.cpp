#include <bits/stdc++.h>
#include <string>
using namespace std;
 
typedef long long ll;
typedef unsigned long long ull;
typedef pair<ull, ull> pii;
typedef vector<int> vi;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define ret(x) for (auto it = (x).begin(); it != (x).end(); ++it)
#define pb push_back
#define sz(x) (int)(x).size()

bool bound_check(pair<int, int> p, pair<int, int> d, int maxi, int maxj) { // true = bad
    return (p.first+d.first < 0 || p.first+d.first >= maxi || p.second+d.second < 0 || p.second+d.second >= maxj);
}

vector<pii> dir2 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1)}; 
vector<pii> dir3 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1),
                    pii(1, 1), pii(1, -1), pii(-1, 1), pii(-1, -1)};
vector<pii> dir4 = {pii(1, 1), pii(-1, 1), pii(1, -1), pii(-1, -1)}; 

     
#if 0
#define Cin cin
#endif 

//
fstream Cin;  // UNCOMMENT THIS

const int N = 2e5 + 2;
int test = 1;


int countedges(vector<pair<pair<int, int>, int>> v) {
    int n = v.size();

    int x, y, z;
    int x2, y2, z2;

    int touching = 0;

    rep(i, 0, n) {
        rep(j, i+1, n) {
            x = v[i].first.first;
            y = v[i].first.second;
            z = v[i].second;

            x2 = v[j].first.first;
            y2 = v[j].first.second;
            z2 = v[j].second;

            int difx = abs(x - x2);
            int dify = abs(y - y2);
            int difz = abs(z - z2);

            if (difx == 0 && dify == 0 && difz == 0) {
                cout << "HERE\n";
                return -1;
            }
            if ((difx == 0 && dify == 0 && difz == 1) ||
                (difx == 0 && dify == 1 && difz == 0) ||
                (difx == 1 && dify == 0 && difz == 0)) {
                touching++;
            }
        }
    }

    int ans = 6 * n - 2 * touching;
    return ans;
}


void solve() {
    ll ans = 0;

    int x, y, z;
    int x2, y2, z2;
    vector<pair<pair<int, int>, int>> v;

    while(1) {
        if (Cin.peek() == '\n' || Cin.peek() == EOF) {
            // Don't write here except for newline detection
        } else {   
            Cin >> x >> y >> z;
            v.pb({{x, y}, z});

        }
        if (Cin.peek() == EOF) break;   // Don't remove
        Cin.ignore();                   // Don't remove     
    }
    int n = v.size();

    int touching = 0;

    rep(i, 0, n) {
        rep(j, i+1, n) {
            x = v[i].first.first;
            y = v[i].first.second;
            z = v[i].second;

            x2 = v[j].first.first;
            y2 = v[j].first.second;
            z2 = v[j].second;

            int difx = abs(x - x2);
            int dify = abs(y - y2);
            int difz = abs(z - z2);

            if (difx == 0 && dify == 0 && difz == 0) {
                cout << "HERE\n";
                return;
            }
            if ((difx == 0 && dify == 0 && difz == 1) ||
                (difx == 0 && dify == 1 && difz == 0) ||
                (difx == 1 && dify == 0 && difz == 0)) {
                touching++;
            }
        }
    }

    ans = 6 * n - 2 * touching;

    cout << ans << '\n';

    int mnx, mxx, mny, mxy, mnz, mxz;
    mnx = mny = mnz = 20;
    mxx = mxy = mxz = 0;

    set<pair<pair<int, int>, int>> st;

    rep(i, 0, n) {
        x = v[i].first.first;
        y = v[i].first.second;
        z = v[i].second;

        st.insert(v[i]);

        mnx = min(mnx, x);
        mny = min(mny, y);
        mnz = min(mnz, z);

        mxx = max(mxx, x);
        mxy = max(mxy, y);
        mxz = max(mxz, z);
    }
    mxx++;
    mxy++;
    mxz++;

    set<pair<pair<int, int>, int>> vis;

    cout << "xs bound " << mnx << ' ' << mxx << '\n';
    cout << "ys bound " << mny << ' ' << mxy << '\n';
    cout << "zs bound " << mnz << ' ' << mxz << '\n';

    int inside = 0;

    rep(i, mnx, mxx) {
        rep(j, mny, mxy) {
            rep(k, mnz, mxz) {
                pair<pair<int, int>, int> p = {{i, j}, k};
                if (st.count(p) == 0 && vis.count(p) == 0) {
                    bool count = true;
                    vector<pair<pair<int, int>, int>> w = {p};
                    vector<pair<pair<int, int>, int>> vc = {p};
                    cout << "Begin with " << i << ' ' << j << ' ' << k << '\n';
                    vis.insert(p);

                    while(vc.size() > 0) {
                        p = *(vc.begin()); vc.erase(vc.begin());
                        
                        x = p.first.first;
                        y = p.first.second;
                        z = p.second;

                        if (x == mnx || x == mxx-1) count = false;
                        if (y == mny || y == mxy-1) count = false;
                        if (z == mnz || z == mxz-1) count = false;

                        pair<pair<int, int>, int> p1, p2, p3, p4, p5, p6;
                        p1 = {{x+1, y}, z};
                        p2 = {{x-1, y}, z};
                        p3 = {{x, y+1}, z};
                        p4 = {{x, y-1}, z};
                        p5 = {{x, y}, z+1};
                        p6 = {{x, y}, z-1};

                        vector<pair<pair<int, int>, int>> news = {p1, p2, p3, p4, p5, p6};

                        for (auto q : news) {
                            x = q.first.first;
                            y = q.first.second;
                            z = q.second;
                            if (x < mnx || x >= mxx) continue;
                            if (y < mny || y >= mxy) continue;
                            if (z < mnz || z >= mxz) continue;
                            if (st.count(q) > 0) continue;
                            if (vis.count(q) > 0) continue;
                            vc.pb(q);
                            w.pb(q);
                            vis.insert(q);
                            //cout << " added " << x << ' ' << y << ' ' << z << '\n';
                        }
                        //return;
                    }

                    if (count) {
                        pair<pair<int, int>, int> p = {{i, j}, k};
                        cout << "Inside with " << i << ' ' << j << ' ' << k << '\n';
                        int ww = countedges(w);
                        inside += ww;
                        cout << "Inside added " << ww << '\n';
                    }

                    //return;
                    
                }
            }
        }
    }
    cout << ans - inside << '\n';
    return;
}



int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout << setprecision(10) << fixed;

    //
    Cin.open("input2.txt",ios::in); // UNCOMMENT THIS



    solve();


    //
    Cin.close(); // UNCOMMENT THIS
    return 0;
}


