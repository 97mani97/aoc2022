#include <bits/stdc++.h>
#include <string>
using namespace std;
 
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
typedef vector<int> vi;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define ret(x) for (auto it = (x).begin(); it != (x).end(); ++it)
#define pb push_back
#define sz(x) (int)(x).size()

bool bound_check(pair<int, int> p, pair<int, int> d, int maxi, int maxj) { // true = bad
    return (p.first+d.first < 0 || p.first+d.first >= maxi || p.second+d.second < 0 || p.second+d.second >= maxj);
}

vector<pii> dir2 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1)}; 
vector<pii> dir3 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1),
                    pii(1, 1), pii(1, -1), pii(-1, 1), pii(-1, -1)};
vector<pii> dir4 = {pii(1, 1), pii(-1, 1), pii(1, -1), pii(-1, -1)}; 

vector<vector<pii>> dirs = {{pii(0, -1), pii(-1, -1), pii(1, -1)},
                            {pii(0, 1), pii(-1, 1), pii(1, 1)},
                            {pii(-1, 0), pii(-1, -1), pii(-1, 1)},
                            {pii(1, 0), pii(1, -1), pii(1, 1)}};

     
#if 0
#define Cin cin
#endif 

//
fstream Cin;  // UNCOMMENT THIS

const int N = 2e5 + 2;
int test = 1;


void solve() {
    ll ans = 0;

    int x, y;
    vector<string> v;
    string s;

    while(1) {
        if (Cin.peek() == '\n' || Cin.peek() == EOF) {
            // Don't write here except for newline detection

        } else {
            Cin >> s;
            v.pb(s);

        }
        if (Cin.peek() == EOF) break;   // Don't remove
        Cin.ignore();                   // Don't remove     
    }
    int m = v.size();
    int n = v[0].size();

    set<pii> st;
    rep(i, 0, m) {
        rep(j, 0, n) {
            if (v[i][j] == '#') st.insert({i, j});
        }
    } 

    int dirbegin = 2;

    for (int round = 0; ; round++) {
        //rep(i, -2, 10) {
        //    rep(j, -2, 10) {
        //        if (st.count(pii({i, j})) == 1) cout << "#";
        //        else cout << ".";
        //    } cout << '\n';
        //} cout << '\n';


        set<pii> copy;
        map<pii, pii> wantMove;
        map<pii, int> wantCount;

        for (pii p : st) {
            x = p.first;
            y = p.second;

            bool look = true;
            for (pii dir : dir3) {
                if (st.count({x + dir.first, y + dir.second}) == 1) look = false;
            }

            if (look) {
                //cout << "The point p = " << p.first << " , " << p.second << " is empty around it" << '\n';
                wantMove[p] = p;
                wantCount[p]++;
                continue;
            }

            bool canmoveonce = false;
            for (int i = 0; i < 4; i++) {
                vector<pii> vec = dirs[((dirbegin + i) % 4)];
                bool canmove = true;
                int j = 0;
                pii r;
                for (pii q : vec) {
                    if (j == 0) r = pii({x+q.first, y+q.second});
                    
                    if (st.count(pii({x+q.first, y+q.second})) > 0) canmove = false;
                    j++;
                }
                if (canmove) {
                    //cout << "The point p = " << p.first << " , " << p.second << " moves to " << r.first << " , " << r.second << '\n';
                    canmoveonce = true;
                    wantMove[p] = r;
                    wantCount[r]++;
                    break;
                }
            }
            if (!canmoveonce) {
                //cout << "The point p = " << p.first << " , " << p.second << " tries to move but does not" << '\n';

                wantMove[p] = p;
                wantCount[p]++;
            }
        }

        ret(wantMove) {
            pii p = it->first;
            pii q = it->second;
            if (wantCount[q] == 1) {
                copy.insert(q);
            } else {
                copy.insert(p);
            }
        }

        bool same = true;
        for (pii p : st) {
            if (copy.count(p) == 0) same = false;
        }
        for (pii p : copy) {
            if (st.count(p) == 0) same = false;
        }

        if (same) {
            cout << round + 1 << '\n';
            break;
        }

        st = copy;
        dirbegin = Mod(dirbegin+1, 4);
    }


    int mnx, mny, mxx, mxy;
    mnx = mny = 10000;
    mxx = mxy = 0;

    for (pii p : st) {
        x = p.first;
        y = p.second;
        mnx = min(mnx, x);
        mny = min(mny, y);
        mxx = max(mxx, x);
        mxy = max(mxy, y);
    }

    for (x = mnx; x <= mxx; x++) {
        for (y = mny; y <= mxy; y++) {
            pii p = {x, y};
            if (st.count(p) == 0) ans++;
        }
    }

    cout << ans << '\n';
        
    return;
}



int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout << setprecision(10) << fixed;

    //
    Cin.open("input.txt",ios::in); // UNCOMMENT THIS

    solve();

    //
    Cin.close(); // UNCOMMENT THIS
    return 0;
}


