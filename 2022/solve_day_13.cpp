#include <bits/stdc++.h>
#include <string>
using namespace std;
 
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
typedef vector<int> vi;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define ret(x) for (auto it = (x).begin(); it != (x).end(); ++it)
#define pb push_back

bool bound_check(pair<int, int> p, pair<int, int> d, int maxi, int maxj) { // true = bad
    return (p.first+d.first < 0 || p.first+d.first >= maxi || p.second+d.second < 0 || p.second+d.second >= maxj);
}

vector<pii> dir2 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1)}; 
vector<pii> dir3 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1),
                    pii(1, 1), pii(1, -1), pii(-1, 1), pii(-1, -1)};
vector<pii> dir4 = {pii(1, 1), pii(-1, 1), pii(1, -1), pii(-1, -1)}; 

     
#if 0
#define Cin cin
#endif 

//
fstream Cin;  // UNCOMMENT THIS

const int N = 2e5 + 2;
int test = 1;

vector<string> parse(string s1) {
    vector<string> v1;
    int i1 = 1;
    int j1 = 1;
    while(j1 < s1.size()) {
        if (s1[j1] != '[') {
            while(s1[j1] != ' ' && s1[j1] != ']') j1++;

            v1.pb(s1.substr(i1, j1-i1));
            i1 = j1+1;
            j1 = j1+1;
        } else {
            int nol = 0;
            while(s1[j1] != ']' || nol > 1) {
                if (s1[j1] == '[') nol++;
                if (s1[j1] == ']') nol--;
                j1++;
            }
            //cout << "got nol = " << nol << '\n';
            v1.pb(s1.substr(i1, 1+j1-i1));
            i1 = j1+2;
            j1 = j1+2;
        }
    }
    return v1;
}

bool comp(string s1, string s2) {
    //cout << "\nBegin s1 " << s1 << " and s2 " << s2 << '\n';
    int i1, i2, j1, j2;
    i1 = i2 = 0;
    string l1, l2;
    j1 = j2 = 0;

    if (s1 == "[]") return true;
    if (s2 == "[]") return false;

    vector<string> v1 = parse(s1);
    vector<string> v2 = parse(s2);

    
    //for (string s : v1) {
    //    cout << s << '\n';
    //}
    //for (string s : v2) {
    //    cout << s << '\n';
    //}
    

    for (int i = 0; i < v1.size(); i++) {
        if (i >= v2.size()) return false;
        l1 = v1[i];
        l2 = v2[i];
        //if (l1 == "[]" || l1 == "") continue;
        //if (l2 == "[]" || l2 == "") return false;
        //cout << "Here with l1 = " << l1 << " and l2 = " << l2 << '\n';

        if (l1[0] == '[' && l2[0] == '[') {
            //cout << "Both lists\n";
            if (l1 == l2) continue;
            if (!comp(l1, l2)) return false;
            else return true;
        }
        else if (l1[0] != '[' && l2[0] != '[') {
            //cout << "Both values l1 = "<< l1 << " and l2 = " << l2 << "\n";
            if (stoi(l1) > stoi(l2)) return false;
            if (stoi(l1) < stoi(l2)) return true;
        }
        else {
            //cout << "One is list only\n";
            if (l1[0] != '[') {
                l1 = "[" + l1;
                l1 = l1 + "]";
            } else {
                l2 = "[" + l2;
                l2 = l2 + "]";
            }
            if (l1 == l2) continue;
            if (!comp(l1, l2)) return false;
            else return true;
        }
    }

    //cout << "s1 = " << s1 << " and s2 = " << s2 << " returns TRUE\n";

    return true;
    
    
}


void solve() {
    int ans = 0;

    string s1, s2;
    int index = 1;

    vector<string> v = {"[[2]]", "[[6]]"};

    while(1) {
        if (Cin.peek() == '\n' || Cin.peek() == EOF) {
            // Don't write here except for newline detection
        } else {
            getline(Cin, s1);
            getline(Cin, s2);
            //cout << "Got s1 = " << s1 << " and s2 = " << s2 << '\n';
            if(comp(s1, s2)) {
                ans += index;
                //cout << "True for index = " << index << '\n';
            }
            index++;
            v.pb(s1);
            v.pb(s2);
        }
        if (Cin.peek() == EOF) break;   // Don't remove
        Cin.ignore();                   // Don't remove     
    }
    cout << ans << "\n\n";
    sort(v.begin(), v.end(), [](const string& t1, const string& t2)
    {
        return comp(t1, t2);
    });
    ans = 0;
    int i1, i2;
    index = 1;
    for (string s : v) {
        //cout << s << '\n';
        if (s == "[[2]]") i1 = index;
        if (s == "[[6]]") i2 = index;
        index++;
    }
    cout << i1 * i2 << "\n\n";

}



int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout << setprecision(10) << fixed;

    //
    Cin.open("input.txt",ios::in); // UNCOMMENT THIS

    rep(i, 1, test+1) {
        //cout << "Case #" << i << ": ";
        solve();
    }

    //
    Cin.close(); // UNCOMMENT THIS
    return 0;
}


