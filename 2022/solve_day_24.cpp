#include <bits/stdc++.h>
#include <string>
using namespace std;
 
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
typedef vector<int> vi;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define ret(x) for (auto it = (x).begin(); it != (x).end(); ++it)
#define pb push_back
#define sz(x) (int)(x).size()

bool bound_check(pair<int, int> p, pair<int, int> d, int maxi, int maxj) { // true = bad
    return (p.first+d.first < 0 || p.first+d.first >= maxi || p.second+d.second < 0 || p.second+d.second >= maxj);
}

vector<pii> dir2 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1)}; 
vector<pii> dir3 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1),
                    pii(1, 1), pii(1, -1), pii(-1, 1), pii(-1, -1)};
vector<pii> dir4 = {pii(1, 1), pii(-1, 1), pii(1, -1), pii(-1, -1)}; 

vector<vector<pii>> dirs = {{pii(0, -1), pii(-1, -1), pii(1, -1)},
                            {pii(0, 1), pii(-1, 1), pii(1, 1)},
                            {pii(-1, 0), pii(-1, -1), pii(-1, 1)},
                            {pii(1, 0), pii(1, -1), pii(1, 1)}};

     
#if 0
#define Cin cin
#endif 

//
fstream Cin;  // UNCOMMENT THIS

const int N = 2e5 + 2;
int test = 1;

pii init = {-1, -1};
pii final = {-2, -2};

vector<pii> nextmoves(pii cur, vector<string> v) {
    int i = cur.first;
    int j = cur.second;
    int m = v.size();
    int n = v[0].size();

    vector<pii> rets;

   
    if (cur == init) {
        rets.pb({-1, -1});
        if (v[0][0] == '.') rets.pb({0, 0});
        return rets;
    }

    if (cur == final) {
        rets.pb({-2, -2});
        if (v[m-1][n-1] == '.') rets.pb({m-1, n-1});
        return rets;
    }

    if (v[i][j] == '.') rets.pb({i, j});

    for (pii d : dir2) {
        if (!bound_check(cur, d, m, n) && v[i+d.first][j+d.second] == '.') rets.pb({i+d.first, j+d.second}); 
    }

    return rets;
}
map<pii, vector<char>> mp;

vector<string> update_v(vector<string> v) {
    int m = v.size();
    int n = v[0].size();

    vector<string> copy;
    map<pii, vector<char>> mp_copy;

    copy = v;
    
    for (auto pai : mp) {
        pii p = pai.first;
        vector<char> chars = pai.second; 

        int i = p.first;
        int j = p.second;
        for (char c : chars) {
            if (c == '>')       mp_copy[{i, Mod(j+1, n)}].pb(c);
            else if (c == '<')  mp_copy[{i, Mod(j-1, n)}].pb(c);
            else if (c == 'v')  mp_copy[{Mod(i+1, m), j}].pb(c);
            else                mp_copy[{Mod(i-1, m), j}].pb(c);
        }
    }

    rep(i, 0, m) {
        rep(j, 0, n) {
            if (mp_copy.count({i, j}) == 0) copy[i][j] = '.';
            else copy[i][j] = '#';
        }
    }

    mp = mp_copy;

    return copy;
}


void solve() {
    ll ans = 0;

    string s;
    char c;
    vector<string> vs;

    while(1) {
        if (Cin.peek() == '\n' || Cin.peek() == EOF) {
            // Don't write here except for newline detection

        } else {
            Cin >> s;
            vs.pb(s);
        }
        if (Cin.peek() == EOF) break;   // Don't remove
        Cin.ignore();                   // Don't remove     
    }

    vector<string> v;
    rep(i, 1, vs.size()-1) {
        s = vs[i];
        s = s.substr(1, s.size()-2);
        v.pb(s);
    }
    int m = v.size();
    int n = v[0].size();

    pii start = init;
    pii end = {m-1, n-1};

    rep(i, 0, m) {
        rep(j, 0, n) {
            if (v[i][j] != '.') mp[{i, j}].pb(v[i][j]);
        }
    }


    //cout << "Initial\n";
    //rep(i, 0, m) {
    //    cout << v[i] << '\n';
    //}

    vector<pii> next_moves = {init};

    for (int count = 0; ; count++) {
        v = update_v(v);

        vector<pii> next;
        set<pii> next_s;
        for (pii cur : next_moves) {
            vector<pii> vec_nex = nextmoves(cur, v);
            for (pii p : vec_nex) next_s.insert(p);
        }
        for (pii p : next_s) next.pb(p);

        next_moves = next;

        //cout << "Update " << count << "\n";
        //rep(i, 0, m) {
        //    cout << v[i] << '\n';
        //}

        if (next_s.count(end) == 1) {
            ans += count+2;
            cout << "Got it at " << count+2 << '\n';
            break;
        }
    }
    v = update_v(v);


    next_moves = {final};

    for (int count = 0; ; count++) {
        v = update_v(v);

        vector<pii> next;
        set<pii> next_s;
        for (pii cur : next_moves) {
            vector<pii> vec_nex = nextmoves(cur, v);
            for (pii p : vec_nex) next_s.insert(p);
        }
        for (pii p : next_s) next.pb(p);

        next_moves = next;

        //cout << "Update " << count << "\n";
        //rep(i, 0, m) {
        //    cout << v[i] << '\n';
        //}

        if (next_s.count({0, 0}) == 1) {
            ans += count+2;
            cout << "Got it at " << count+2 << '\n';
            break;
        }
    }
    v = update_v(v);

    next_moves = {init};

    for (int count = 0; ; count++) {
        v = update_v(v);

        vector<pii> next;
        set<pii> next_s;
        for (pii cur : next_moves) {
            vector<pii> vec_nex = nextmoves(cur, v);
            for (pii p : vec_nex) next_s.insert(p);
        }
        for (pii p : next_s) next.pb(p);

        next_moves = next;

        //cout << "Update " << count << "\n";
        //rep(i, 0, m) {
        //    cout << v[i] << '\n';
        //}

        if (next_s.count(end) == 1) {
            ans += count+2;
            cout << "Got it at " << count+2 << '\n';
            break;
        }
    }


    cout << ans << '\n';
        
    return;
}



int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout << setprecision(10) << fixed;

    //
    Cin.open("input.txt",ios::in); // UNCOMMENT THIS

    solve();

    //
    Cin.close(); // UNCOMMENT THIS
    return 0;
}


