#include <bits/stdc++.h>
#include <string>
using namespace std;
 
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
typedef vector<int> vi;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define ret(x) for (auto it = (x).begin(); it != (x).end(); ++it)
#define pb push_back

bool bound_check(pair<int, int> p, pair<int, int> d, int maxi, int maxj) { // true = bad
    return (p.first+d.first < 0 || p.first+d.first >= maxi || p.second+d.second < 0 || p.second+d.second >= maxj);
}

vector<pii> dir2 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1)}; 
vector<pii> dir3 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1),
                    pii(1, 1), pii(1, -1), pii(-1, 1), pii(-1, -1)};
vector<pii> dir4 = {pii(1, 1), pii(-1, 1), pii(1, -1), pii(-1, -1)}; 

     


#if 0
#define Cin cin
#endif 

//
fstream Cin;  // UNCOMMENT THIS

const int N = 2e5 + 2;
int test = 1;

void solve() {
    ull ans = 0;

    string s;

    vector<string> v;

    while(1) {
        if (Cin.peek() == '\n' || Cin.peek() == EOF) {
            // Don't write here except for newline detection
        } else {
            Cin >> s;
            v.pb(s);
        }

        if (Cin.peek() == EOF) break;   // Don't remove
        Cin.ignore();                   // Don't remove     
    }
    int m = v.size();
    int n = v[0].size();

    set<pii> visi;

    rep(i, 0, m) {
        ull no = 0;
        int lb = -1;

        rep(j, 0, n) {
            if ((v[i][j] - '0') > lb) {
                lb = v[i][j] - '0';
                no++;
                visi.insert({i, j});
                //cout << "Got i " << i << " and j = " << j << '\n';
            }
        }

        lb = -1;
        rep(j, 0, n) {
            
            if ((v[i][n-1-j] - '0') > lb) {
                lb = v[i][n-1-j] - '0';
                if (visi.count({i, n-1-j}) > 0) continue;
                no++;
                visi.insert({i, n-1-j});
            }
        }
        ans += no;
    }

    rep(j, 0, n) {
        ull no = 0;
        int lb = -1;
        rep(i, 0, m) {
            
            if ((v[i][j] - '0') > lb) {
                lb = v[i][j] - '0';

                if (visi.count({i, j}) > 0) continue;

                no++;
                visi.insert({i, j});
            }
        }

        lb = -1;
        rep(i, 0, m) {
            if ((v[n-1-i][j] - '0') > lb) {
                lb = v[n-1-i][j] - '0';

                if (visi.count({n-1-i, j}) > 0) continue;

                no++;
                visi.insert({n-1-i, j});
            }
        }
        ans += no;
    }
    int ans2 = -1;

    rep(i, 0, m) {
        
        rep(j, 0, n) {
            if (i == 0 || j == 0 || i == m-1 || j == n-1) continue;
            cout << "here " << i << " " << j << " and " << v[i][j] - '0' <<  '\n';
            int cur = 1;
            int h = v[i][j] - '0';
            
            int k;

            for (k = i-1; k >= 1; k--) {
                if (v[k][j] - '0' >= h) {
                    break;
                }
            }
            if (i != 0)
                cur *= (i - k);
            cout << "Got " << (i - k) << '\n';

            for (k = i+1; k < m-1; k++) {
                if (v[k][j] - '0' >= h) {
                    break;
                }
            }
            if (i != m-1)
                cur *= (k - i);
            cout << "Got " << (k - i) << '\n';

            for (k = j-1; k >= 1; k--) {
                if (v[i][k] - '0' >= h) {
                    break;
                }
            }
            if (j != 0)
                cur *= (j - k);
            cout << "Got " << (j - k) << '\n';

            for (k = j+1; k < n-1; k++) {
                if (v[i][k] - '0' >= h) {
                    break;
                }
            }
            if (j != n-1)
                cur *= (k - j);
            cout << "Got " << (k - j) << '\n' << '\n';

            ans2 = max(ans2, cur);
        }
        
    }


    cout << ans << '\n';
    cout << ans2 << '\n';

}



int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout << setprecision(10) << fixed;

    //
    Cin.open("input.txt",ios::in); // UNCOMMENT THIS

    rep(i, 1, test+1) {
        //cout << "Case #" << i << ": ";
        solve();
    }

    //
    Cin.close(); // UNCOMMENT THIS
    return 0;
}

