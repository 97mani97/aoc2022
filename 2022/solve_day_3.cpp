#include <bits/stdc++.h>
#include <string>
using namespace std;
 
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
typedef vector<int> vi;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define ret(x) for (auto it = (x).begin(); it != (x).end(); ++it)
#define pb push_back

bool bound_check(pair<int, int> p, pair<int, int> d, int maxi, int maxj) {
    return (p.first+d.first < 0 || p.first+d.first >= maxi || p.second+d.second < 0 || p.second+d.second >= maxj);
}

vector<pii> dir2 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1)}; 
vector<pii> dir3 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1),
                    pii(1, 1), pii(1, -1), pii(-1, 1), pii(-1, -1)};
vector<pii> dir4 = {pii(1, 1), pii(-1, 1), pii(1, -1), pii(-1, -1)}; 

     


#if 0
#define Cin cin
#endif 

//
fstream Cin;  // UNCOMMENT THIS

const int N = 2e5 + 2;
int test = 1;


void solve() {
    int ans = 0;
    string s, t;
    vector<string> v;

    while(1) {
        if (Cin.peek() == '\n' || Cin.peek() == EOF) {
            
        } else {
            Cin >> s;
            v.pb(s);
        }

        if (Cin.peek() == EOF) break;   // Don't remove
        Cin.ignore();                   // Don't remove
    }



    for (int i = 0; i < v.size(); i+= 3) {
        s = v[i];
        t = v[i+1];
        string u = v[i+2];
        set<char> ss, tt;

        rep(i, 0, s.size()) {
            ss.insert(s[i]);
        }
        rep(i, 0, t.size()) {
            tt.insert(t[i]);
        }
         char common;
        rep(i, 0, u.size()) {
            if (ss.count(u[i]) > 0 && tt.count(u[i]) > 0) {
                common = u[i];
            }
        }
       
        
        cout << "Got common " << common << '\n';

        if (common >= 'a' && common <= 'z') {
            ans += common - 'a' + 1;
        } else {
            ans += common - 'A' + 27;
        }
    }

    cout << ans << '\n';


}



int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout << setprecision(10) << fixed;

    //
    Cin.open("input.txt",ios::in); // UNCOMMENT THIS

    rep(i, 1, test+1) {
        //cout << "Case #" << i << ": ";
        solve();
    }

    //
    Cin.close(); // UNCOMMENT THIS
    return 0;
}

