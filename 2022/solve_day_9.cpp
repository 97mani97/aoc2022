#include <bits/stdc++.h>
#include <string>
using namespace std;
 
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
typedef vector<int> vi;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define ret(x) for (auto it = (x).begin(); it != (x).end(); ++it)
#define pb push_back

bool bound_check(pair<int, int> p, pair<int, int> d, int maxi, int maxj) { // true = bad
    return (p.first+d.first < 0 || p.first+d.first >= maxi || p.second+d.second < 0 || p.second+d.second >= maxj);
}

vector<pii> dir2 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1)}; 
vector<pii> dir3 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1),
                    pii(1, 1), pii(1, -1), pii(-1, 1), pii(-1, -1)};
vector<pii> dir4 = {pii(1, 1), pii(-1, 1), pii(1, -1), pii(-1, -1)}; 

     


#if 0
#define Cin cin
#endif 

//
fstream Cin;  // UNCOMMENT THIS

const int N = 2e5 + 2;
int test = 1;

bool updatestep(pii h, pii t) {

    if (max(abs(h.first - t.first),abs(h.second - t.second)) <= 1) return true;
    return false;

    //if (h.first == t.first && h.second = t.second + 1) return true;
    //else if (h.first == t.first && h.second = t.second - 1) return true;
    //else if (h.first == t.first + 1 && h.second = t.second) return true
    //else if (h.first == t.first - 1 && h.second = t.second) return true;

    return false;
} 

pii move(pii h, pii t) {
    if (updatestep(h, t)) return t;

    if (h.first == t.first) t.second = (t.second + h.second) / 2;
    else if (h.second == t.second) t.first = (t.first + h.first) / 2;
    else {
        pii t1 = t; t1.first += 1; t1.second += 1;
        pii t2 = t; t2.first += 1; t2.second -= 1;
        pii t3 = t; t3.first -= 1; t3.second += 1;
        pii t4 = t; t4.first -= 1; t4.second -= 1;
        if (updatestep(h, t1)) return t1;
        if (updatestep(h, t2)) return t2;
        if (updatestep(h, t3)) return t3;
        if (updatestep(h, t4)) return t4;
    }
    return t;
}

void solve() {
    ull ans = 0;

    pii prevh;

    pii h = {0, 0};
    pii t = {0, 0};
    prevh = h;
    set<pii> st;

    vector<pii> v = {h};
    rep(i, 0, 9) v.pb({0, 0});

    vector<pii> vc = v;

    char c;
    int x;

    while(1) {
        if (Cin.peek() == '\n' || Cin.peek() == EOF) {
            // Don't write here except for newline detection
        } else {
            Cin >> c >> x;
            cout << "got c = " << c << " and x = " << x << '\n'; 
            if (c == 'R') {
                rep(i, 0, x) {
                    v[0].second += 1;
                    rep(i, 1, 10) {
                        t = v[i];
                        v[i] = move(v[i-1], t);
                    }
                    st.insert(v[9]);
                }
            }

            if (c == 'U') {
                rep(i, 0, x) {
                    v[0].first += 1;
                    rep(i, 1, 10) {
                        t = v[i];
                        v[i] = move(v[i-1], t);
                    }
                    st.insert(v[9]);
                    //cout << "at t = " << t.first << " " << t.second << '\n';

                }
            }

            if (c == 'L') {
                rep(i, 0, x) {
                    v[0].second -= 1;
                    //cout << "here with h = " << h.first << " " << h.second << " and t = " << t.first << " " << t.second << " and prevh = " << prevh.first << " " << prevh.second << '\n';
                    rep(i, 1, 10) {
                        t = v[i];
                        v[i] = move(v[i-1], t);
                    }
                    st.insert(v[9]);
                    //cout << "at t = " << t.first << " " << t.second << '\n';

                }
            }

            if (c == 'D') {
                rep(i, 0, x) {
                    v[0].first -= 1;
                    rep(i, 1, 10) {
                        t = v[i];
                        v[i] = move(v[i-1], t);
                    }
                    st.insert(v[9]);
                    //cout << "at t = " << t.first << " " << t.second << '\n';

                }
            }
            cout << "Got tail = " << v[9].first << " " << v[9].second << '\n';
            
        }

        if (Cin.peek() == EOF) break;   // Don't remove
        Cin.ignore();                   // Don't remove     
    }

    cout << st.size() << '\n';

}



int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout << setprecision(10) << fixed;

    //
    Cin.open("input.txt",ios::in); // UNCOMMENT THIS

    rep(i, 1, test+1) {
        //cout << "Case #" << i << ": ";
        solve();
    }

    //
    Cin.close(); // UNCOMMENT THIS
    return 0;
}

