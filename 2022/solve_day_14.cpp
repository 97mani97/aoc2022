#include <bits/stdc++.h>
#include <string>
using namespace std;
 
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
typedef vector<int> vi;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define ret(x) for (auto it = (x).begin(); it != (x).end(); ++it)
#define pb push_back

bool bound_check(pair<int, int> p, pair<int, int> d, int maxi, int maxj) { // true = bad
    return (p.first+d.first < 0 || p.first+d.first >= maxi || p.second+d.second < 0 || p.second+d.second >= maxj);
}

vector<pii> dir2 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1)}; 
vector<pii> dir3 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1),
                    pii(1, 1), pii(1, -1), pii(-1, 1), pii(-1, -1)};
vector<pii> dir4 = {pii(1, 1), pii(-1, 1), pii(1, -1), pii(-1, -1)}; 

     
#if 0
#define Cin cin
#endif 

//
fstream Cin;  // UNCOMMENT THIS

const int N = 2e5 + 2;
int test = 1;


void solve() {
    int ans = 0;

    int x, y;
    int px, py;
    set<pii> solids;

    while(1) {
        if (Cin.peek() == '\n' || Cin.peek() == EOF) {
            // Don't write here except for newline detection
        } else {
            Cin >> x >> y; 
            //cout << "x = " << x << " y = " << y << '\n';
            while(Cin.peek() != '\n') {
                Cin >> px >> py;
                //cout << "px = " << px << " py = " << py << '\n';
                rep(i, 0, px-x+1) {
                    //cout << "adding " << x+i << " and " << y << '\n';
                    solids.insert({x + i, y});
                }
                rep(i, 0, py-y+1) {
                    //cout << "adding " << x << " and " << y+i << '\n';
                    solids.insert({x, y + i});
                }
                rep(i, 0, x-px+1) {
                    //cout << "adding " << x-i << " and " << y << '\n';
                    solids.insert({x - i, y});
                }
                rep(i, 0, y-py+1) {
                    //cout << "adding " << x << " and " << y-i << '\n';                 
                    solids.insert({x, y - i});
                }


                x = px;
                y = py;
            }

        }
        if (Cin.peek() == EOF) break;   // Don't remove
        Cin.ignore();                   // Don't remove     
    }


    
    //cout << "bef = " << bef << '\n';

    int mx = -1;
    int miny = 1000;
    int mxy = 0;
    for (auto p : solids) {
        mx = max(p.second, mx);
        miny = min(p.first, miny);
        mxy = max(p.first, mxy);
    }
    cout << "miny = " << miny << " and mxy " << mxy << '\n';
    mx+=2;

    for(int i = miny-10; i < mxy+10; i++) {
        //cout << "inserting " << i << " and " << mx << '\n';
        //solids.insert({i, mx});
    }

    int height = mx;
    int base = 2*height - 1;
    int area = base*height/2;
    cout << height << " " << base << " " <<area << '\n';
    area = 0;
    for (int i = 0; i < height; i++) {
        for (int j = 500-i; j < 500+i+1; j++) {
            area++;
            pii up = {j, i-1};
            pii upl = {j-1, i-1};
            pii upr = {j+1, i-1};
            if (solids.count(up) == 1 && solids.count(upl) == 1 && solids.count(upr) == 1) {
                solids.insert({j, i});
                //cout << "remove1 \n";
                area--;
            } else if (solids.count({j, i}) == 1) {
                area--;
            }
        }
    }

    cout << area << '\n';

    return;

    int bef = solids.size();
    
    //return;
    while(1) {
        pii sand = {500, 0};
        while(1) {
            pii down = {sand.first, sand.second+1};
            pii downl = {sand.first-1, sand.second+1};
            pii downr = {sand.first+1, sand.second+1};
            if (solids.count(down) == 0) sand = down;
            else if (solids.count(downl) == 0) sand = downl;
            else if (solids.count(downr) == 0) sand = downr;
            else {
                //cout << "inserting sand at " << sand.first << " and " << sand.second << '\n';
                solids.insert(sand);

                break;
            }
            //cout << "Got sand = " << sand.first << " and " << sand.second << '\n';
            //break;
        }
        if (sand.first == 500 && sand.second == 0) break;
        //if (sand.second == mx) {
        //    break;
        //}
        //break;
    }

    int now = solids.size();
    ans = now - bef;

    cout << ans << "\n";
}



int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout << setprecision(10) << fixed;

    //
    Cin.open("input.txt",ios::in); // UNCOMMENT THIS



    solve();


    //
    Cin.close(); // UNCOMMENT THIS
    return 0;
}


