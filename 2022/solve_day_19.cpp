#include <bits/stdc++.h>
#include <string>
using namespace std;
 
typedef long long ll;
typedef unsigned long long ull;
typedef pair<ull, ull> pii;
typedef vector<int> vi;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define ret(x) for (auto it = (x).begin(); it != (x).end(); ++it)
#define pb push_back
#define sz(x) (int)(x).size()

bool bound_check(pair<int, int> p, pair<int, int> d, int maxi, int maxj) { // true = bad
    return (p.first+d.first < 0 || p.first+d.first >= maxi || p.second+d.second < 0 || p.second+d.second >= maxj);
}

vector<pii> dir2 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1)}; 
vector<pii> dir3 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1),
                    pii(1, 1), pii(1, -1), pii(-1, 1), pii(-1, -1)};
vector<pii> dir4 = {pii(1, 1), pii(-1, 1), pii(1, -1), pii(-1, -1)}; 

     
#if 0
#define Cin cin
#endif 

//
fstream Cin;  // UNCOMMENT THIS

const int N = 2e5 + 2;
int test = 1;

int ore_cost_ore, clay_cost_ore, obs_cost_ore, obs_cost_clay, geo_cost_ore, geo_cost_obs;

//int MIN_CONST = 24;
int MIN_CONST = 32;

int maxi_so_far = -1;

int help(int ores, int clay, int obs, int geo, int ore_robot, int clay_robot, int obs_robot, 
         int geo_robot, int minu) {
    //cout << "Here at minu = " << minu << '\n';

    int pos = (MIN_CONST - minu) * (MIN_CONST - minu + 1) / 2;

    if (minu == MIN_CONST) return geo;

    if (geo + pos + geo_robot*(MIN_CONST - minu) < maxi_so_far) {
        //cout << "Pruning at " << minu << " with geo = " << geo << " and pos = " << pos << " and maxi_so_far = " << maxi_so_far << '\n';

        return geo;
    }

    int temp_ores = ores;
    int temp_clay = clay;
    int temp_obs = obs;
    int temp_geo = geo;

    int temp_ore_r = ore_robot;
    int temp_clay_r = clay_robot;
    int temp_obs_r = obs_robot;
    int temp_geo_r = geo_robot;


    int maxi = -1;
    if (true) {
        temp_ores += ore_robot;
        temp_clay += clay_robot;
        temp_obs += obs_robot;
        temp_geo += geo_robot;

        maxi = max(maxi, help(temp_ores, temp_clay, temp_obs, temp_geo, temp_ore_r, temp_clay_r, temp_obs_r, temp_geo_r, (minu+1)));
        maxi_so_far = max(maxi_so_far, maxi);
    }

    temp_ores = ores;
    temp_clay = clay;
    temp_obs = obs;
    temp_geo = geo;

    temp_ore_r = ore_robot;
    temp_clay_r = clay_robot;
    temp_obs_r = obs_robot;
    temp_geo_r = geo_robot;

    if (ores >= geo_cost_ore && obs >= geo_cost_obs) {
        //cout << "Building geo at min = " << minu << '\n';
        temp_ores -= geo_cost_ore;
        temp_obs -= geo_cost_obs;
        
        temp_ores += ore_robot;
        temp_clay += clay_robot;
        temp_obs += obs_robot;
        temp_geo += geo_robot;

        temp_geo_r++;

        maxi = max(maxi, help(temp_ores, temp_clay, temp_obs, temp_geo, temp_ore_r, temp_clay_r, temp_obs_r, temp_geo_r, (minu+1)));
        maxi_so_far = max(maxi_so_far, maxi);

    }

    temp_ores = ores;
    temp_clay = clay;
    temp_obs = obs;
    temp_geo = geo;

    temp_ore_r = ore_robot;
    temp_clay_r = clay_robot;
    temp_obs_r = obs_robot;
    temp_geo_r = geo_robot;

    if (ores >= obs_cost_ore && clay >= obs_cost_clay && temp_obs_r < geo_cost_obs) {
        temp_ores -= obs_cost_ore;
        temp_clay -= obs_cost_clay;
        
        temp_ores += ore_robot;
        temp_clay += clay_robot;
        temp_obs += obs_robot;
        temp_geo += geo_robot;

        temp_obs_r++;

        maxi = max(maxi, help(temp_ores, temp_clay, temp_obs, temp_geo, temp_ore_r, temp_clay_r, temp_obs_r, temp_geo_r, (minu+1)));
        maxi_so_far = max(maxi_so_far, maxi);

    }

    temp_ores = ores;
    temp_clay = clay;
    temp_obs = obs;
    temp_geo = geo;

    temp_ore_r = ore_robot;
    temp_clay_r = clay_robot;
    temp_obs_r = obs_robot;
    temp_geo_r = geo_robot;

    if (ores >= clay_cost_ore && temp_clay_r < obs_cost_clay) {
        temp_ores -= clay_cost_ore;
        
        temp_ores += ore_robot;
        temp_clay += clay_robot;
        temp_obs += obs_robot;
        temp_geo += geo_robot;

        temp_clay_r++;

        maxi = max(maxi, help(temp_ores, temp_clay, temp_obs, temp_geo, temp_ore_r, temp_clay_r, temp_obs_r, temp_geo_r, (minu+1)));
        maxi_so_far = max(maxi_so_far, maxi);

    }

    temp_ores = ores;
    temp_clay = clay;
    temp_obs = obs;
    temp_geo = geo;

    temp_ore_r = ore_robot;
    temp_clay_r = clay_robot;
    temp_obs_r = obs_robot;
    temp_geo_r = geo_robot;

    if (ores >= ore_cost_ore && temp_ore_r < max(max(max(ore_cost_ore, clay_cost_ore), obs_cost_ore), geo_cost_ore)) {
        temp_ores -= ore_cost_ore;
        
        temp_ores += ore_robot;
        temp_clay += clay_robot;
        temp_obs += obs_robot;
        temp_geo += geo_robot;

        temp_ore_r++;

        maxi = max(maxi, help(temp_ores, temp_clay, temp_obs, temp_geo, temp_ore_r, temp_clay_r, temp_obs_r, temp_geo_r, (minu+1)));
        maxi_so_far = max(maxi_so_far, maxi);

    }

    return maxi;
}
int help2(int ores, int clay, int obs, int geo, int ore_robot, int clay_robot, int obs_robot, 
         int geo_robot, int minu) {
    ores = 0;
    clay = 0;
    obs = 0;
    geo = 0;

    ore_robot = 1;
    clay_robot = 0;
    obs_robot = 0;
    geo_robot = 0;

    int min = 0;

    while(min < MIN_CONST) {
        bool inc_ores = false;
        bool inc_clay = false;
        bool inc_obs = false;
        bool inc_geo = false;

        if (ores >= geo_cost_ore && obs >= geo_cost_obs) {
            cout << "At min " << min << " Building geo\n";
            ores -= geo_cost_ore;
            obs -= geo_cost_obs;
            inc_geo = true;
        } else if (ores >= obs_cost_ore && clay >= obs_cost_clay && obs_robot < geo_cost_obs) {
            cout << "At min " << min << " Building obs\n";
            ores -= obs_cost_ore;
            clay -= obs_cost_clay;
            inc_obs = true;
        } else if (ores >= clay_cost_ore && clay_robot < obs_cost_clay) {
            cout << "At min " << min << " Building clay\n";
            ores -= clay_cost_ore;
            inc_clay = true;
        } else if (ores >= ore_cost_ore && ore_robot < max(max(max(ore_cost_ore, clay_cost_ore), obs_cost_ore), geo_cost_ore)) {
            cout << "At min " << min << " Building ores\n";
            ores -= ore_cost_ore;
            inc_ores = true;
        }

        ores += ore_robot;
        clay += clay_robot;
        obs += obs_robot;
        geo += geo_robot;
        min++;

        if (inc_ores) ore_robot++;
        if (inc_clay) clay_robot++;
        if (inc_obs) obs_robot++;
        if (inc_geo) geo_robot++;
    }

    return geo;
}


void solve() {
    ll ans = 1;

    int no;


    while(1) {
        if (Cin.peek() == '\n' || Cin.peek() == EOF) {
            // Don't write here except for newline detection
        } else {   
            Cin >> no >> ore_cost_ore >> clay_cost_ore >> obs_cost_ore >> obs_cost_clay >> geo_cost_ore >> geo_cost_obs;
            
            if (no <= 3) {
                int approx = help2(0, 0, 0, 0, 1, 0, 0, 0, 0);
                maxi_so_far = approx;
                cout << "Got approx = " << approx << '\n'; 
                int count =  help(0, 0, 0, 0, 1, 0, 0, 0, 0);
                cout << "At no = " << no << " Got count = " << count << '\n';
                ans *= count;
                //return;
            }

            
            //return;
        }
        if (Cin.peek() == EOF) break;   // Don't remove
        Cin.ignore();                   // Don't remove     
    }

    cout << ans << '\n';

    return;
}



int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout << setprecision(10) << fixed;

    //
    Cin.open("input.txt",ios::in); // UNCOMMENT THIS



    solve();


    //
    Cin.close(); // UNCOMMENT THIS
    return 0;
}


