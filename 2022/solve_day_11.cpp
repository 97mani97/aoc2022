#include <bits/stdc++.h>
#include <string>
using namespace std;
 
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
typedef vector<int> vi;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define ret(x) for (auto it = (x).begin(); it != (x).end(); ++it)
#define pb push_back

bool bound_check(pair<int, int> p, pair<int, int> d, int maxi, int maxj) { // true = bad
    return (p.first+d.first < 0 || p.first+d.first >= maxi || p.second+d.second < 0 || p.second+d.second >= maxj);
}

vector<pii> dir2 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1)}; 
vector<pii> dir3 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1),
                    pii(1, 1), pii(1, -1), pii(-1, 1), pii(-1, -1)};
vector<pii> dir4 = {pii(1, 1), pii(-1, 1), pii(1, -1), pii(-1, -1)}; 

     
#if 0
#define Cin cin
#endif 

//
fstream Cin;  // UNCOMMENT THIS

const int N = 2e5 + 2;
int test = 1;

int no, start_item, new_giv, divi, true_mon, false_mon;
string s1, s2;
char c;

vector<vector<ull>> v;
map<int, char> mp;
map<int, string> mpp;
map<int, int> trues;
map<int, int> falses;
map<int, ull> divis;

map<int, ull> insp;

void solve() {

    while(1) {
        if (Cin.peek() == '\n' || Cin.peek() == EOF) {
            // Don't write here except for newline detection
        } else {
            //cout << "Got t peek = " <<(char)Cin.peek() << '\n';
            vector<ull> vv;

            Cin >> no >> start_item;
            vv.pb(start_item);
            Cin.ignore();

            while(Cin.peek() != 'o') {
                //cout << "Got  peek = " <<(char)Cin.peek() << '\n';
                Cin >> start_item;
                vv.pb(start_item);
                Cin.ignore();

            }
            Cin >> s1 >> c >> s2 >> divi;
            mp[no] = c;
            mpp[no] = s2;
            Cin >> true_mon >> false_mon;
            trues[no] = true_mon;
            falses[no] = false_mon;
            divis[no] = divi;
            cout << "Got s1 = " << s1 << " c = " << c << " and s2 = " << s2 << '\n';

            v.pb(vv);
            
        }
        if (Cin.peek() == EOF) break;   // Don't remove
        Cin.ignore();                   // Don't remove     
    }
    int n = v.size();
    cout << " n = " << n << '\n';
    rep(i, 0, n) {
        insp[i] = 0;
    }

    int l = 9699690;
    //l = 96577;
    //l = 10000000;
 
    rep(round, 0, 10000) {
        rep(i, 0, n) {
            vector<ull> vv = v[i];
            vector<ull> vvv;
            v[i] = vvv;

            for (ull item : vv) {
                insp[i]++;
                ull itemc;
                if (mp[i] == '+') {
                    if (mpp[i] == "old") itemc = item + item;
                    else itemc = item + stoi(mpp[i]);
                } else {
                    if (mpp[i] == "old") itemc = item * item;
                    else itemc = item * stoi(mpp[i]);
                }

                //cout << "Before = " << itemc;
                itemc = (ull)((itemc) % l);
                //cout << " After = " << itemc << '\n';
                //itemc /= 3;
                if (itemc % divis[i] == 0) {
                    v[trues[i]].pb(itemc);
                } else {
                    v[falses[i]].pb(itemc);
                }
            }
        }
    }
    vector<ull> w;
    for (auto p : insp) {
        w.pb(p.second);
    }
    sort(all(w));
    rep(i, 0, w.size()) {
        cout << w[i] << ' ';
    } cout << '\n';
    unsigned long long ans = (unsigned long long)w[n-1] * (unsigned long long)w[n-2];
 
    cout << ans << '\n';
}



int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout << setprecision(10) << fixed;

    //
    Cin.open("input.txt",ios::in); // UNCOMMENT THIS

    rep(i, 1, test+1) {
        //cout << "Case #" << i << ": ";
        solve();
    }

    //
    Cin.close(); // UNCOMMENT THIS
    return 0;
}

