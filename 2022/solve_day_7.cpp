#include <bits/stdc++.h>
#include <string>
using namespace std;
 
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
typedef vector<int> vi;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define ret(x) for (auto it = (x).begin(); it != (x).end(); ++it)
#define pb push_back

bool bound_check(pair<int, int> p, pair<int, int> d, int maxi, int maxj) { // true = bad
    return (p.first+d.first < 0 || p.first+d.first >= maxi || p.second+d.second < 0 || p.second+d.second >= maxj);
}

vector<pii> dir2 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1)}; 
vector<pii> dir3 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1),
                    pii(1, 1), pii(1, -1), pii(-1, 1), pii(-1, -1)};
vector<pii> dir4 = {pii(1, 1), pii(-1, 1), pii(1, -1), pii(-1, -1)}; 

     


#if 0
#define Cin cin
#endif 

//
fstream Cin;  // UNCOMMENT THIS

const int N = 2e5 + 2;
int test = 1;

void solve() {
    ull ans = 0;
    int m, n;
    int x, y, z;
    string s, t;
    char c, d;    

    vi v;

    string cur;

    map<string, vector<string>> mp;
    map<string, ull> mpp;

    map<string, ull> stupid;

    map<string, string> outer_inner;

    set<string> dirs;



    int i = 0;

    while(1) {
        if (Cin.peek() == '\n' || Cin.peek() == EOF) {
            // Don't write here except for newline detection
        } else {
            getline(Cin, s);
            cout << "Got line = " << s << '\n';
            if (s[0] == '$') {
                if (s.substr(2, 2) == "cd") {
                    string curc;
                    curc = s.substr(5);
                    if (curc == "..") {
                        cout << "CHCE outer cur = " << cur << " and outer_inner " << outer_inner[cur] << '\n';
                        cur = outer_inner[cur];
                    } else if (curc == "/") {
                        cur = "/";
                    } else {
                        string prevc = cur;
                        cur = cur + curc +"/";
                        outer_inner[cur] = prevc;
                    }
                    cout << "cur = " << cur << '\n';
                } else {
                    vector<string> vvv;
                    mp[cur] = vvv;
                    while(Cin.peek() != '$' && Cin.peek() != EOF) {
                        Cin >> s >> t;
                        t = cur + t + "/";
                        if (s != "dir") {
                            int x = stoi(s);
                            if (x < 0) {
                                return;
                            }
                            if (mpp.count(t) > 0) {
                                cout << "double " << t << '\n';
                                return;
                            }
                            stupid[cur] += (ull)x;
                            cout << "stupid " << cur << " getting " << x << '\n';
                        } else {
                            mp[cur].pb(t);
                            dirs.insert(t);
                            outer_inner[t] = cur;
                            cout << "out inner " << t << " getting " << cur << "\n";
                        }
                        Cin.ignore();
                    }
                }
            }
        }

        if (Cin.peek() == EOF) break;   // Don't remove
        //Cin.ignore();                   // Don't remove
        
        
    }
    //cout << "Beginning " << '\n' << '\n';
    //return;
    
    while(1) {
        bool ok = true;
        for (auto p : mp) {
            bool curok = true;
            
            s = p.first;
            if (mpp.count(s) > 0) continue;
            ull cur = stupid[s];
            vector<string> vc = p.second;
            //cout << "here with s " << s << '\n';
            for(string e : vc) {
                //cout << "getting " << e << '\n';
                if (mpp.count(e) == 0) {
                    //cout << "e = " << e << " does not exist " << '\n';
                    ok = false;
                    curok = false;
                    break;
                }
                cur += mpp[e];
            }
            if (curok) {
                //cout << "to s = " << s << " adding cur = " << cur << '\n';
                mpp[s] = cur;
            }     
        }
        if (ok) break;
        //break;
        //cout << "at end " << '\n' << '\n';
    }
    cout << '\n';
    
    ull x2 = 70000000 - mpp["/"];
    ull y2 = 30000000 - x2;
    ull ans2 = 70000000;
    cout << ans2 << '\n';
    for (auto p : mpp) {
        if (dirs.count(p.first) > 0) {
            if (p.second > y2 && p.second < ans2) {
                ans2 = p.second;
            }
        }
        if (p.second <= 100000 && dirs.count(p.first) > 0) {
            //cout << "adding " << p.first << " with " << p.second << '\n';
            ans += p.second;
        } else {
            //if (dirs.count(p.first) > 0)
            //cout << "not adding " << p.first << " with " << p.second << '\n';
        }
    }


    cout << '\n' << ans << '\n';
    cout << ans2 << '\n';

}



int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout << setprecision(10) << fixed;

    //
    Cin.open("input.txt",ios::in); // UNCOMMENT THIS

    rep(i, 1, test+1) {
        //cout << "Case #" << i << ": ";
        solve();
    }

    //
    Cin.close(); // UNCOMMENT THIS
    return 0;
}

