#include <bits/stdc++.h>
#include <string>
using namespace std;
 
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
typedef vector<int> vi;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define ret(x) for (auto it = (x).begin(); it != (x).end(); ++it)
#define pb push_back

bool bound_check(pair<int, int> p, pair<int, int> d, int maxi, int maxj) { // true = bad
    return (p.first+d.first < 0 || p.first+d.first >= maxi || p.second+d.second < 0 || p.second+d.second >= maxj);
}

vector<pii> dir2 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1)}; 
vector<pii> dir3 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1),
                    pii(1, 1), pii(1, -1), pii(-1, 1), pii(-1, -1)};
vector<pii> dir4 = {pii(1, 1), pii(-1, 1), pii(1, -1), pii(-1, -1)}; 

     
#if 0
#define Cin cin
#endif 

//
fstream Cin;  // UNCOMMENT THIS

const int N = 2e5 + 2;
int test = 1;



void solve() {
    int ans = 0;

    string s;
    int x;
    vi v = {1};

    while(1) {
        if (Cin.peek() == '\n' || Cin.peek() == EOF) {
            // Don't write here except for newline detection
        } else {
            Cin >> s;
            if (s == "noop") {
                v.pb(v[v.size()-1]);
            } else {
                Cin >> x;
                v.pb(v[v.size()-1]);
                //v.pb(v[v.size()-1]);
                v.pb(v[v.size()-1] + x);
                //cout << "Adding x = " << x << " Getting " << v[v.size()-1] <<'\n';
            }

        }
        if (Cin.peek() == EOF) break;   // Don't remove
        Cin.ignore();                   // Don't remove     
    }

    //cout << v[19] << '\n';

    ans = 20*v[20-1] + 60*v[60-1] + 100*v[100-1] + 140*v[140-1] + 180*v[180-1] + 220*v[220-1];

    cout << ans << '\n';

    int pos = v[0];

    vector<vector<char>> vc;
    rep(i, 0, 6) {
        vector<char> vvc;
        rep(j, 0, 40) {
            int n = i*40 + j;
            pos = v[n];
            //cout << "Here with i = " << i << " j = " << j << " and pos = " << pos << '\n'; 
            if (abs(pos - j) <= 1) vvc.pb('#');
            else vvc.pb('.');
           
        }
        vc.pb(vvc);
    }
    rep(i, 0, 6) {
        rep(j, 0, 40) {
            cout << vc[i][j];
        } cout << '\n';
    } cout << '\n';
}



int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout << setprecision(10) << fixed;

    //
    Cin.open("input.txt",ios::in); // UNCOMMENT THIS

    rep(i, 1, test+1) {
        //cout << "Case #" << i << ": ";
        solve();
    }

    //
    Cin.close(); // UNCOMMENT THIS
    return 0;
}

