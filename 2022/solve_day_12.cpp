#include <bits/stdc++.h>
#include <string>
using namespace std;
 
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
typedef vector<int> vi;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define ret(x) for (auto it = (x).begin(); it != (x).end(); ++it)
#define pb push_back

bool bound_check(pair<int, int> p, pair<int, int> d, int maxi, int maxj) { // true = bad
    return (p.first+d.first < 0 || p.first+d.first >= maxi || p.second+d.second < 0 || p.second+d.second >= maxj);
}

vector<pii> dir2 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1)}; 
vector<pii> dir3 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1),
                    pii(1, 1), pii(1, -1), pii(-1, 1), pii(-1, -1)};
vector<pii> dir4 = {pii(1, 1), pii(-1, 1), pii(1, -1), pii(-1, -1)}; 

     
#if 0
#define Cin cin
#endif 

//
fstream Cin;  // UNCOMMENT THIS

const int N = 2e5 + 2;
int test = 1;

#define V 6000
 
// A utility function to find the vertex with minimum
// distance value, from the set of vertices not yet included
// in shortest path tree
int minDistance(int dist[], bool sptSet[])
{
 
    // Initialize min value
    int min = INT_MAX, min_index;
 
    for (int v = 0; v < V; v++)
        if (sptSet[v] == false && dist[v] <= min)
            min = dist[v], min_index = v;
 
    return min_index;
}
 
// A utility function to print the constructed distance
// array
void printSolution(int dist[])
{
    cout << "Vertex \t Distance from Source" << endl;
    for (int i = 0; i < V; i++)
        cout << i << " \t\t\t\t" << dist[i] << endl;
}
 
// Function that implements Dijkstra's single source
// shortest path algorithm for a graph represented using
// adjacency matrix representation
void dijkstra(int graph[V][V], int src)
{
    int dist[V]; // The output array.  dist[i] will hold the
                 // shortest
    // distance from src to i
 
    bool sptSet[V]; // sptSet[i] will be true if vertex i is
                    // included in shortest
    // path tree or shortest distance from src to i is
    // finalized
 
    // Initialize all distances as INFINITE and stpSet[] as
    // false
    for (int i = 0; i < V; i++)
        dist[i] = INT_MAX, sptSet[i] = false;
 
    // Distance of source vertex from itself is always 0
    dist[src] = 0;
 
    // Find shortest path for all vertices
    for (int count = 0; count < V - 1; count++) {
        // Pick the minimum distance vertex from the set of
        // vertices not yet processed. u is always equal to
        // src in the first iteration.
        int u = minDistance(dist, sptSet);
 
        // Mark the picked vertex as processed
        sptSet[u] = true;
 
        // Update dist value of the adjacent vertices of the
        // picked vertex.
        for (int v = 0; v < V; v++)
 
            // Update dist[v] only if is not in sptSet,
            // there is an edge from u to v, and total
            // weight of path from src to  v through u is
            // smaller than current value of dist[v]
            if (!sptSet[v] && graph[u][v]
                && dist[u] != INT_MAX
                && dist[u] + graph[u][v] < dist[v])
                dist[v] = dist[u] + graph[u][v];
    }
 
    // print the constructed distance array
    printSolution(dist);
}

void solve() {
    ull ans = 0;
    //int graph[V][V];

    vector<vector<char>> v;

    while(1) {
        if (Cin.peek() == '\n' || Cin.peek() == EOF) {
            // Don't write here except for newline detection
        } else {
            string s;
            Cin >> s;
            vector<char> w;
            rep(i, 0, s.size()) {
                //cout << "size = " << s.size() << '\n';
                w.pb(s[i]);
            }
            v.pb(w);
            
        }
        if (Cin.peek() == EOF) break;   // Don't remove
        Cin.ignore();                   // Don't remove     
    }
    int m = v.size();
    int n = v[0].size();
    cout << "Got m = " << m << " and n = " << n << '\n';
    cout << m * n << '\n';

    vector<vector<int>> adj(V);

    int start, end;

    rep(i, 0, m) {
        rep(j, 0, n) {
            if (v[i][j] == 'S') {
                start = i*n + j;
                v[i][j] = 'a';
            }
            if (v[i][j] == 'E') {
                end = i * n + j;
                v[i][j] = 'z';
            }
        }
    }

    rep(i, 0, m) {
        rep(j, 0, n) {
            int x = i*n + j;
            vi w;
            if (i+1 < m && v[i][j] + 1 >= v[i+1][j]) w.pb((i+1)*n + j);
            if (i-1 >= 0 && v[i][j] + 1 >= v[i-1][j]) w.pb((i-1)*n + j);
            if (j+1 < n && v[i][j] + 1 >= v[i][j+1]) w.pb((i)*n + j+1);
            if (j-1 >= 0 && v[i][j] + 1 >= v[i][j-1]) w.pb((i)*n + j-1);
            adj[x] = w;
        }
    }


    priority_queue<pii> pq;
    vector<bool> mark(V);

    vi dist(V);
    rep(i, 0, V) {
        dist[i] = INT_MAX;
        mark[i] = false;
    }

    rep(i, 0, m) {
        rep(j, 0, n) {
            if (v[i][j] == 'a') {
                dist[i*n + j] = 0;
                pq.push({0, i*n+j});
            }
        }
    }

    //dist[start] = 0;
    //pq.push({0, start});
    while(!pq.empty()) {
        int d = pq.top().first;
        int cur = pq.top().second;
        pq.pop();
        if (mark[cur]) continue;
        mark[cur] = true;
        //cout << "at letter " << v[cur/n][cur%n] << " and dist = " << d <<" with i = " << cur / n << " and j = " << cur % n << '\n';

        for (int next : adj[cur]) {
            if (dist[cur] + 1 < dist[next]) {
                dist[next] = dist[cur] + 1;
                pq.push({-dist[next], next});
            }
        }
    }

    
    cout << "end = " << end << '\n';
    cout << dist[end] << '\n';
}



int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout << setprecision(10) << fixed;

    //
    Cin.open("input.txt",ios::in); // UNCOMMENT THIS

    rep(i, 1, test+1) {
        //cout << "Case #" << i << ": ";
        solve();
    }

    //
    Cin.close(); // UNCOMMENT THIS
    return 0;
}


