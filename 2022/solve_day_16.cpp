#include <bits/stdc++.h>
#include <string>
using namespace std;
 
typedef long long ll;
typedef unsigned long long ull;
typedef pair<long long, long long> pii;
typedef vector<int> vi;

#define Mod(x,y) (((x)%(y)+(y))%(y))
#define rep(i, a, b) for(int (i) = (a); (i) < (b); ++(i))
#define all(x) begin(x), end(x)
#define ret(x) for (auto it = (x).begin(); it != (x).end(); ++it)
#define pb push_back

bool bound_check(pair<int, int> p, pair<int, int> d, int maxi, int maxj) { // true = bad
    return (p.first+d.first < 0 || p.first+d.first >= maxi || p.second+d.second < 0 || p.second+d.second >= maxj);
}

vector<pii> dir2 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1)}; 
vector<pii> dir3 = {pii(1, 0), pii(-1, 0), pii(0, 1), pii(0, -1),
                    pii(1, 1), pii(1, -1), pii(-1, 1), pii(-1, -1)};
vector<pii> dir4 = {pii(1, 1), pii(-1, 1), pii(1, -1), pii(-1, -1)}; 

     
#if 0
#define Cin cin
#endif 

//
fstream Cin;  // UNCOMMENT THIS

const int N = 2e5 + 2;
int test = 1;


//# define V 10
# define V 58

string s, t;
int flow;
map<string, int> st_to_ind;
map<int, string> ind_to_st;
map<int, int> ind_flow;
map<string, vector<string>> mp;

int minDistance(map<int, int> dist, bool sptSet[])
{
 
    // Initialize min value
    int min = INT_MAX, min_index;
 
    for (int v = 0; v < V; v++)
        if (sptSet[v] == false && dist[v] <= min)
            min = dist[v], min_index = v;
 
    return min_index;
}

 
// Function that implements Dijkstra's single source
// shortest path algorithm for a graph represented using
// adjacency matrix representation
map<int, int> dijkstra(int graph[V][V], int src)
{
    map<int, int> dist; // The output array.  dist[i] will hold the
                 // shortest
    // distance from src to i
 
    bool sptSet[V]; // sptSet[i] will be true if vertex i is
                    // included in shortest
    // path tree or shortest distance from src to i is
    // finalized
 
    // Initialize all distances as INFINITE and stpSet[] as
    // false
    for (int i = 0; i < V; i++)
        dist[i] = INT_MAX, sptSet[i] = false;
 
    // Distance of source vertex from itself is always 0
    dist[src] = 0;
 
    // Find shortest path for all vertices
    for (int count = 0; count < V - 1; count++) {
        // Pick the minimum distance vertex from the set of
        // vertices not yet processed. u is always equal to
        // src in the first iteration.
        int u = minDistance(dist, sptSet);
 
        // Mark the picked vertex as processed
        sptSet[u] = true;
 
        // Update dist value of the adjacent vertices of the
        // picked vertex.
        for (int v = 0; v < V; v++)
 
            // Update dist[v] only if is not in sptSet,
            // there is an edge from u to v, and total
            // weight of path from src to  v through u is
            // smaller than current value of dist[v]
            if (!sptSet[v] && graph[u][v]
                && dist[u] != INT_MAX
                && dist[u] + graph[u][v] < dist[v])
                dist[v] = dist[u] + graph[u][v];
    }
 
    // print the constructed distance array
    return dist;
}

vector<map<int, int>> v;
int T = 26;


int help(int min, int cur, set<int> vis, int cur_win) {
    int ret_win = cur_win;
    set<int> ret_vis = vis;

    map<int, int> cur_map = v[cur];
    for (auto p : cur_map) {
        int node = p.first;
        int dist = p.second;

        if (ind_flow[node] == 0) continue;
        if (vis.count(node) > 0) continue;
        if (min + dist + 1 > T) continue; // NOCHECKIN


        int nxt_min = min + dist + 1;
        int win = ind_flow[node]*(T - (min + dist + 1));
        set<int> nxt_vis = vis;
        nxt_vis.insert(node);

        int helper = help(nxt_min, node, nxt_vis, cur_win + win);
        if (helper > ret_win) {
            ret_win = helper;
        }
    }

    return ret_win;
}


void solve() {
    int ans = 0;

    int graph[V][V];
    int count = 0;

    while(1) {
        if (Cin.peek() == '\n' || Cin.peek() == EOF) {
            // Don't write here except for newline detection
        } else {
            Cin >> s >> flow;
            st_to_ind[s] = count;
            ind_to_st[count] = s;
            ind_flow[count] = flow;
            while(Cin.peek() != '\n') {
                //cout << "To s = " << s << " connecting " << t
                Cin >> t;
                mp[s].pb(t);
            }

            count++;

        }
        if (Cin.peek() == EOF) break;   // Don't remove
        Cin.ignore();                   // Don't remove     
    }

    vector<int> nonzeros;

    rep(i, 0, ind_flow.size()) {
        if (ind_flow[i] > 0) {
            nonzeros.pb(i);
            cout << "Got i = " << i << '\n';
        }
    }

    int non = nonzeros.size();



    cout << mp.size() << '\n';
    assert(mp.size() == V);

    rep(i, 0, V) rep(j, 0, V) graph[i][j] = 0;
 
    rep(i, 0, V) {
        s = ind_to_st[i];
        for (string con : mp[s]) {
            graph[i][st_to_ind[con]] = 1;
        }
    }

    rep(i, 0, V) {
        v.pb(dijkstra(graph, i));
    }

    int res = -1;

    vector<int> curs;

    for (int i = 0; i < pow(2, non); i++) {
        int cur = 0;
        set<int> vis;
        map<int, int> cpy = ind_flow;
        cout << "at i = " << i << '\n';
        int k = i;
        int count = 0;
        while(k > 0) {
            int j = k & 1;
            //cout << "Got j = " << j << '\n';
            if (j == 1) {
                ind_flow[nonzeros[count]] = 0; 
                cout << "Blocking pipe " << ind_to_st[nonzeros[count]] << '\n';
            }
            k = (k >> 1);
            count++;
        }
        cur = help(0, st_to_ind["AA"], vis, 0);
        cout << "Cur = " << cur << '\n';
        ind_flow = cpy;

        curs.pb(cur);
    }

    rep(i, 0, curs.size()) {
        res = max(res, curs[i] + curs[curs.size()-1-i]);
    }

    cout << res << '\n';
    
 
    return;
}



int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout << setprecision(10) << fixed;

    //
    Cin.open("input.txt",ios::in); // UNCOMMENT THIS



    solve();


    //
    Cin.close(); // UNCOMMENT THIS
    return 0;
}


